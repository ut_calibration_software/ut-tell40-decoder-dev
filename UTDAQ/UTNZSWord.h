/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <iostream>
#include <string>

/** @class UTNZSWord UTNZSWord.h "UTDAQ/UTNZSWord.h"
 *
 *  Class for encapsulating header word in RAW data format
 *  for the UT Si detectors.
 *
 *  @author Wojciech Krupa (based on code by Xuhao Yuan, wokrupa@cern.ch)
 *  @date   2020-06-17  / 2023-06-12
 */

class UTNZSWord final {
  enum class mask {
    strip   = 0xFFE0,
    adc     = 0x1F,
    adcL    = 0xFF,
    adcR    = 0xFF00,
    mcm_val = 0xFC0000,
    mcm_ch  = 0x03FC00,
    mem_sep = 0x0003FE,
    ev_par  = 0x000001
  };

  static constexpr unsigned int maxstrip = 512;

  template <mask m>
  [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
    constexpr auto b = __builtin_ctz( static_cast<unsigned int>( m ) );
    return ( i & static_cast<unsigned int>( m ) ) >> b;
  }

  template <mask m>
  [[nodiscard]] static constexpr unsigned int shift( unsigned int i ) {
    constexpr auto b = __builtin_ctz( static_cast<unsigned int>( m ) );
    auto           v = ( i << static_cast<unsigned int>( b ) );
    assert( extract<m>( v ) == i );
    return v;
  }

public:
  /** constructer with int
    @param value
    */
  explicit constexpr UTNZSWord( unsigned int value ) : m_value( value ) {}

  constexpr UTNZSWord( unsigned int value, unsigned int nlane ) : m_value( value ), m_lane( nlane ){};

  constexpr UTNZSWord( unsigned int value, unsigned int nlane, unsigned int asic_num, unsigned int nzs_header,
                       unsigned int pos )
      : m_value( value ), m_lane( nlane ), m_asic_num( asic_num ), m_nzs_header( nzs_header ), m_pos( pos ){};

  /** The actual value
    @return valueW
    */
  [[nodiscard]] constexpr unsigned int value() const { return m_value; };

  [[nodiscard]] constexpr unsigned int channelIDL() const {
    return maxstrip * m_lane + asic_num() % 4 * 128 + ( 127 - ( 2 * m_pos % 132 - 4 ) );
  }
  [[nodiscard]] constexpr unsigned int channelIDR() const {
    return maxstrip * m_lane + asic_num() % 4 * 128 + ( 127 - ( 2 * m_pos % 132 - 3 ) );
  }
  [[nodiscard]] constexpr unsigned int adcL() const { return extract<mask::adcL>( m_value ); }

  [[nodiscard]] constexpr unsigned int adcR() const { return extract<mask::adcR>( m_value ); }

  [[nodiscard]] constexpr unsigned int pos() const { return m_pos; }

  [[nodiscard]] constexpr bool is_not_data_header() const { return m_pos % 66 > 1 ? 1 : 0; }

  [[nodiscard]] constexpr unsigned int asic_num() const { return extract<mask::adcR>( m_asic_num ); }

  [[nodiscard]] constexpr unsigned int nzs_header() const { return extract<mask::adcL>( m_asic_num ) | m_nzs_header; }

  [[nodiscard]] constexpr unsigned int mcm_val() const {
    return extract<mask::mcm_val>( extract<mask::adcL>( m_asic_num ) | m_nzs_header );
  }

  [[nodiscard]] constexpr unsigned int mcm_ch() const {
    return extract<mask::mcm_ch>( extract<mask::adcL>( m_asic_num ) | m_nzs_header );
  }

  [[nodiscard]] constexpr unsigned int mem_sep() const {
    return extract<mask::mem_sep>( extract<mask::adcL>( m_asic_num ) | m_nzs_header );
  }

  [[nodiscard]] constexpr unsigned int ev_par() const {
    return extract<mask::ev_par>( extract<mask::adcL>( m_asic_num ) | m_nzs_header );
  }

  [[nodiscard]] constexpr unsigned int lane() const { return m_lane; }

  // Operator overloading for stringoutput
  friend std::ostream& operator<<( std::ostream& s, const UTNZSWord& obj ) { return obj.fillStream( s ); }

  // Fill the ASCII output stream
  std::ostream& fillStream( std::ostream& s ) const {
    return s << "{ "
             << " value:\t" << value() << " }\n";
  }

private:
  unsigned int m_value          = 0;
  unsigned int m_lane           = 0;
  unsigned int m_asic_num       = 0;
  unsigned int m_nzs_header     = 0;
  unsigned int m_pos            = 0;
};
