/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <iostream>
#include <string>

/** @class UTADCWord UTADCWord.h "UTDAQ/UTADCWord.h"
 *
 *  Class for encapsulating header word in RAW data format
 *  for the UT Si detectors.
 *
 *  @author Xuhao Yuan
 *  @author Wojciech Krupa (based on code by Xuhao Yuan, wokrupa@cern.ch)
 *  @date   2020-06-17  / 2023-06-12
 */

class UTADCWord final {
  enum class mask { strip = 0xFFE0, adc = 0x1F };

  static constexpr unsigned int maxstrip = 512;

  template <mask m>
  [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
    constexpr auto b = __builtin_ctz( static_cast<unsigned int>( m ) );
    return ( i & static_cast<unsigned int>( m ) ) >> b;
  }

  template <mask m>
  [[nodiscard]] static constexpr unsigned int shift( unsigned int i ) {
    constexpr auto b = __builtin_ctz( static_cast<unsigned int>( m ) );
    auto           v = ( i << static_cast<unsigned int>( b ) );
    assert( extract<m>( v ) == i );
    return v;
  }

public:
  /** constructer with int
    @param value
    */
  explicit constexpr UTADCWord( unsigned int value ) : m_value( value ) {}

  constexpr UTADCWord( unsigned int value, unsigned int nlane, double offset = 0., unsigned int cluster_charge = 0,
                       unsigned int cluster_size = 0 )
      : m_value( value )
      , m_lane( nlane )
      , m_offset( offset )
      , m_cluster_charge( cluster_charge )
      , m_cluster_size( cluster_size ){};

  /** The actual value
    @return valueW
    */
  [[nodiscard]] constexpr unsigned int value() const { return m_value; };

  [[nodiscard]] constexpr unsigned int channelID() const { return (extract<mask::strip>( m_value ) % 512 ) + maxstrip * m_lane; }

  [[nodiscard]] constexpr unsigned int adc() const { return extract<mask::adc>( m_value ); }

  [[nodiscard]] constexpr unsigned int lane() const { return m_lane; }

  [[nodiscard]] constexpr double fractionalStripPosition() const { return m_offset; }

  // Backward compability
  [[nodiscard]] constexpr double fracStripBits() const { return fractionalStripPosition(); }

  [[nodiscard]] constexpr unsigned int clusterSize() const {
    if ( m_cluster_size ) {
      return m_cluster_size;
    } else {
      return 1;
    }
  }

  // Backward compability
  [[nodiscard]] constexpr unsigned int pseudoSizeBits() const { return clusterSize(); }

  [[nodiscard]] constexpr unsigned int clusterCharge() const {
    if ( m_cluster_charge ) {
      return m_cluster_charge;
    } else {
      return adc();
    }
  }

  [[nodiscard]] constexpr bool hasHighThreshold() const { return true; }

  // Operator overloading for stringoutput
  friend std::ostream& operator<<( std::ostream& s, const UTADCWord& obj ) { return obj.fillStream( s ); }

  // Fill the ASCII output stream
  std::ostream& fillStream( std::ostream& s ) const {
    return s << "{ "
             << " value:\t" << value() << " }\n";
  }

private:
  unsigned int m_value          = 0;
  unsigned int m_lane           = 0;
  double       m_offset         = 0.;
  unsigned int m_cluster_charge = 0;
  unsigned int m_cluster_size   = 0;
};
