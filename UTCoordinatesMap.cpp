/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3),copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence,CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTCoordinatesMap.cpp
 *
 *  Created on: August,2023
 *  Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#include "UTDAQ/UTCoordinatesMap.h"

// Constructor definition initializes the 5D vector
UTCoordinatesMap::UTCoordinatesMap()
    : UT_CoordinatesMap(
          8, std::vector<std::vector<std::vector<std::vector<CoordinatesTuple>>>>(
                 2, std::vector<std::vector<std::vector<CoordinatesTuple>>>(
                        9, std::vector<std::vector<CoordinatesTuple>>(
                               2, std::vector<CoordinatesTuple>( 2, std::make_tuple( 0.0f, 0.0f, "", "" ) ) ) ) ) ) {

  UT_CoordinatesMap[7][0][0][1][0] = std::make_tuple( -0.5, 6.5, "1AT_M4", "A" );
  UT_CoordinatesMap[7][1][0][1][0] = std::make_tuple( -0.5, 5.5, "1AT_S4", "A" );
  UT_CoordinatesMap[6][0][0][1][0] = std::make_tuple( -0.5, 4.5, "1AT_M3", "A" );
  UT_CoordinatesMap[6][1][0][1][0] = std::make_tuple( -0.5, 3.5, "1AT_S3", "A" );
  UT_CoordinatesMap[5][0][0][1][0] = std::make_tuple( -0.5, 2.5, "1AT_M2", "A" );
  UT_CoordinatesMap[5][1][0][1][1] = std::make_tuple( -0.25, 1.5, "1AT_S2E", "B" );
  UT_CoordinatesMap[5][1][0][1][0] = std::make_tuple( -0.75, 1.5, "1AT_S2W", "B" );
  UT_CoordinatesMap[4][0][0][1][1] = std::make_tuple( -0.25, 0.75, "1AT_M1E", "C" );
  UT_CoordinatesMap[4][0][0][1][0] = std::make_tuple( -0.75, 0.75, "1AT_M1W", "C" );
  UT_CoordinatesMap[4][1][0][1][1] = std::make_tuple( -0.25, 0.25, "1AT_S1E", "C" );
  UT_CoordinatesMap[4][1][0][1][0] = std::make_tuple( -0.75, 0.25, "1AT_S1W", "C" );
  UT_CoordinatesMap[3][0][0][1][1] = std::make_tuple( -0.25, -0.25, "1AB_S1E", "C" );
  UT_CoordinatesMap[3][0][0][1][0] = std::make_tuple( -0.75, -0.25, "1AB_S1W", "C" );
  UT_CoordinatesMap[3][1][0][1][1] = std::make_tuple( -0.25, -0.75, "1AB_M1E", "C" );
  UT_CoordinatesMap[3][1][0][1][0] = std::make_tuple( -0.75, -0.75, "1AB_M1W", "C" );
  UT_CoordinatesMap[2][0][0][1][1] = std::make_tuple( -0.25, -1.5, "1AB_S2E", "B" );
  UT_CoordinatesMap[2][0][0][1][0] = std::make_tuple( -0.75, -1.5, "1AB_S2W", "B" );
  UT_CoordinatesMap[2][1][0][1][0] = std::make_tuple( -0.5, -2.5, "1AB_M2", "A" );
  UT_CoordinatesMap[1][0][0][1][0] = std::make_tuple( -0.5, -3.5, "1AB_S3", "A" );
  UT_CoordinatesMap[1][1][0][1][0] = std::make_tuple( -0.5, -4.5, "1AB_M3", "A" );
  UT_CoordinatesMap[0][0][0][1][0] = std::make_tuple( -0.5, -5.5, "1AB_S4", "A" );
  UT_CoordinatesMap[0][1][0][1][0] = std::make_tuple( -0.5, -6.5, "1AB_M4", "A" );
  UT_CoordinatesMap[7][0][0][0][0] = std::make_tuple( 0.5, 6.5, "1CT_M4", "A" );
  UT_CoordinatesMap[7][1][0][0][0] = std::make_tuple( 0.5, 5.5, "1CT_S4", "A" );
  UT_CoordinatesMap[6][0][0][0][0] = std::make_tuple( 0.5, 4.5, "1CT_M3", "A" );
  UT_CoordinatesMap[6][1][0][0][0] = std::make_tuple( 0.5, 3.5, "1CT_S3", "A" );
  UT_CoordinatesMap[5][0][0][0][0] = std::make_tuple( 0.5, 2.5, "1CT_M2", "A" );
  UT_CoordinatesMap[5][1][0][0][1] = std::make_tuple( 0.75, 1.5, "1CT_S2E", "B" );
  UT_CoordinatesMap[5][1][0][0][0] = std::make_tuple( 0.25, 1.5, "1CT_S2W", "B" );
  UT_CoordinatesMap[4][0][0][0][1] = std::make_tuple( 0.75, 0.75, "1CT_M1E", "C" );
  UT_CoordinatesMap[4][0][0][0][0] = std::make_tuple( 0.25, 0.75, "1CT_M1W", "C" );
  UT_CoordinatesMap[4][1][0][0][1] = std::make_tuple( 0.75, 0.25, "1CT_S1E", "C" );
  UT_CoordinatesMap[4][1][0][0][0] = std::make_tuple( 0.25, 0.25, "1CT_S1W", "C" );
  UT_CoordinatesMap[3][0][0][0][1] = std::make_tuple( 0.75, -0.25, "1CB_S1E", "C" );
  UT_CoordinatesMap[3][0][0][0][0] = std::make_tuple( 0.25, -0.25, "1CB_S1W", "C" );
  UT_CoordinatesMap[3][1][0][0][1] = std::make_tuple( 0.75, -0.75, "1CB_M1E", "C" );
  UT_CoordinatesMap[3][1][0][0][0] = std::make_tuple( 0.25, -0.75, "1CB_M1W", "C" );
  UT_CoordinatesMap[2][0][0][0][1] = std::make_tuple( 0.75, -1.5, "1CB_S2E", "B" );
  UT_CoordinatesMap[2][0][0][0][0] = std::make_tuple( 0.25, -1.5, "1CB_S2W", "B" );
  UT_CoordinatesMap[2][1][0][0][0] = std::make_tuple( 0.5, -2.5, "1CB_M2", "A" );
  UT_CoordinatesMap[1][0][0][0][0] = std::make_tuple( 0.5, -3.5, "1CB_S3", "A" );
  UT_CoordinatesMap[1][1][0][0][0] = std::make_tuple( 0.5, -4.5, "1CB_M3", "A" );
  UT_CoordinatesMap[0][0][0][0][0] = std::make_tuple( 0.5, -5.5, "1CB_S4", "A" );
  UT_CoordinatesMap[0][1][0][0][0] = std::make_tuple( 0.5, -6.5, "1CB_M4", "A" );
  UT_CoordinatesMap[7][0][1][1][0] = std::make_tuple( -1.5, 6.5, "2AT_M4", "A" );
  UT_CoordinatesMap[7][1][1][1][0] = std::make_tuple( -1.5, 5.5, "2AT_S3", "A" );
  UT_CoordinatesMap[6][0][1][1][0] = std::make_tuple( -1.5, 4.5, "2AT_M3", "A" );
  UT_CoordinatesMap[6][1][1][1][0] = std::make_tuple( -1.5, 3.5, "2AT_S2", "A" );
  UT_CoordinatesMap[5][0][1][1][0] = std::make_tuple( -1.5, 2.5, "2AT_M2", "A" );
  UT_CoordinatesMap[5][1][1][1][1] = std::make_tuple( -1.25, 1.5, "2AT_S1E", "B" );
  UT_CoordinatesMap[5][1][1][1][0] = std::make_tuple( -1.75, 1.5, "2AT_S1W", "B" );
  UT_CoordinatesMap[4][0][1][1][1] = std::make_tuple( -1.25, 0.5, "2AT_M1E", "B" );
  UT_CoordinatesMap[4][0][1][1][0] = std::make_tuple( -1.75, 0.5, "2AT_M1W", "B" );
  UT_CoordinatesMap[3][1][1][1][1] = std::make_tuple( -1.25, -0.5, "2AB_M1E", "B" );
  UT_CoordinatesMap[3][1][1][1][0] = std::make_tuple( -1.75, -0.5, "2AB_M1W", "B" );
  UT_CoordinatesMap[2][0][1][1][1] = std::make_tuple( -1.25, -1.5, "2AB_S1E", "B" );
  UT_CoordinatesMap[2][0][1][1][0] = std::make_tuple( -1.75, -1.5, "2AB_S1W", "B" );
  UT_CoordinatesMap[2][1][1][1][0] = std::make_tuple( -1.5, -2.5, "2AB_M2", "A" );
  UT_CoordinatesMap[1][0][1][1][0] = std::make_tuple( -1.5, -3.5, "2AB_S2", "A" );
  UT_CoordinatesMap[1][1][1][1][0] = std::make_tuple( -1.5, -4.5, "2AB_M3", "A" );
  UT_CoordinatesMap[0][0][1][1][0] = std::make_tuple( -1.5, -5.5, "2AB_S3", "A" );
  UT_CoordinatesMap[0][1][1][1][0] = std::make_tuple( -1.5, -6.5, "2AB_M4", "A" );
  UT_CoordinatesMap[7][0][1][0][0] = std::make_tuple( 1.5, 6.5, "2CT_M4", "A" );
  UT_CoordinatesMap[7][1][1][0][0] = std::make_tuple( 1.5, 5.5, "2CT_S3", "A" );
  UT_CoordinatesMap[6][0][1][0][0] = std::make_tuple( 1.5, 4.5, "2CT_M3", "A" );
  UT_CoordinatesMap[6][1][1][0][0] = std::make_tuple( 1.5, 3.5, "2CT_S2", "A" );
  UT_CoordinatesMap[5][0][1][0][0] = std::make_tuple( 1.5, 2.5, "2CT_M2", "A" );
  UT_CoordinatesMap[5][1][1][0][1] = std::make_tuple( 1.75, 1.5, "2CT_S1E", "B" );
  UT_CoordinatesMap[5][1][1][0][0] = std::make_tuple( 1.25, 1.5, "2CT_S1W", "B" );
  UT_CoordinatesMap[4][0][1][0][1] = std::make_tuple( 1.75, 0.5, "2CT_M1E", "B" );
  UT_CoordinatesMap[4][0][1][0][0] = std::make_tuple( 1.25, 0.5, "2CT_M1W", "B" );
  UT_CoordinatesMap[3][1][1][0][1] = std::make_tuple( 1.75, -0.5, "2CB_M1E", "B" );
  UT_CoordinatesMap[3][1][1][0][0] = std::make_tuple( 1.25, -0.5, "2CB_M1W", "B" );
  UT_CoordinatesMap[2][0][1][0][1] = std::make_tuple( 1.75, -1.5, "2CB_S1E", "B" );
  UT_CoordinatesMap[2][0][1][0][0] = std::make_tuple( 1.25, -1.5, "2CB_S1W", "B" );
  UT_CoordinatesMap[2][1][1][0][0] = std::make_tuple( 1.5, -2.5, "2CB_M2", "A" );
  UT_CoordinatesMap[1][0][1][0][0] = std::make_tuple( 1.5, -3.5, "2CB_S2", "A" );
  UT_CoordinatesMap[1][1][1][0][0] = std::make_tuple( 1.5, -4.5, "2CB_M3", "A" );
  UT_CoordinatesMap[0][0][1][0][0] = std::make_tuple( 1.5, -5.5, "2CB_S3", "A" );
  UT_CoordinatesMap[0][1][1][0][0] = std::make_tuple( 1.5, -6.5, "2CB_M4", "A" );
  UT_CoordinatesMap[7][0][2][1][0] = std::make_tuple( -2.5, 6.5, "3AT_M4", "A" );
  UT_CoordinatesMap[7][1][2][1][0] = std::make_tuple( -2.5, 5.5, "3AT_S3", "A" );
  UT_CoordinatesMap[6][0][2][1][0] = std::make_tuple( -2.5, 4.5, "3AT_M3", "A" );
  UT_CoordinatesMap[6][1][2][1][0] = std::make_tuple( -2.5, 3.5, "3AT_S2", "A" );
  UT_CoordinatesMap[5][0][2][1][0] = std::make_tuple( -2.5, 2.5, "3AT_M2", "A" );
  UT_CoordinatesMap[5][1][2][1][0] = std::make_tuple( -2.5, 1.5, "3AT_S1", "A" );
  UT_CoordinatesMap[4][0][2][1][0] = std::make_tuple( -2.5, 0.5, "3AT_M1", "A" );
  UT_CoordinatesMap[3][1][2][1][0] = std::make_tuple( -2.5, -0.5, "3AB_M1", "A" );
  UT_CoordinatesMap[2][0][2][1][0] = std::make_tuple( -2.5, -1.5, "3AB_S1", "A" );
  UT_CoordinatesMap[2][1][2][1][0] = std::make_tuple( -2.5, -2.5, "3AB_M2", "A" );
  UT_CoordinatesMap[1][0][2][1][0] = std::make_tuple( -2.5, -3.5, "3AB_S2", "A" );
  UT_CoordinatesMap[1][1][2][1][0] = std::make_tuple( -2.5, -4.5, "3AB_M3", "A" );
  UT_CoordinatesMap[0][0][2][1][0] = std::make_tuple( -2.5, -5.5, "3AB_S3", "A" );
  UT_CoordinatesMap[0][1][2][1][0] = std::make_tuple( -2.5, -6.5, "3AB_M4", "A" );
  UT_CoordinatesMap[7][0][2][0][0] = std::make_tuple( 2.5, 6.5, "3CT_M4", "A" );
  UT_CoordinatesMap[7][1][2][0][0] = std::make_tuple( 2.5, 5.5, "3CT_S3", "A" );
  UT_CoordinatesMap[6][0][2][0][0] = std::make_tuple( 2.5, 4.5, "3CT_M3", "A" );
  UT_CoordinatesMap[6][1][2][0][0] = std::make_tuple( 2.5, 3.5, "3CT_S2", "A" );
  UT_CoordinatesMap[5][0][2][0][0] = std::make_tuple( 2.5, 2.5, "3CT_M2", "A" );
  UT_CoordinatesMap[5][1][2][0][0] = std::make_tuple( 2.5, 1.5, "3CT_S1", "A" );
  UT_CoordinatesMap[4][0][2][0][0] = std::make_tuple( 2.5, 0.5, "3CT_M1", "A" );
  UT_CoordinatesMap[3][1][2][0][0] = std::make_tuple( 2.5, -0.5, "3CB_M1", "A" );
  UT_CoordinatesMap[2][0][2][0][0] = std::make_tuple( 2.5, -1.5, "3CB_S1", "A" );
  UT_CoordinatesMap[2][1][2][0][0] = std::make_tuple( 2.5, -2.5, "3CB_M2", "A" );
  UT_CoordinatesMap[1][0][2][0][0] = std::make_tuple( 2.5, -3.5, "3CB_S2", "A" );
  UT_CoordinatesMap[1][1][2][0][0] = std::make_tuple( 2.5, -4.5, "3CB_M3", "A" );
  UT_CoordinatesMap[0][0][2][0][0] = std::make_tuple( 2.5, -5.5, "3CB_S3", "A" );
  UT_CoordinatesMap[0][1][2][0][0] = std::make_tuple( 2.5, -6.5, "3CB_M4", "A" );
  UT_CoordinatesMap[7][0][3][1][0] = std::make_tuple( -3.5, 6.5, "4AT_M4", "A" );
  UT_CoordinatesMap[7][1][3][1][0] = std::make_tuple( -3.5, 5.5, "4AT_S3", "A" );
  UT_CoordinatesMap[6][0][3][1][0] = std::make_tuple( -3.5, 4.5, "4AT_M3", "A" );
  UT_CoordinatesMap[6][1][3][1][0] = std::make_tuple( -3.5, 3.5, "4AT_S2", "A" );
  UT_CoordinatesMap[5][0][3][1][0] = std::make_tuple( -3.5, 2.5, "4AT_M2", "A" );
  UT_CoordinatesMap[5][1][3][1][0] = std::make_tuple( -3.5, 1.5, "4AT_S1", "A" );
  UT_CoordinatesMap[4][0][3][1][0] = std::make_tuple( -3.5, 0.5, "4AT_M1", "A" );
  UT_CoordinatesMap[3][1][3][1][0] = std::make_tuple( -3.5, -0.5, "4AB_M1", "A" );
  UT_CoordinatesMap[2][0][3][1][0] = std::make_tuple( -3.5, -1.5, "4AB_S1", "A" );
  UT_CoordinatesMap[2][1][3][1][0] = std::make_tuple( -3.5, -2.5, "4AB_M2", "A" );
  UT_CoordinatesMap[1][0][3][1][0] = std::make_tuple( -3.5, -3.5, "4AB_S2", "A" );
  UT_CoordinatesMap[1][1][3][1][0] = std::make_tuple( -3.5, -4.5, "4AB_M3", "A" );
  UT_CoordinatesMap[0][0][3][1][0] = std::make_tuple( -3.5, -5.5, "4AB_S3", "A" );
  UT_CoordinatesMap[0][1][3][1][0] = std::make_tuple( -3.5, -6.5, "4AB_M4", "A" );
  UT_CoordinatesMap[7][0][3][0][0] = std::make_tuple( 3.5, 6.5, "4CT_M4", "A" );
  UT_CoordinatesMap[7][1][3][0][0] = std::make_tuple( 3.5, 5.5, "4CT_S3", "A" );
  UT_CoordinatesMap[6][0][3][0][0] = std::make_tuple( 3.5, 4.5, "4CT_M3", "A" );
  UT_CoordinatesMap[6][1][3][0][0] = std::make_tuple( 3.5, 3.5, "4CT_S2", "A" );
  UT_CoordinatesMap[5][0][3][0][0] = std::make_tuple( 3.5, 2.5, "4CT_M2", "A" );
  UT_CoordinatesMap[5][1][3][0][0] = std::make_tuple( 3.5, 1.5, "4CT_S1", "A" );
  UT_CoordinatesMap[4][0][3][0][0] = std::make_tuple( 3.5, 0.5, "4CT_M1", "A" );
  UT_CoordinatesMap[3][1][3][0][0] = std::make_tuple( 3.5, -0.5, "4CB_M1", "A" );
  UT_CoordinatesMap[2][0][3][0][0] = std::make_tuple( 3.5, -1.5, "4CB_S1", "A" );
  UT_CoordinatesMap[2][1][3][0][0] = std::make_tuple( 3.5, -2.5, "4CB_M2", "A" );
  UT_CoordinatesMap[1][0][3][0][0] = std::make_tuple( 3.5, -3.5, "4CB_S2", "A" );
  UT_CoordinatesMap[1][1][3][0][0] = std::make_tuple( 3.5, -4.5, "4CB_M3", "A" );
  UT_CoordinatesMap[0][0][3][0][0] = std::make_tuple( 3.5, -5.5, "4CB_S3", "A" );
  UT_CoordinatesMap[0][1][3][0][0] = std::make_tuple( 3.5, -6.5, "4CB_M4", "A" );
  UT_CoordinatesMap[7][0][4][1][0] = std::make_tuple( -4.5, 6.5, "5AT_M4", "A" );
  UT_CoordinatesMap[7][1][4][1][0] = std::make_tuple( -4.5, 5.5, "5AT_S3", "A" );
  UT_CoordinatesMap[6][0][4][1][0] = std::make_tuple( -4.5, 4.5, "5AT_M3", "A" );
  UT_CoordinatesMap[6][1][4][1][0] = std::make_tuple( -4.5, 3.5, "5AT_S2", "A" );
  UT_CoordinatesMap[5][0][4][1][0] = std::make_tuple( -4.5, 2.5, "5AT_M2", "A" );
  UT_CoordinatesMap[5][1][4][1][0] = std::make_tuple( -4.5, 1.5, "5AT_S1", "A" );
  UT_CoordinatesMap[4][0][4][1][0] = std::make_tuple( -4.5, 0.5, "5AT_M1", "A" );
  UT_CoordinatesMap[3][1][4][1][0] = std::make_tuple( -4.5, -0.5, "5AB_M1", "A" );
  UT_CoordinatesMap[2][0][4][1][0] = std::make_tuple( -4.5, -1.5, "5AB_S1", "A" );
  UT_CoordinatesMap[2][1][4][1][0] = std::make_tuple( -4.5, -2.5, "5AB_M2", "A" );
  UT_CoordinatesMap[1][0][4][1][0] = std::make_tuple( -4.5, -3.5, "5AB_S2", "A" );
  UT_CoordinatesMap[1][1][4][1][0] = std::make_tuple( -4.5, -4.5, "5AB_M3", "A" );
  UT_CoordinatesMap[0][0][4][1][0] = std::make_tuple( -4.5, -5.5, "5AB_S3", "A" );
  UT_CoordinatesMap[0][1][4][1][0] = std::make_tuple( -4.5, -6.5, "5AB_M4", "A" );
  UT_CoordinatesMap[7][0][4][0][0] = std::make_tuple( 4.5, 6.5, "5CT_M4", "A" );
  UT_CoordinatesMap[7][1][4][0][0] = std::make_tuple( 4.5, 5.5, "5CT_S3", "A" );
  UT_CoordinatesMap[6][0][4][0][0] = std::make_tuple( 4.5, 4.5, "5CT_M3", "A" );
  UT_CoordinatesMap[6][1][4][0][0] = std::make_tuple( 4.5, 3.5, "5CT_S2", "A" );
  UT_CoordinatesMap[5][0][4][0][0] = std::make_tuple( 4.5, 2.5, "5CT_M2", "A" );
  UT_CoordinatesMap[5][1][4][0][0] = std::make_tuple( 4.5, 1.5, "5CT_S1", "A" );
  UT_CoordinatesMap[4][0][4][0][0] = std::make_tuple( 4.5, 0.5, "5CT_M1", "A" );
  UT_CoordinatesMap[3][1][4][0][0] = std::make_tuple( 4.5, -0.5, "5CB_M1", "A" );
  UT_CoordinatesMap[2][0][4][0][0] = std::make_tuple( 4.5, -1.5, "5CB_S1", "A" );
  UT_CoordinatesMap[2][1][4][0][0] = std::make_tuple( 4.5, -2.5, "5CB_M2", "A" );
  UT_CoordinatesMap[1][0][4][0][0] = std::make_tuple( 4.5, -3.5, "5CB_S2", "A" );
  UT_CoordinatesMap[1][1][4][0][0] = std::make_tuple( 4.5, -4.5, "5CB_M3", "A" );
  UT_CoordinatesMap[0][0][4][0][0] = std::make_tuple( 4.5, -5.5, "5CB_S3", "A" );
  UT_CoordinatesMap[0][1][4][0][0] = std::make_tuple( 4.5, -6.5, "5CB_M4", "A" );
  UT_CoordinatesMap[7][0][5][1][0] = std::make_tuple( -5.5, 6.5, "6AT_M4", "A" );
  UT_CoordinatesMap[7][1][5][1][0] = std::make_tuple( -5.5, 5.5, "6AT_S3", "A" );
  UT_CoordinatesMap[6][0][5][1][0] = std::make_tuple( -5.5, 4.5, "6AT_M3", "A" );
  UT_CoordinatesMap[6][1][5][1][0] = std::make_tuple( -5.5, 3.5, "6AT_S2", "A" );
  UT_CoordinatesMap[5][0][5][1][0] = std::make_tuple( -5.5, 2.5, "6AT_M2", "A" );
  UT_CoordinatesMap[5][1][5][1][0] = std::make_tuple( -5.5, 1.5, "6AT_S1", "A" );
  UT_CoordinatesMap[4][0][5][1][0] = std::make_tuple( -5.5, 0.5, "6AT_M1", "A" );
  UT_CoordinatesMap[3][1][5][1][0] = std::make_tuple( -5.5, -0.5, "6AB_M1", "A" );
  UT_CoordinatesMap[2][0][5][1][0] = std::make_tuple( -5.5, -1.5, "6AB_S1", "A" );
  UT_CoordinatesMap[2][1][5][1][0] = std::make_tuple( -5.5, -2.5, "6AB_M2", "A" );
  UT_CoordinatesMap[1][0][5][1][0] = std::make_tuple( -5.5, -3.5, "6AB_S2", "A" );
  UT_CoordinatesMap[1][1][5][1][0] = std::make_tuple( -5.5, -4.5, "6AB_M3", "A" );
  UT_CoordinatesMap[0][0][5][1][0] = std::make_tuple( -5.5, -5.5, "6AB_S3", "A" );
  UT_CoordinatesMap[0][1][5][1][0] = std::make_tuple( -5.5, -6.5, "6AB_M4", "A" );
  UT_CoordinatesMap[7][0][5][0][0] = std::make_tuple( 5.5, 6.5, "6CT_M4", "A" );
  UT_CoordinatesMap[7][1][5][0][0] = std::make_tuple( 5.5, 5.5, "6CT_S3", "A" );
  UT_CoordinatesMap[6][0][5][0][0] = std::make_tuple( 5.5, 4.5, "6CT_M3", "A" );
  UT_CoordinatesMap[6][1][5][0][0] = std::make_tuple( 5.5, 3.5, "6CT_S2", "A" );
  UT_CoordinatesMap[5][0][5][0][0] = std::make_tuple( 5.5, 2.5, "6CT_M2", "A" );
  UT_CoordinatesMap[5][1][5][0][0] = std::make_tuple( 5.5, 1.5, "6CT_S1", "A" );
  UT_CoordinatesMap[4][0][5][0][0] = std::make_tuple( 5.5, 0.5, "6CT_M1", "A" );
  UT_CoordinatesMap[3][1][5][0][0] = std::make_tuple( 5.5, -0.5, "6CB_M1", "A" );
  UT_CoordinatesMap[2][0][5][0][0] = std::make_tuple( 5.5, -1.5, "6CB_S1", "A" );
  UT_CoordinatesMap[2][1][5][0][0] = std::make_tuple( 5.5, -2.5, "6CB_M2", "A" );
  UT_CoordinatesMap[1][0][5][0][0] = std::make_tuple( 5.5, -3.5, "6CB_S2", "A" );
  UT_CoordinatesMap[1][1][5][0][0] = std::make_tuple( 5.5, -4.5, "6CB_M3", "A" );
  UT_CoordinatesMap[0][0][5][0][0] = std::make_tuple( 5.5, -5.5, "6CB_S3", "A" );
  UT_CoordinatesMap[0][1][5][0][0] = std::make_tuple( 5.5, -6.5, "6CB_M4", "A" );
  UT_CoordinatesMap[7][0][6][1][0] = std::make_tuple( -6.5, 6.5, "7AT_M4", "A" );
  UT_CoordinatesMap[7][1][6][1][0] = std::make_tuple( -6.5, 5.5, "7AT_S3", "A" );
  UT_CoordinatesMap[6][0][6][1][0] = std::make_tuple( -6.5, 4.5, "7AT_M3", "A" );
  UT_CoordinatesMap[6][1][6][1][0] = std::make_tuple( -6.5, 3.5, "7AT_S2", "A" );
  UT_CoordinatesMap[5][0][6][1][0] = std::make_tuple( -6.5, 2.5, "7AT_M2", "A" );
  UT_CoordinatesMap[5][1][6][1][0] = std::make_tuple( -6.5, 1.5, "7AT_S1", "A" );
  UT_CoordinatesMap[4][0][6][1][0] = std::make_tuple( -6.5, 0.5, "7AT_M1", "A" );
  UT_CoordinatesMap[3][1][6][1][0] = std::make_tuple( -6.5, -0.5, "7AB_M1", "A" );
  UT_CoordinatesMap[2][0][6][1][0] = std::make_tuple( -6.5, -1.5, "7AB_S1", "A" );
  UT_CoordinatesMap[2][1][6][1][0] = std::make_tuple( -6.5, -2.5, "7AB_M2", "A" );
  UT_CoordinatesMap[1][0][6][1][0] = std::make_tuple( -6.5, -3.5, "7AB_S2", "A" );
  UT_CoordinatesMap[1][1][6][1][0] = std::make_tuple( -6.5, -4.5, "7AB_M3", "A" );
  UT_CoordinatesMap[0][0][6][1][0] = std::make_tuple( -6.5, -5.5, "7AB_S3", "A" );
  UT_CoordinatesMap[0][1][6][1][0] = std::make_tuple( -6.5, -6.5, "7AB_M4", "A" );
  UT_CoordinatesMap[7][0][6][0][0] = std::make_tuple( 6.5, 6.5, "7CT_M4", "A" );
  UT_CoordinatesMap[7][1][6][0][0] = std::make_tuple( 6.5, 5.5, "7CT_S3", "A" );
  UT_CoordinatesMap[6][0][6][0][0] = std::make_tuple( 6.5, 4.5, "7CT_M3", "A" );
  UT_CoordinatesMap[6][1][6][0][0] = std::make_tuple( 6.5, 3.5, "7CT_S2", "A" );
  UT_CoordinatesMap[5][0][6][0][0] = std::make_tuple( 6.5, 2.5, "7CT_M2", "A" );
  UT_CoordinatesMap[5][1][6][0][0] = std::make_tuple( 6.5, 1.5, "7CT_S1", "A" );
  UT_CoordinatesMap[4][0][6][0][0] = std::make_tuple( 6.5, 0.5, "7CT_M1", "A" );
  UT_CoordinatesMap[3][1][6][0][0] = std::make_tuple( 6.5, -0.5, "7CB_M1", "A" );
  UT_CoordinatesMap[2][0][6][0][0] = std::make_tuple( 6.5, -1.5, "7CB_S1", "A" );
  UT_CoordinatesMap[2][1][6][0][0] = std::make_tuple( 6.5, -2.5, "7CB_M2", "A" );
  UT_CoordinatesMap[1][0][6][0][0] = std::make_tuple( 6.5, -3.5, "7CB_S2", "A" );
  UT_CoordinatesMap[1][1][6][0][0] = std::make_tuple( 6.5, -4.5, "7CB_M3", "A" );
  UT_CoordinatesMap[0][0][6][0][0] = std::make_tuple( 6.5, -5.5, "7CB_S3", "A" );
  UT_CoordinatesMap[0][1][6][0][0] = std::make_tuple( 6.5, -6.5, "7CB_M4", "A" );
  UT_CoordinatesMap[7][0][7][1][0] = std::make_tuple( -7.5, 6.5, "8AT_M4", "A" );
  UT_CoordinatesMap[7][1][7][1][0] = std::make_tuple( -7.5, 5.5, "8AT_S3", "A" );
  UT_CoordinatesMap[6][0][7][1][0] = std::make_tuple( -7.5, 4.5, "8AT_M3", "A" );
  UT_CoordinatesMap[6][1][7][1][0] = std::make_tuple( -7.5, 3.5, "8AT_S2", "A" );
  UT_CoordinatesMap[5][0][7][1][0] = std::make_tuple( -7.5, 2.5, "8AT_M2", "A" );
  UT_CoordinatesMap[5][1][7][1][0] = std::make_tuple( -7.5, 1.5, "8AT_S1", "A" );
  UT_CoordinatesMap[4][0][7][1][0] = std::make_tuple( -7.5, 0.5, "8AT_M1", "A" );
  UT_CoordinatesMap[3][1][7][1][0] = std::make_tuple( -7.5, -0.5, "8AB_M1", "A" );
  UT_CoordinatesMap[2][0][7][1][0] = std::make_tuple( -7.5, -1.5, "8AB_S1", "A" );
  UT_CoordinatesMap[2][1][7][1][0] = std::make_tuple( -7.5, -2.5, "8AB_M2", "A" );
  UT_CoordinatesMap[1][0][7][1][0] = std::make_tuple( -7.5, -3.5, "8AB_S2", "A" );
  UT_CoordinatesMap[1][1][7][1][0] = std::make_tuple( -7.5, -4.5, "8AB_M3", "A" );
  UT_CoordinatesMap[0][0][7][1][0] = std::make_tuple( -7.5, -5.5, "8AB_S3", "A" );
  UT_CoordinatesMap[0][1][7][1][0] = std::make_tuple( -7.5, -6.5, "8AB_M4", "A" );
  UT_CoordinatesMap[7][0][7][0][0] = std::make_tuple( 7.5, 6.5, "8CT_M4", "A" );
  UT_CoordinatesMap[7][1][7][0][0] = std::make_tuple( 7.5, 5.5, "8CT_S3", "A" );
  UT_CoordinatesMap[6][0][7][0][0] = std::make_tuple( 7.5, 4.5, "8CT_M3", "A" );
  UT_CoordinatesMap[6][1][7][0][0] = std::make_tuple( 7.5, 3.5, "8CT_S2", "A" );
  UT_CoordinatesMap[5][0][7][0][0] = std::make_tuple( 7.5, 2.5, "8CT_M2", "A" );
  UT_CoordinatesMap[5][1][7][0][0] = std::make_tuple( 7.5, 1.5, "8CT_S1", "A" );
  UT_CoordinatesMap[4][0][7][0][0] = std::make_tuple( 7.5, 0.5, "8CT_M1", "A" );
  UT_CoordinatesMap[3][1][7][0][0] = std::make_tuple( 7.5, -0.5, "8CB_M1", "A" );
  UT_CoordinatesMap[2][0][7][0][0] = std::make_tuple( 7.5, -1.5, "8CB_S1", "A" );
  UT_CoordinatesMap[2][1][7][0][0] = std::make_tuple( 7.5, -2.5, "8CB_M2", "A" );
  UT_CoordinatesMap[1][0][7][0][0] = std::make_tuple( 7.5, -3.5, "8CB_S2", "A" );
  UT_CoordinatesMap[1][1][7][0][0] = std::make_tuple( 7.5, -4.5, "8CB_M3", "A" );
  UT_CoordinatesMap[0][0][7][0][0] = std::make_tuple( 7.5, -5.5, "8CB_S3", "A" );
  UT_CoordinatesMap[0][1][7][0][0] = std::make_tuple( 7.5, -6.5, "8CB_M4", "A" );
  UT_CoordinatesMap[7][0][8][1][0] = std::make_tuple( -8.5, 6.5, "9AT_M4", "A" );
  UT_CoordinatesMap[7][1][8][1][0] = std::make_tuple( -8.5, 5.5, "9AT_S3", "A" );
  UT_CoordinatesMap[6][0][8][1][0] = std::make_tuple( -8.5, 4.5, "9AT_M3", "A" );
  UT_CoordinatesMap[6][1][8][1][0] = std::make_tuple( -8.5, 3.5, "9AT_S2", "A" );
  UT_CoordinatesMap[5][0][8][1][0] = std::make_tuple( -8.5, 2.5, "9AT_M2", "A" );
  UT_CoordinatesMap[5][1][8][1][0] = std::make_tuple( -8.5, 1.5, "9AT_S1", "A" );
  UT_CoordinatesMap[4][0][8][1][0] = std::make_tuple( -8.5, 0.5, "9AT_M1", "A" );
  UT_CoordinatesMap[3][1][8][1][0] = std::make_tuple( -8.5, -0.5, "9AB_M1", "A" );
  UT_CoordinatesMap[2][0][8][1][0] = std::make_tuple( -8.5, -1.5, "9AB_S1", "A" );
  UT_CoordinatesMap[2][1][8][1][0] = std::make_tuple( -8.5, -2.5, "9AB_M2", "A" );
  UT_CoordinatesMap[1][0][8][1][0] = std::make_tuple( -8.5, -3.5, "9AB_S2", "A" );
  UT_CoordinatesMap[1][1][8][1][0] = std::make_tuple( -8.5, -4.5, "9AB_M3", "A" );
  UT_CoordinatesMap[0][0][8][1][0] = std::make_tuple( -8.5, -5.5, "9AB_S3", "A" );
  UT_CoordinatesMap[0][1][8][1][0] = std::make_tuple( -8.5, -6.5, "9AB_M4", "A" );
  UT_CoordinatesMap[7][0][8][0][0] = std::make_tuple( 8.5, 6.5, "9CT_M4", "A" );
  UT_CoordinatesMap[7][1][8][0][0] = std::make_tuple( 8.5, 5.5, "9CT_S3", "A" );
  UT_CoordinatesMap[6][0][8][0][0] = std::make_tuple( 8.5, 4.5, "9CT_M3", "A" );
  UT_CoordinatesMap[6][1][8][0][0] = std::make_tuple( 8.5, 3.5, "9CT_S2", "A" );
  UT_CoordinatesMap[5][0][8][0][0] = std::make_tuple( 8.5, 2.5, "9CT_M2", "A" );
  UT_CoordinatesMap[5][1][8][0][0] = std::make_tuple( 8.5, 1.5, "9CT_S1", "A" );
  UT_CoordinatesMap[4][0][8][0][0] = std::make_tuple( 8.5, 0.5, "9CT_M1", "A" );
  UT_CoordinatesMap[3][1][8][0][0] = std::make_tuple( 8.5, -0.5, "9CB_M1", "A" );
  UT_CoordinatesMap[2][0][8][0][0] = std::make_tuple( 8.5, -1.5, "9CB_S1", "A" );
  UT_CoordinatesMap[2][1][8][0][0] = std::make_tuple( 8.5, -2.5, "9CB_M2", "A" );
  UT_CoordinatesMap[1][0][8][0][0] = std::make_tuple( 8.5, -3.5, "9CB_S2", "A" );
  UT_CoordinatesMap[1][1][8][0][0] = std::make_tuple( 8.5, -4.5, "9CB_M3", "A" );
  UT_CoordinatesMap[0][0][8][0][0] = std::make_tuple( 8.5, -5.5, "9CB_S3", "A" );
  UT_CoordinatesMap[0][1][8][0][0] = std::make_tuple( 8.5, -6.5, "9CB_M4", "A" );

  UT_TELL40toModuleName = {
      { "UATEL011_0", "UTbX_7AT_M2" },  { "UATEL011_0", "UTbX_7AT_M3" },  { "UATEL011_0", "UTbX_7AT_M4" },
      { "UATEL011_0", "UTbX_7AT_S1" },  { "UATEL011_0", "UTbX_7AT_S2" },  { "UATEL011_0", "UTbX_7AT_S3" },
      { "UATEL011_1", "UTbV_7AT_M2" },  { "UATEL011_1", "UTbV_7AT_M3" },  { "UATEL011_1", "UTbV_7AT_M4" },
      { "UATEL011_1", "UTbV_7AT_S1" },  { "UATEL011_1", "UTbV_7AT_S2" },  { "UATEL011_1", "UTbV_7AT_S3" },
      { "UATEL012_0", "UTbX_8AT_M2" },  { "UATEL012_0", "UTbX_8AT_M3" },  { "UATEL012_0", "UTbX_8AT_M4" },
      { "UATEL012_0", "UTbX_8AT_S1" },  { "UATEL012_0", "UTbX_8AT_S2" },  { "UATEL012_0", "UTbX_8AT_S3" },
      { "UATEL012_1", "UTbV_8AT_M2" },  { "UATEL012_1", "UTbV_8AT_M3" },  { "UATEL012_1", "UTbV_8AT_M4" },
      { "UATEL012_1", "UTbV_8AT_S1" },  { "UATEL012_1", "UTbV_8AT_S2" },  { "UATEL012_1", "UTbV_8AT_S3" },
      { "UATEL013_0", "UTbX_9AT_M2" },  { "UATEL013_0", "UTbX_9AT_M3" },  { "UATEL013_0", "UTbX_9AT_M4" },
      { "UATEL013_0", "UTbX_9AT_S1" },  { "UATEL013_0", "UTbX_9AT_S2" },  { "UATEL013_0", "UTbX_9AT_S3" },
      { "UATEL013_1", "UTbV_9AT_M2" },  { "UATEL013_1", "UTbV_9AT_M3" },  { "UATEL013_1", "UTbV_9AT_M4" },
      { "UATEL013_1", "UTbV_9AT_S1" },  { "UATEL013_1", "UTbV_9AT_S2" },  { "UATEL013_1", "UTbV_9AT_S3" },
      { "UATEL021_0", "UTbX_4AT_M2" },  { "UATEL021_0", "UTbX_4AT_M3" },  { "UATEL021_0", "UTbX_4AT_M4" },
      { "UATEL021_0", "UTbX_4AT_S1" },  { "UATEL021_0", "UTbX_4AT_S2" },  { "UATEL021_0", "UTbX_4AT_S3" },
      { "UATEL021_1", "UTbV_4AT_M2" },  { "UATEL021_1", "UTbV_4AT_M3" },  { "UATEL021_1", "UTbV_4AT_M4" },
      { "UATEL021_1", "UTbV_4AT_S1" },  { "UATEL021_1", "UTbV_4AT_S2" },  { "UATEL021_1", "UTbV_4AT_S3" },
      { "UATEL022_0", "UTbX_5AT_M2" },  { "UATEL022_0", "UTbX_5AT_M3" },  { "UATEL022_0", "UTbX_5AT_M4" },
      { "UATEL022_0", "UTbX_5AT_S1" },  { "UATEL022_0", "UTbX_5AT_S2" },  { "UATEL022_0", "UTbX_5AT_S3" },
      { "UATEL022_1", "UTbV_5AT_M2" },  { "UATEL022_1", "UTbV_5AT_M3" },  { "UATEL022_1", "UTbV_5AT_M4" },
      { "UATEL022_1", "UTbV_5AT_S1" },  { "UATEL022_1", "UTbV_5AT_S2" },  { "UATEL022_1", "UTbV_5AT_S3" },
      { "UATEL023_0", "UTbX_6AT_M2" },  { "UATEL023_0", "UTbX_6AT_M3" },  { "UATEL023_0", "UTbX_6AT_M4" },
      { "UATEL023_0", "UTbX_6AT_S1" },  { "UATEL023_0", "UTbX_6AT_S2" },  { "UATEL023_0", "UTbX_6AT_S3" },
      { "UATEL023_1", "UTbV_6AT_M2" },  { "UATEL023_1", "UTbV_6AT_M3" },  { "UATEL023_1", "UTbV_6AT_M4" },
      { "UATEL023_1", "UTbV_6AT_S1" },  { "UATEL023_1", "UTbV_6AT_S2" },  { "UATEL023_1", "UTbV_6AT_S3" },
      { "UATEL031_0", "UTbX_8AB_M2" },  { "UATEL031_0", "UTbX_8AB_M3" },  { "UATEL031_0", "UTbX_8AB_M4" },
      { "UATEL031_0", "UTbX_8AB_S1" },  { "UATEL031_0", "UTbX_8AB_S2" },  { "UATEL031_0", "UTbX_8AB_S3" },
      { "UATEL031_1", "UTbV_8AB_M2" },  { "UATEL031_1", "UTbV_8AB_M3" },  { "UATEL031_1", "UTbV_8AB_M4" },
      { "UATEL031_1", "UTbV_8AB_S1" },  { "UATEL031_1", "UTbV_8AB_S2" },  { "UATEL031_1", "UTbV_8AB_S3" },
      { "UATEL032_0", "UTbX_9AB_M2" },  { "UATEL032_0", "UTbX_9AB_M3" },  { "UATEL032_0", "UTbX_9AB_M4" },
      { "UATEL032_0", "UTbX_9AB_S1" },  { "UATEL032_0", "UTbX_9AB_S2" },  { "UATEL032_0", "UTbX_9AB_S3" },
      { "UATEL032_1", "UTbV_9AB_M2" },  { "UATEL032_1", "UTbV_9AB_M3" },  { "UATEL032_1", "UTbV_9AB_M4" },
      { "UATEL032_1", "UTbV_9AB_S1" },  { "UATEL032_1", "UTbV_9AB_S2" },  { "UATEL032_1", "UTbV_9AB_S3" },
      { "UATEL033_0", "UTbX_4AT_M1" },  { "UATEL033_0", "UTbX_5AT_M1" },  { "UATEL033_0", "UTbX_6AT_M1" },
      { "UATEL033_0", "UTbX_7AT_M1" },  { "UATEL033_0", "UTbX_8AT_M1" },  { "UATEL033_0", "UTbX_9AT_M1" },
      { "UATEL033_1", "UTbV_4AT_M1" },  { "UATEL033_1", "UTbV_5AT_M1" },  { "UATEL033_1", "UTbV_6AT_M1" },
      { "UATEL033_1", "UTbV_7AT_M1" },  { "UATEL033_1", "UTbV_8AT_M1" },  { "UATEL033_1", "UTbV_9AT_M1" },
      { "UATEL041_0", "UTbX_5AB_M2" },  { "UATEL041_0", "UTbX_5AB_M3" },  { "UATEL041_0", "UTbX_5AB_M4" },
      { "UATEL041_0", "UTbX_5AB_S1" },  { "UATEL041_0", "UTbX_5AB_S2" },  { "UATEL041_0", "UTbX_5AB_S3" },
      { "UATEL041_1", "UTbV_5AB_M2" },  { "UATEL041_1", "UTbV_5AB_M3" },  { "UATEL041_1", "UTbV_5AB_M4" },
      { "UATEL041_1", "UTbV_5AB_S1" },  { "UATEL041_1", "UTbV_5AB_S2" },  { "UATEL041_1", "UTbV_5AB_S3" },
      { "UATEL042_0", "UTbX_6AB_M2" },  { "UATEL042_0", "UTbX_6AB_M3" },  { "UATEL042_0", "UTbX_6AB_M4" },
      { "UATEL042_0", "UTbX_6AB_S1" },  { "UATEL042_0", "UTbX_6AB_S2" },  { "UATEL042_0", "UTbX_6AB_S3" },
      { "UATEL042_1", "UTbV_6AB_M2" },  { "UATEL042_1", "UTbV_6AB_M3" },  { "UATEL042_1", "UTbV_6AB_M4" },
      { "UATEL042_1", "UTbV_6AB_S1" },  { "UATEL042_1", "UTbV_6AB_S2" },  { "UATEL042_1", "UTbV_6AB_S3" },
      { "UATEL043_0", "UTbX_7AB_M2" },  { "UATEL043_0", "UTbX_7AB_M3" },  { "UATEL043_0", "UTbX_7AB_M4" },
      { "UATEL043_0", "UTbX_7AB_S1" },  { "UATEL043_0", "UTbX_7AB_S2" },  { "UATEL043_0", "UTbX_7AB_S3" },
      { "UATEL043_1", "UTbV_7AB_M2" },  { "UATEL043_1", "UTbV_7AB_M3" },  { "UATEL043_1", "UTbV_7AB_M4" },
      { "UATEL043_1", "UTbV_7AB_S1" },  { "UATEL043_1", "UTbV_7AB_S2" },  { "UATEL043_1", "UTbV_7AB_S3" },
      { "UATEL051_0", "UTbX_3AT_M2" },  { "UATEL051_0", "UTbX_3AT_M3" },  { "UATEL051_0", "UTbX_3AT_M4" },
      { "UATEL051_0", "UTbX_3AT_S1" },  { "UATEL051_0", "UTbX_3AT_S2" },  { "UATEL051_0", "UTbX_3AT_S3" },
      { "UATEL051_1", "UTbV_3AT_M2" },  { "UATEL051_1", "UTbV_3AT_M3" },  { "UATEL051_1", "UTbV_3AT_M4" },
      { "UATEL051_1", "UTbV_3AT_S1" },  { "UATEL051_1", "UTbV_3AT_S2" },  { "UATEL051_1", "UTbV_3AT_S3" },
      { "UATEL052_0", "UTbX_4AB_M1" },  { "UATEL052_0", "UTbX_5AB_M1" },  { "UATEL052_0", "UTbX_6AB_M1" },
      { "UATEL052_0", "UTbX_7AB_M1" },  { "UATEL052_0", "UTbX_8AB_M1" },  { "UATEL052_0", "UTbX_9AB_M1" },
      { "UATEL052_1", "UTbV_4AB_M1" },  { "UATEL052_1", "UTbV_5AB_M1" },  { "UATEL052_1", "UTbV_6AB_M1" },
      { "UATEL052_1", "UTbV_7AB_M1" },  { "UATEL052_1", "UTbV_8AB_M1" },  { "UATEL052_1", "UTbV_9AB_M1" },
      { "UATEL053_0", "UTbX_4AB_M2" },  { "UATEL053_0", "UTbX_4AB_M3" },  { "UATEL053_0", "UTbX_4AB_M4" },
      { "UATEL053_0", "UTbX_4AB_S1" },  { "UATEL053_0", "UTbX_4AB_S2" },  { "UATEL053_0", "UTbX_4AB_S3" },
      { "UATEL053_1", "UTbV_4AB_M2" },  { "UATEL053_1", "UTbV_4AB_M3" },  { "UATEL053_1", "UTbV_4AB_M4" },
      { "UATEL053_1", "UTbV_4AB_S1" },  { "UATEL053_1", "UTbV_4AB_S2" },  { "UATEL053_1", "UTbV_4AB_S3" },
      { "UATEL061_0", "UTbX_3AB_M2" },  { "UATEL061_0", "UTbX_3AB_M3" },  { "UATEL061_0", "UTbX_3AB_M4" },
      { "UATEL061_0", "UTbX_3AB_S1" },  { "UATEL061_0", "UTbX_3AB_S2" },  { "UATEL061_0", "UTbX_3AB_S3" },
      { "UATEL061_1", "UTbV_3AB_M2" },  { "UATEL061_1", "UTbV_3AB_M3" },  { "UATEL061_1", "UTbV_3AB_M4" },
      { "UATEL061_1", "UTbV_3AB_S1" },  { "UATEL061_1", "UTbV_3AB_S2" },  { "UATEL061_1", "UTbV_3AB_S3" },
      { "UATEL062_0", "UTbX_1AT_M2" },  { "UATEL062_0", "UTbX_1AT_M3" },  { "UATEL062_0", "UTbX_1AT_M4" },
      { "UATEL062_0", "UTbX_1AT_S3" },  { "UATEL062_0", "UTbX_1AT_S4" },  { "UATEL062_0", "UTbX_2AT_M4" },
      { "UATEL062_1", "UTbV_1AT_M2" },  { "UATEL062_1", "UTbV_1AT_M3" },  { "UATEL062_1", "UTbV_1AT_M4" },
      { "UATEL062_1", "UTbV_1AT_S3" },  { "UATEL062_1", "UTbV_1AT_S4" },  { "UATEL062_1", "UTbV_2AT_M4" },
      { "UATEL063_0", "UTbX_2AT_M2" },  { "UATEL063_0", "UTbX_2AT_M3" },  { "UATEL063_0", "UTbX_2AT_S1E" },
      { "UATEL063_0", "UTbX_2AT_S1W" }, { "UATEL063_0", "UTbX_2AT_S2" },  { "UATEL063_0", "UTbX_2AT_S3" },
      { "UATEL063_1", "UTbV_2AT_M2" },  { "UATEL063_1", "UTbV_2AT_M3" },  { "UATEL063_1", "UTbV_2AT_S1E" },
      { "UATEL063_1", "UTbV_2AT_S1W" }, { "UATEL063_1", "UTbV_2AT_S2" },  { "UATEL063_1", "UTbV_2AT_S3" },
      { "UATEL071_0", "UTbX_1AB_M2" },  { "UATEL071_0", "UTbX_1AB_M3" },  { "UATEL071_0", "UTbX_1AB_M4" },
      { "UATEL071_0", "UTbX_1AB_S3" },  { "UATEL071_0", "UTbX_1AB_S4" },  { "UATEL071_0", "UTbX_2AB_M4" },
      { "UATEL071_1", "UTbV_1AB_M2" },  { "UATEL071_1", "UTbV_1AB_M3" },  { "UATEL071_1", "UTbV_1AB_M4" },
      { "UATEL071_1", "UTbV_1AB_S3" },  { "UATEL071_1", "UTbV_1AB_S4" },  { "UATEL071_1", "UTbV_2AB_M4" },
      { "UATEL072_0", "UTbX_2AB_M2" },  { "UATEL072_0", "UTbX_2AB_M3" },  { "UATEL072_0", "UTbX_2AB_S1E" },
      { "UATEL072_0", "UTbX_2AB_S1W" }, { "UATEL072_0", "UTbX_2AB_S2" },  { "UATEL072_0", "UTbX_2AB_S3" },
      { "UATEL072_1", "UTbV_2AB_M2" },  { "UATEL072_1", "UTbV_2AB_M3" },  { "UATEL072_1", "UTbV_2AB_S1E" },
      { "UATEL072_1", "UTbV_2AB_S1W" }, { "UATEL072_1", "UTbV_2AB_S2" },  { "UATEL072_1", "UTbV_2AB_S3" },
      { "UATEL073_0", "UTbV_1AT_M1E" }, { "UATEL073_0", "UTbV_1AT_M1W" }, { "UATEL073_0", "UTbV_1AT_S2E" },
      { "UATEL073_0", "UTbV_1AT_S2W" }, { "UATEL073_0", "UTbV_3AT_M1" },  { "UATEL081_0", "UTbX_1AT_S1E" },
      { "UATEL081_0", "UTbX_1AT_S1W" }, { "UATEL081_0", "UTbX_2AT_M1E" }, { "UATEL081_0", "UTbX_2AT_M1W" },
      { "UATEL082_0", "UTbV_1AT_S1E" }, { "UATEL082_0", "UTbV_1AT_S1W" }, { "UATEL082_0", "UTbV_2AT_M1E" },
      { "UATEL082_0", "UTbV_2AT_M1W" }, { "UATEL083_0", "UTbX_1AT_M1E" }, { "UATEL083_0", "UTbX_1AT_M1W" },
      { "UATEL083_0", "UTbX_1AT_S2E" }, { "UATEL083_0", "UTbX_1AT_S2W" }, { "UATEL083_0", "UTbX_3AT_M1" },
      { "UATEL091_0", "UTbV_1AB_S1E" }, { "UATEL091_0", "UTbV_1AB_S1W" }, { "UATEL091_0", "UTbV_2AB_M1E" },
      { "UATEL091_0", "UTbV_2AB_M1W" }, { "UATEL092_0", "UTbX_1AB_M1E" }, { "UATEL092_0", "UTbX_1AB_M1W" },
      { "UATEL092_0", "UTbX_1AB_S2E" }, { "UATEL092_0", "UTbX_1AB_S2W" }, { "UATEL092_0", "UTbX_3AB_M1" },
      { "UATEL093_0", "UTbV_1AB_M1E" }, { "UATEL093_0", "UTbV_1AB_M1W" }, { "UATEL093_0", "UTbV_1AB_S2E" },
      { "UATEL093_0", "UTbV_1AB_S2W" }, { "UATEL093_0", "UTbV_3AB_M1" },  { "UATEL101_0", "UTaX_7AT_M2" },
      { "UATEL101_0", "UTaX_7AT_M3" },  { "UATEL101_0", "UTaX_7AT_M4" },  { "UATEL101_0", "UTaX_7AT_S1" },
      { "UATEL101_0", "UTaX_7AT_S2" },  { "UATEL101_0", "UTaX_7AT_S3" },  { "UATEL101_1", "UTaU_7AT_M2" },
      { "UATEL101_1", "UTaU_7AT_M3" },  { "UATEL101_1", "UTaU_7AT_M4" },  { "UATEL101_1", "UTaU_7AT_S1" },
      { "UATEL101_1", "UTaU_7AT_S2" },  { "UATEL101_1", "UTaU_7AT_S3" },  { "UATEL102_0", "UTaX_8AT_M2" },
      { "UATEL102_0", "UTaX_8AT_M3" },  { "UATEL102_0", "UTaX_8AT_M4" },  { "UATEL102_0", "UTaX_8AT_S1" },
      { "UATEL102_0", "UTaX_8AT_S2" },  { "UATEL102_0", "UTaX_8AT_S3" },  { "UATEL102_1", "UTaU_8AT_M2" },
      { "UATEL102_1", "UTaU_8AT_M3" },  { "UATEL102_1", "UTaU_8AT_M4" },  { "UATEL102_1", "UTaU_8AT_S1" },
      { "UATEL102_1", "UTaU_8AT_S2" },  { "UATEL102_1", "UTaU_8AT_S3" },  { "UATEL103_0", "UTbX_1AB_S1E" },
      { "UATEL103_0", "UTbX_1AB_S1W" }, { "UATEL103_0", "UTbX_2AB_M1E" }, { "UATEL103_0", "UTbX_2AB_M1W" },
      { "UATEL111_0", "UTaX_4AT_M2" },  { "UATEL111_0", "UTaX_4AT_M3" },  { "UATEL111_0", "UTaX_4AT_M4" },
      { "UATEL111_0", "UTaX_4AT_S1" },  { "UATEL111_0", "UTaX_4AT_S2" },  { "UATEL111_0", "UTaX_4AT_S3" },
      { "UATEL111_1", "UTaU_4AT_M2" },  { "UATEL111_1", "UTaU_4AT_M3" },  { "UATEL111_1", "UTaU_4AT_M4" },
      { "UATEL111_1", "UTaU_4AT_S1" },  { "UATEL111_1", "UTaU_4AT_S2" },  { "UATEL111_1", "UTaU_4AT_S3" },
      { "UATEL112_0", "UTaX_5AT_M2" },  { "UATEL112_0", "UTaX_5AT_M3" },  { "UATEL112_0", "UTaX_5AT_M4" },
      { "UATEL112_0", "UTaX_5AT_S1" },  { "UATEL112_0", "UTaX_5AT_S2" },  { "UATEL112_0", "UTaX_5AT_S3" },
      { "UATEL112_1", "UTaU_5AT_M2" },  { "UATEL112_1", "UTaU_5AT_M3" },  { "UATEL112_1", "UTaU_5AT_M4" },
      { "UATEL112_1", "UTaU_5AT_S1" },  { "UATEL112_1", "UTaU_5AT_S2" },  { "UATEL112_1", "UTaU_5AT_S3" },
      { "UATEL113_0", "UTaX_6AT_M2" },  { "UATEL113_0", "UTaX_6AT_M3" },  { "UATEL113_0", "UTaX_6AT_M4" },
      { "UATEL113_0", "UTaX_6AT_S1" },  { "UATEL113_0", "UTaX_6AT_S2" },  { "UATEL113_0", "UTaX_6AT_S3" },
      { "UATEL113_1", "UTaU_6AT_M2" },  { "UATEL113_1", "UTaU_6AT_M3" },  { "UATEL113_1", "UTaU_6AT_M4" },
      { "UATEL113_1", "UTaU_6AT_S1" },  { "UATEL113_1", "UTaU_6AT_S2" },  { "UATEL113_1", "UTaU_6AT_S3" },
      { "UATEL121_0", "UTaX_7AB_M2" },  { "UATEL121_0", "UTaX_7AB_M3" },  { "UATEL121_0", "UTaX_7AB_M4" },
      { "UATEL121_0", "UTaX_7AB_S1" },  { "UATEL121_0", "UTaX_7AB_S2" },  { "UATEL121_0", "UTaX_7AB_S3" },
      { "UATEL121_1", "UTaU_7AB_M2" },  { "UATEL121_1", "UTaU_7AB_M3" },  { "UATEL121_1", "UTaU_7AB_M4" },
      { "UATEL121_1", "UTaU_7AB_S1" },  { "UATEL121_1", "UTaU_7AB_S2" },  { "UATEL121_1", "UTaU_7AB_S3" },
      { "UATEL122_0", "UTaX_8AB_M2" },  { "UATEL122_0", "UTaX_8AB_M3" },  { "UATEL122_0", "UTaX_8AB_M4" },
      { "UATEL122_0", "UTaX_8AB_S1" },  { "UATEL122_0", "UTaX_8AB_S2" },  { "UATEL122_0", "UTaX_8AB_S3" },
      { "UATEL122_1", "UTaU_8AB_M2" },  { "UATEL122_1", "UTaU_8AB_M3" },  { "UATEL122_1", "UTaU_8AB_M4" },
      { "UATEL122_1", "UTaU_8AB_S1" },  { "UATEL122_1", "UTaU_8AB_S2" },  { "UATEL122_1", "UTaU_8AB_S3" },
      { "UATEL123_0", "UTaX_4AT_M1" },  { "UATEL123_0", "UTaX_5AT_M1" },  { "UATEL123_0", "UTaX_6AT_M1" },
      { "UATEL123_0", "UTaX_7AT_M1" },  { "UATEL123_0", "UTaX_8AT_M1" },  { "UATEL123_1", "UTaU_4AT_M1" },
      { "UATEL123_1", "UTaU_5AT_M1" },  { "UATEL123_1", "UTaU_6AT_M1" },  { "UATEL123_1", "UTaU_7AT_M1" },
      { "UATEL123_1", "UTaU_8AT_M1" },  { "UATEL131_0", "UTaX_4AB_M2" },  { "UATEL131_0", "UTaX_4AB_M3" },
      { "UATEL131_0", "UTaX_4AB_M4" },  { "UATEL131_0", "UTaX_4AB_S1" },  { "UATEL131_0", "UTaX_4AB_S2" },
      { "UATEL131_0", "UTaX_4AB_S3" },  { "UATEL131_1", "UTaU_4AB_M2" },  { "UATEL131_1", "UTaU_4AB_M3" },
      { "UATEL131_1", "UTaU_4AB_M4" },  { "UATEL131_1", "UTaU_4AB_S1" },  { "UATEL131_1", "UTaU_4AB_S2" },
      { "UATEL131_1", "UTaU_4AB_S3" },  { "UATEL132_0", "UTaX_5AB_M2" },  { "UATEL132_0", "UTaX_5AB_M3" },
      { "UATEL132_0", "UTaX_5AB_M4" },  { "UATEL132_0", "UTaX_5AB_S1" },  { "UATEL132_0", "UTaX_5AB_S2" },
      { "UATEL132_0", "UTaX_5AB_S3" },  { "UATEL132_1", "UTaU_5AB_M2" },  { "UATEL132_1", "UTaU_5AB_M3" },
      { "UATEL132_1", "UTaU_5AB_M4" },  { "UATEL132_1", "UTaU_5AB_S1" },  { "UATEL132_1", "UTaU_5AB_S2" },
      { "UATEL132_1", "UTaU_5AB_S3" },  { "UATEL133_0", "UTaX_6AB_M2" },  { "UATEL133_0", "UTaX_6AB_M3" },
      { "UATEL133_0", "UTaX_6AB_M4" },  { "UATEL133_0", "UTaX_6AB_S1" },  { "UATEL133_0", "UTaX_6AB_S2" },
      { "UATEL133_0", "UTaX_6AB_S3" },  { "UATEL133_1", "UTaU_6AB_M2" },  { "UATEL133_1", "UTaU_6AB_M3" },
      { "UATEL133_1", "UTaU_6AB_M4" },  { "UATEL133_1", "UTaU_6AB_S1" },  { "UATEL133_1", "UTaU_6AB_S2" },
      { "UATEL133_1", "UTaU_6AB_S3" },  { "UATEL141_0", "UTaX_2AT_M2" },  { "UATEL141_0", "UTaX_2AT_M3" },
      { "UATEL141_0", "UTaX_2AT_S1E" }, { "UATEL141_0", "UTaX_2AT_S1W" }, { "UATEL141_0", "UTaX_2AT_S2" },
      { "UATEL141_0", "UTaX_2AT_S3" },  { "UATEL141_1", "UTaU_2AT_M2" },  { "UATEL141_1", "UTaU_2AT_M3" },
      { "UATEL141_1", "UTaU_2AT_S1E" }, { "UATEL141_1", "UTaU_2AT_S1W" }, { "UATEL141_1", "UTaU_2AT_S2" },
      { "UATEL141_1", "UTaU_2AT_S3" },  { "UATEL142_0", "UTaX_3AT_M2" },  { "UATEL142_0", "UTaX_3AT_M3" },
      { "UATEL142_0", "UTaX_3AT_M4" },  { "UATEL142_0", "UTaX_3AT_S1" },  { "UATEL142_0", "UTaX_3AT_S2" },
      { "UATEL142_0", "UTaX_3AT_S3" },  { "UATEL142_1", "UTaU_3AT_M2" },  { "UATEL142_1", "UTaU_3AT_M3" },
      { "UATEL142_1", "UTaU_3AT_M4" },  { "UATEL142_1", "UTaU_3AT_S1" },  { "UATEL142_1", "UTaU_3AT_S2" },
      { "UATEL142_1", "UTaU_3AT_S3" },  { "UATEL143_0", "UTaX_4AB_M1" },  { "UATEL143_0", "UTaX_5AB_M1" },
      { "UATEL143_0", "UTaX_6AB_M1" },  { "UATEL143_0", "UTaX_7AB_M1" },  { "UATEL143_0", "UTaX_8AB_M1" },
      { "UATEL143_1", "UTaU_4AB_M1" },  { "UATEL143_1", "UTaU_5AB_M1" },  { "UATEL143_1", "UTaU_6AB_M1" },
      { "UATEL143_1", "UTaU_7AB_M1" },  { "UATEL143_1", "UTaU_8AB_M1" },  { "UATEL151_0", "UTaX_2AB_M2" },
      { "UATEL151_0", "UTaX_2AB_M3" },  { "UATEL151_0", "UTaX_2AB_S1E" }, { "UATEL151_0", "UTaX_2AB_S1W" },
      { "UATEL151_0", "UTaX_2AB_S2" },  { "UATEL151_0", "UTaX_2AB_S3" },  { "UATEL151_1", "UTaU_2AB_M2" },
      { "UATEL151_1", "UTaU_2AB_M3" },  { "UATEL151_1", "UTaU_2AB_S1E" }, { "UATEL151_1", "UTaU_2AB_S1W" },
      { "UATEL151_1", "UTaU_2AB_S2" },  { "UATEL151_1", "UTaU_2AB_S3" },  { "UATEL152_0", "UTaX_3AB_M2" },
      { "UATEL152_0", "UTaX_3AB_M3" },  { "UATEL152_0", "UTaX_3AB_M4" },  { "UATEL152_0", "UTaX_3AB_S1" },
      { "UATEL152_0", "UTaX_3AB_S2" },  { "UATEL152_0", "UTaX_3AB_S3" },  { "UATEL152_1", "UTaU_3AB_M2" },
      { "UATEL152_1", "UTaU_3AB_M3" },  { "UATEL152_1", "UTaU_3AB_M4" },  { "UATEL152_1", "UTaU_3AB_S1" },
      { "UATEL152_1", "UTaU_3AB_S2" },  { "UATEL152_1", "UTaU_3AB_S3" },  { "UATEL153_0", "UTaX_1AT_M2" },
      { "UATEL153_0", "UTaX_1AT_M3" },  { "UATEL153_0", "UTaX_1AT_M4" },  { "UATEL153_0", "UTaX_1AT_S3" },
      { "UATEL153_0", "UTaX_1AT_S4" },  { "UATEL153_0", "UTaX_2AT_M4" },  { "UATEL153_1", "UTaU_1AT_M2" },
      { "UATEL153_1", "UTaU_1AT_M3" },  { "UATEL153_1", "UTaU_1AT_M4" },  { "UATEL153_1", "UTaU_1AT_S3" },
      { "UATEL153_1", "UTaU_1AT_S4" },  { "UATEL153_1", "UTaU_2AT_M4" },  { "UATEL161_0", "UTaX_1AB_M2" },
      { "UATEL161_0", "UTaX_1AB_M3" },  { "UATEL161_0", "UTaX_1AB_M4" },  { "UATEL161_0", "UTaX_1AB_S3" },
      { "UATEL161_0", "UTaX_1AB_S4" },  { "UATEL161_0", "UTaX_2AB_M4" },  { "UATEL161_1", "UTaU_1AB_M2" },
      { "UATEL161_1", "UTaU_1AB_M3" },  { "UATEL161_1", "UTaU_1AB_M4" },  { "UATEL161_1", "UTaU_1AB_S3" },
      { "UATEL161_1", "UTaU_1AB_S4" },  { "UATEL161_1", "UTaU_2AB_M4" },  { "UATEL162_0", "UTaX_1AT_M1E" },
      { "UATEL162_0", "UTaX_1AT_M1W" }, { "UATEL162_0", "UTaX_1AT_S2E" }, { "UATEL162_0", "UTaX_1AT_S2W" },
      { "UATEL162_0", "UTaX_3AT_M1" },  { "UATEL163_0", "UTaU_1AT_M1E" }, { "UATEL163_0", "UTaU_1AT_M1W" },
      { "UATEL163_0", "UTaU_1AT_S2E" }, { "UATEL163_0", "UTaU_1AT_S2W" }, { "UATEL163_0", "UTaU_3AT_M1" },
      { "UATEL171_0", "UTaU_1AB_M1E" }, { "UATEL171_0", "UTaU_1AB_M1W" }, { "UATEL171_0", "UTaU_1AB_S2E" },
      { "UATEL171_0", "UTaU_1AB_S2W" }, { "UATEL171_0", "UTaU_3AB_M1" },  { "UATEL172_0", "UTaX_1AT_S1E" },
      { "UATEL172_0", "UTaX_1AT_S1W" }, { "UATEL172_0", "UTaX_2AT_M1E" }, { "UATEL172_0", "UTaX_2AT_M1W" },
      { "UATEL173_0", "UTaU_1AT_S1E" }, { "UATEL173_0", "UTaU_1AT_S1W" }, { "UATEL173_0", "UTaU_2AT_M1E" },
      { "UATEL173_0", "UTaU_2AT_M1W" }, { "UATEL181_0", "UTaX_1AB_S1E" }, { "UATEL181_0", "UTaX_1AB_S1W" },
      { "UATEL181_0", "UTaX_2AB_M1E" }, { "UATEL181_0", "UTaX_2AB_M1W" }, { "UATEL182_0", "UTaU_1AB_S1E" },
      { "UATEL182_0", "UTaU_1AB_S1W" }, { "UATEL182_0", "UTaU_2AB_M1E" }, { "UATEL182_0", "UTaU_2AB_M1W" },
      { "UATEL183_0", "UTaX_1AB_M1E" }, { "UATEL183_0", "UTaX_1AB_M1W" }, { "UATEL183_0", "UTaX_1AB_S2E" },
      { "UATEL183_0", "UTaX_1AB_S2W" }, { "UATEL183_0", "UTaX_3AB_M1" },  { "UCTEL171_0", "UTaU_1CB_M1E" },
      { "UCTEL171_0", "UTaU_1CB_M1W" }, { "UCTEL161_1", "UTaU_1CB_M2" },  { "UCTEL161_1", "UTaU_1CB_M3" },
      { "UCTEL161_1", "UTaU_1CB_M4" },  { "UCTEL182_0", "UTaU_1CB_S1E" }, { "UCTEL182_0", "UTaU_1CB_S1W" },
      { "UCTEL171_0", "UTaU_1CB_S2E" }, { "UCTEL171_0", "UTaU_1CB_S2W" }, { "UCTEL161_1", "UTaU_1CB_S3" },
      { "UCTEL161_1", "UTaU_1CB_S4" },  { "UCTEL163_0", "UTaU_1CT_M1E" }, { "UCTEL163_0", "UTaU_1CT_M1W" },
      { "UCTEL153_1", "UTaU_1CT_M2" },  { "UCTEL153_1", "UTaU_1CT_M3" },  { "UCTEL153_1", "UTaU_1CT_M4" },
      { "UCTEL173_0", "UTaU_1CT_S1E" }, { "UCTEL173_0", "UTaU_1CT_S1W" }, { "UCTEL163_0", "UTaU_1CT_S2E" },
      { "UCTEL163_0", "UTaU_1CT_S2W" }, { "UCTEL153_1", "UTaU_1CT_S3" },  { "UCTEL153_1", "UTaU_1CT_S4" },
      { "UCTEL182_0", "UTaU_2CB_M1E" }, { "UCTEL182_0", "UTaU_2CB_M1W" }, { "UCTEL151_1", "UTaU_2CB_M2" },
      { "UCTEL151_1", "UTaU_2CB_M3" },  { "UCTEL161_1", "UTaU_2CB_M4" },  { "UCTEL151_1", "UTaU_2CB_S1E" },
      { "UCTEL151_1", "UTaU_2CB_S1W" }, { "UCTEL151_1", "UTaU_2CB_S2" },  { "UCTEL151_1", "UTaU_2CB_S3" },
      { "UCTEL173_0", "UTaU_2CT_M1E" }, { "UCTEL173_0", "UTaU_2CT_M1W" }, { "UCTEL141_1", "UTaU_2CT_M2" },
      { "UCTEL141_1", "UTaU_2CT_M3" },  { "UCTEL153_1", "UTaU_2CT_M4" },  { "UCTEL141_1", "UTaU_2CT_S1E" },
      { "UCTEL141_1", "UTaU_2CT_S1W" }, { "UCTEL141_1", "UTaU_2CT_S2" },  { "UCTEL141_1", "UTaU_2CT_S3" },
      { "UCTEL171_0", "UTaU_3CB_M1" },  { "UCTEL152_1", "UTaU_3CB_M2" },  { "UCTEL152_1", "UTaU_3CB_M3" },
      { "UCTEL152_1", "UTaU_3CB_M4" },  { "UCTEL152_1", "UTaU_3CB_S1" },  { "UCTEL152_1", "UTaU_3CB_S2" },
      { "UCTEL152_1", "UTaU_3CB_S3" },  { "UCTEL163_0", "UTaU_3CT_M1" },  { "UCTEL142_1", "UTaU_3CT_M2" },
      { "UCTEL142_1", "UTaU_3CT_M3" },  { "UCTEL142_1", "UTaU_3CT_M4" },  { "UCTEL142_1", "UTaU_3CT_S1" },
      { "UCTEL142_1", "UTaU_3CT_S2" },  { "UCTEL142_1", "UTaU_3CT_S3" },  { "UCTEL143_1", "UTaU_4CB_M1" },
      { "UCTEL131_1", "UTaU_4CB_M2" },  { "UCTEL131_1", "UTaU_4CB_M3" },  { "UCTEL131_1", "UTaU_4CB_M4" },
      { "UCTEL131_1", "UTaU_4CB_S1" },  { "UCTEL131_1", "UTaU_4CB_S2" },  { "UCTEL131_1", "UTaU_4CB_S3" },
      { "UCTEL123_1", "UTaU_4CT_M1" },  { "UCTEL111_1", "UTaU_4CT_M2" },  { "UCTEL111_1", "UTaU_4CT_M3" },
      { "UCTEL111_1", "UTaU_4CT_M4" },  { "UCTEL111_1", "UTaU_4CT_S1" },  { "UCTEL111_1", "UTaU_4CT_S2" },
      { "UCTEL111_1", "UTaU_4CT_S3" },  { "UCTEL143_1", "UTaU_5CB_M1" },  { "UCTEL132_1", "UTaU_5CB_M2" },
      { "UCTEL132_1", "UTaU_5CB_M3" },  { "UCTEL132_1", "UTaU_5CB_M4" },  { "UCTEL132_1", "UTaU_5CB_S1" },
      { "UCTEL132_1", "UTaU_5CB_S2" },  { "UCTEL132_1", "UTaU_5CB_S3" },  { "UCTEL123_1", "UTaU_5CT_M1" },
      { "UCTEL112_1", "UTaU_5CT_M2" },  { "UCTEL112_1", "UTaU_5CT_M3" },  { "UCTEL112_1", "UTaU_5CT_M4" },
      { "UCTEL112_1", "UTaU_5CT_S1" },  { "UCTEL112_1", "UTaU_5CT_S2" },  { "UCTEL112_1", "UTaU_5CT_S3" },
      { "UCTEL143_1", "UTaU_6CB_M1" },  { "UCTEL133_1", "UTaU_6CB_M2" },  { "UCTEL133_1", "UTaU_6CB_M3" },
      { "UCTEL133_1", "UTaU_6CB_M4" },  { "UCTEL133_1", "UTaU_6CB_S1" },  { "UCTEL133_1", "UTaU_6CB_S2" },
      { "UCTEL133_1", "UTaU_6CB_S3" },  { "UCTEL123_1", "UTaU_6CT_M1" },  { "UCTEL113_1", "UTaU_6CT_M2" },
      { "UCTEL113_1", "UTaU_6CT_M3" },  { "UCTEL113_1", "UTaU_6CT_M4" },  { "UCTEL113_1", "UTaU_6CT_S1" },
      { "UCTEL113_1", "UTaU_6CT_S2" },  { "UCTEL113_1", "UTaU_6CT_S3" },  { "UCTEL143_1", "UTaU_7CB_M1" },
      { "UCTEL121_1", "UTaU_7CB_M2" },  { "UCTEL121_1", "UTaU_7CB_M3" },  { "UCTEL121_1", "UTaU_7CB_M4" },
      { "UCTEL121_1", "UTaU_7CB_S1" },  { "UCTEL121_1", "UTaU_7CB_S2" },  { "UCTEL121_1", "UTaU_7CB_S3" },
      { "UCTEL123_1", "UTaU_7CT_M1" },  { "UCTEL101_1", "UTaU_7CT_M2" },  { "UCTEL101_1", "UTaU_7CT_M3" },
      { "UCTEL101_1", "UTaU_7CT_M4" },  { "UCTEL101_1", "UTaU_7CT_S1" },  { "UCTEL101_1", "UTaU_7CT_S2" },
      { "UCTEL101_1", "UTaU_7CT_S3" },  { "UCTEL143_1", "UTaU_8CB_M1" },  { "UCTEL122_1", "UTaU_8CB_M2" },
      { "UCTEL122_1", "UTaU_8CB_M3" },  { "UCTEL122_1", "UTaU_8CB_M4" },  { "UCTEL122_1", "UTaU_8CB_S1" },
      { "UCTEL122_1", "UTaU_8CB_S2" },  { "UCTEL122_1", "UTaU_8CB_S3" },  { "UCTEL123_1", "UTaU_8CT_M1" },
      { "UCTEL102_1", "UTaU_8CT_M2" },  { "UCTEL102_1", "UTaU_8CT_M3" },  { "UCTEL102_1", "UTaU_8CT_M4" },
      { "UCTEL102_1", "UTaU_8CT_S1" },  { "UCTEL102_1", "UTaU_8CT_S2" },  { "UCTEL102_1", "UTaU_8CT_S3" },
      { "UCTEL183_0", "UTaX_1CB_M1E" }, { "UCTEL183_0", "UTaX_1CB_M1W" }, { "UCTEL161_0", "UTaX_1CB_M2" },
      { "UCTEL161_0", "UTaX_1CB_M3" },  { "UCTEL161_0", "UTaX_1CB_M4" },  { "UCTEL181_0", "UTaX_1CB_S1E" },
      { "UCTEL181_0", "UTaX_1CB_S1W" }, { "UCTEL183_0", "UTaX_1CB_S2E" }, { "UCTEL183_0", "UTaX_1CB_S2W" },
      { "UCTEL161_0", "UTaX_1CB_S3" },  { "UCTEL161_0", "UTaX_1CB_S4" },  { "UCTEL162_0", "UTaX_1CT_M1E" },
      { "UCTEL162_0", "UTaX_1CT_M1W" }, { "UCTEL153_0", "UTaX_1CT_M2" },  { "UCTEL153_0", "UTaX_1CT_M3" },
      { "UCTEL153_0", "UTaX_1CT_M4" },  { "UCTEL172_0", "UTaX_1CT_S1E" }, { "UCTEL172_0", "UTaX_1CT_S1W" },
      { "UCTEL162_0", "UTaX_1CT_S2E" }, { "UCTEL162_0", "UTaX_1CT_S2W" }, { "UCTEL153_0", "UTaX_1CT_S3" },
      { "UCTEL153_0", "UTaX_1CT_S4" },  { "UCTEL181_0", "UTaX_2CB_M1E" }, { "UCTEL181_0", "UTaX_2CB_M1W" },
      { "UCTEL151_0", "UTaX_2CB_M2" },  { "UCTEL151_0", "UTaX_2CB_M3" },  { "UCTEL161_0", "UTaX_2CB_M4" },
      { "UCTEL151_0", "UTaX_2CB_S1E" }, { "UCTEL151_0", "UTaX_2CB_S1W" }, { "UCTEL151_0", "UTaX_2CB_S2" },
      { "UCTEL151_0", "UTaX_2CB_S3" },  { "UCTEL172_0", "UTaX_2CT_M1E" }, { "UCTEL172_0", "UTaX_2CT_M1W" },
      { "UCTEL141_0", "UTaX_2CT_M2" },  { "UCTEL141_0", "UTaX_2CT_M3" },  { "UCTEL153_0", "UTaX_2CT_M4" },
      { "UCTEL141_0", "UTaX_2CT_S1E" }, { "UCTEL141_0", "UTaX_2CT_S1W" }, { "UCTEL141_0", "UTaX_2CT_S2" },
      { "UCTEL141_0", "UTaX_2CT_S3" },  { "UCTEL183_0", "UTaX_3CB_M1" },  { "UCTEL152_0", "UTaX_3CB_M2" },
      { "UCTEL152_0", "UTaX_3CB_M3" },  { "UCTEL152_0", "UTaX_3CB_M4" },  { "UCTEL152_0", "UTaX_3CB_S1" },
      { "UCTEL152_0", "UTaX_3CB_S2" },  { "UCTEL152_0", "UTaX_3CB_S3" },  { "UCTEL162_0", "UTaX_3CT_M1" },
      { "UCTEL142_0", "UTaX_3CT_M2" },  { "UCTEL142_0", "UTaX_3CT_M3" },  { "UCTEL142_0", "UTaX_3CT_M4" },
      { "UCTEL142_0", "UTaX_3CT_S1" },  { "UCTEL142_0", "UTaX_3CT_S2" },  { "UCTEL142_0", "UTaX_3CT_S3" },
      { "UCTEL143_0", "UTaX_4CB_M1" },  { "UCTEL131_0", "UTaX_4CB_M2" },  { "UCTEL131_0", "UTaX_4CB_M3" },
      { "UCTEL131_0", "UTaX_4CB_M4" },  { "UCTEL131_0", "UTaX_4CB_S1" },  { "UCTEL131_0", "UTaX_4CB_S2" },
      { "UCTEL131_0", "UTaX_4CB_S3" },  { "UCTEL123_0", "UTaX_4CT_M1" },  { "UCTEL111_0", "UTaX_4CT_M2" },
      { "UCTEL111_0", "UTaX_4CT_M3" },  { "UCTEL111_0", "UTaX_4CT_M4" },  { "UCTEL111_0", "UTaX_4CT_S1" },
      { "UCTEL111_0", "UTaX_4CT_S2" },  { "UCTEL111_0", "UTaX_4CT_S3" },  { "UCTEL143_0", "UTaX_5CB_M1" },
      { "UCTEL132_0", "UTaX_5CB_M2" },  { "UCTEL132_0", "UTaX_5CB_M3" },  { "UCTEL132_0", "UTaX_5CB_M4" },
      { "UCTEL132_0", "UTaX_5CB_S1" },  { "UCTEL132_0", "UTaX_5CB_S2" },  { "UCTEL132_0", "UTaX_5CB_S3" },
      { "UCTEL123_0", "UTaX_5CT_M1" },  { "UCTEL112_0", "UTaX_5CT_M2" },  { "UCTEL112_0", "UTaX_5CT_M3" },
      { "UCTEL112_0", "UTaX_5CT_M4" },  { "UCTEL112_0", "UTaX_5CT_S1" },  { "UCTEL112_0", "UTaX_5CT_S2" },
      { "UCTEL112_0", "UTaX_5CT_S3" },  { "UCTEL143_0", "UTaX_6CB_M1" },  { "UCTEL133_0", "UTaX_6CB_M2" },
      { "UCTEL133_0", "UTaX_6CB_M3" },  { "UCTEL133_0", "UTaX_6CB_M4" },  { "UCTEL133_0", "UTaX_6CB_S1" },
      { "UCTEL133_0", "UTaX_6CB_S2" },  { "UCTEL133_0", "UTaX_6CB_S3" },  { "UCTEL123_0", "UTaX_6CT_M1" },
      { "UCTEL113_0", "UTaX_6CT_M2" },  { "UCTEL113_0", "UTaX_6CT_M3" },  { "UCTEL113_0", "UTaX_6CT_M4" },
      { "UCTEL113_0", "UTaX_6CT_S1" },  { "UCTEL113_0", "UTaX_6CT_S2" },  { "UCTEL113_0", "UTaX_6CT_S3" },
      { "UCTEL143_0", "UTaX_7CB_M1" },  { "UCTEL121_0", "UTaX_7CB_M2" },  { "UCTEL121_0", "UTaX_7CB_M3" },
      { "UCTEL121_0", "UTaX_7CB_M4" },  { "UCTEL121_0", "UTaX_7CB_S1" },  { "UCTEL121_0", "UTaX_7CB_S2" },
      { "UCTEL121_0", "UTaX_7CB_S3" },  { "UCTEL123_0", "UTaX_7CT_M1" },  { "UCTEL101_0", "UTaX_7CT_M2" },
      { "UCTEL101_0", "UTaX_7CT_M3" },  { "UCTEL101_0", "UTaX_7CT_M4" },  { "UCTEL101_0", "UTaX_7CT_S1" },
      { "UCTEL101_0", "UTaX_7CT_S2" },  { "UCTEL101_0", "UTaX_7CT_S3" },  { "UCTEL143_0", "UTaX_8CB_M1" },
      { "UCTEL122_0", "UTaX_8CB_M2" },  { "UCTEL122_0", "UTaX_8CB_M3" },  { "UCTEL122_0", "UTaX_8CB_M4" },
      { "UCTEL122_0", "UTaX_8CB_S1" },  { "UCTEL122_0", "UTaX_8CB_S2" },  { "UCTEL122_0", "UTaX_8CB_S3" },
      { "UCTEL123_0", "UTaX_8CT_M1" },  { "UCTEL102_0", "UTaX_8CT_M2" },  { "UCTEL102_0", "UTaX_8CT_M3" },
      { "UCTEL102_0", "UTaX_8CT_M4" },  { "UCTEL102_0", "UTaX_8CT_S1" },  { "UCTEL102_0", "UTaX_8CT_S2" },
      { "UCTEL102_0", "UTaX_8CT_S3" },  { "UCTEL061_0", "UTbV_1CB_M1E" }, { "UCTEL061_0", "UTbV_1CB_M1W" },
      { "UCTEL051_1", "UTbV_1CB_M2" },  { "UCTEL051_1", "UTbV_1CB_M3" },  { "UCTEL051_1", "UTbV_1CB_M4" },
      { "UCTEL072_0", "UTbV_1CB_S1E" }, { "UCTEL072_0", "UTbV_1CB_S1W" }, { "UCTEL061_0", "UTbV_1CB_S2E" },
      { "UCTEL061_0", "UTbV_1CB_S2W" }, { "UCTEL051_1", "UTbV_1CB_S3" },  { "UCTEL051_1", "UTbV_1CB_S4" },
      { "UCTEL053_0", "UTbV_1CT_M1E" }, { "UCTEL053_0", "UTbV_1CT_M1W" }, { "UCTEL043_1", "UTbV_1CT_M2" },
      { "UCTEL043_1", "UTbV_1CT_M3" },  { "UCTEL043_1", "UTbV_1CT_M4" },  { "UCTEL063_0", "UTbV_1CT_S1E" },
      { "UCTEL063_0", "UTbV_1CT_S1W" }, { "UCTEL053_0", "UTbV_1CT_S2E" }, { "UCTEL053_0", "UTbV_1CT_S2W" },
      { "UCTEL043_1", "UTbV_1CT_S3" },  { "UCTEL043_1", "UTbV_1CT_S4" },  { "UCTEL072_0", "UTbV_2CB_M1E" },
      { "UCTEL072_0", "UTbV_2CB_M1W" }, { "UCTEL041_1", "UTbV_2CB_M2" },  { "UCTEL041_1", "UTbV_2CB_M3" },
      { "UCTEL051_1", "UTbV_2CB_M4" },  { "UCTEL041_1", "UTbV_2CB_S1E" }, { "UCTEL041_1", "UTbV_2CB_S1W" },
      { "UCTEL041_1", "UTbV_2CB_S2" },  { "UCTEL041_1", "UTbV_2CB_S3" },  { "UCTEL063_0", "UTbV_2CT_M1E" },
      { "UCTEL063_0", "UTbV_2CT_M1W" }, { "UCTEL031_1", "UTbV_2CT_M2" },  { "UCTEL031_1", "UTbV_2CT_M3" },
      { "UCTEL043_1", "UTbV_2CT_M4" },  { "UCTEL031_1", "UTbV_2CT_S1E" }, { "UCTEL031_1", "UTbV_2CT_S1W" },
      { "UCTEL031_1", "UTbV_2CT_S2" },  { "UCTEL031_1", "UTbV_2CT_S3" },  { "UCTEL061_0", "UTbV_3CB_M1" },
      { "UCTEL042_1", "UTbV_3CB_M2" },  { "UCTEL042_1", "UTbV_3CB_M3" },  { "UCTEL042_1", "UTbV_3CB_M4" },
      { "UCTEL042_1", "UTbV_3CB_S1" },  { "UCTEL042_1", "UTbV_3CB_S2" },  { "UCTEL042_1", "UTbV_3CB_S3" },
      { "UCTEL053_0", "UTbV_3CT_M1" },  { "UCTEL032_1", "UTbV_3CT_M2" },  { "UCTEL032_1", "UTbV_3CT_M3" },
      { "UCTEL032_1", "UTbV_3CT_M4" },  { "UCTEL032_1", "UTbV_3CT_S1" },  { "UCTEL032_1", "UTbV_3CT_S2" },
      { "UCTEL032_1", "UTbV_3CT_S3" },  { "UCTEL103_1", "UTbV_4CB_M1" },  { "UCTEL091_1", "UTbV_4CB_M2" },
      { "UCTEL091_1", "UTbV_4CB_M3" },  { "UCTEL091_1", "UTbV_4CB_M4" },  { "UCTEL091_1", "UTbV_4CB_S1" },
      { "UCTEL091_1", "UTbV_4CB_S2" },  { "UCTEL091_1", "UTbV_4CB_S3" },  { "UCTEL033_1", "UTbV_4CT_M1" },
      { "UCTEL021_1", "UTbV_4CT_M2" },  { "UCTEL021_1", "UTbV_4CT_M3" },  { "UCTEL021_1", "UTbV_4CT_M4" },
      { "UCTEL021_1", "UTbV_4CT_S1" },  { "UCTEL021_1", "UTbV_4CT_S2" },  { "UCTEL021_1", "UTbV_4CT_S3" },
      { "UCTEL103_1", "UTbV_5CB_M1" },  { "UCTEL092_1", "UTbV_5CB_M2" },  { "UCTEL092_1", "UTbV_5CB_M3" },
      { "UCTEL092_1", "UTbV_5CB_M4" },  { "UCTEL092_1", "UTbV_5CB_S1" },  { "UCTEL092_1", "UTbV_5CB_S2" },
      { "UCTEL092_1", "UTbV_5CB_S3" },  { "UCTEL033_1", "UTbV_5CT_M1" },  { "UCTEL022_1", "UTbV_5CT_M2" },
      { "UCTEL022_1", "UTbV_5CT_M3" },  { "UCTEL022_1", "UTbV_5CT_M4" },  { "UCTEL022_1", "UTbV_5CT_S1" },
      { "UCTEL022_1", "UTbV_5CT_S2" },  { "UCTEL022_1", "UTbV_5CT_S3" },  { "UCTEL103_1", "UTbV_6CB_M1" },
      { "UCTEL093_1", "UTbV_6CB_M2" },  { "UCTEL093_1", "UTbV_6CB_M3" },  { "UCTEL093_1", "UTbV_6CB_M4" },
      { "UCTEL093_1", "UTbV_6CB_S1" },  { "UCTEL093_1", "UTbV_6CB_S2" },  { "UCTEL093_1", "UTbV_6CB_S3" },
      { "UCTEL033_1", "UTbV_6CT_M1" },  { "UCTEL023_1", "UTbV_6CT_M2" },  { "UCTEL023_1", "UTbV_6CT_M3" },
      { "UCTEL023_1", "UTbV_6CT_M4" },  { "UCTEL023_1", "UTbV_6CT_S1" },  { "UCTEL023_1", "UTbV_6CT_S2" },
      { "UCTEL023_1", "UTbV_6CT_S3" },  { "UCTEL103_1", "UTbV_7CB_M1" },  { "UCTEL081_1", "UTbV_7CB_M2" },
      { "UCTEL081_1", "UTbV_7CB_M3" },  { "UCTEL081_1", "UTbV_7CB_M4" },  { "UCTEL081_1", "UTbV_7CB_S1" },
      { "UCTEL081_1", "UTbV_7CB_S2" },  { "UCTEL081_1", "UTbV_7CB_S3" },  { "UCTEL033_1", "UTbV_7CT_M1" },
      { "UCTEL011_1", "UTbV_7CT_M2" },  { "UCTEL011_1", "UTbV_7CT_M3" },  { "UCTEL011_1", "UTbV_7CT_M4" },
      { "UCTEL011_1", "UTbV_7CT_S1" },  { "UCTEL011_1", "UTbV_7CT_S2" },  { "UCTEL011_1", "UTbV_7CT_S3" },
      { "UCTEL103_1", "UTbV_8CB_M1" },  { "UCTEL082_1", "UTbV_8CB_M2" },  { "UCTEL082_1", "UTbV_8CB_M3" },
      { "UCTEL082_1", "UTbV_8CB_M4" },  { "UCTEL082_1", "UTbV_8CB_S1" },  { "UCTEL082_1", "UTbV_8CB_S2" },
      { "UCTEL082_1", "UTbV_8CB_S3" },  { "UCTEL033_1", "UTbV_8CT_M1" },  { "UCTEL012_1", "UTbV_8CT_M2" },
      { "UCTEL012_1", "UTbV_8CT_M3" },  { "UCTEL012_1", "UTbV_8CT_M4" },  { "UCTEL012_1", "UTbV_8CT_S1" },
      { "UCTEL012_1", "UTbV_8CT_S2" },  { "UCTEL012_1", "UTbV_8CT_S3" },  { "UCTEL103_1", "UTbV_9CB_M1" },
      { "UCTEL083_1", "UTbV_9CB_M2" },  { "UCTEL083_1", "UTbV_9CB_M3" },  { "UCTEL083_1", "UTbV_9CB_M4" },
      { "UCTEL083_1", "UTbV_9CB_S1" },  { "UCTEL083_1", "UTbV_9CB_S2" },  { "UCTEL083_1", "UTbV_9CB_S3" },
      { "UCTEL033_1", "UTbV_9CT_M1" },  { "UCTEL013_1", "UTbV_9CT_M2" },  { "UCTEL013_1", "UTbV_9CT_M3" },
      { "UCTEL013_1", "UTbV_9CT_M4" },  { "UCTEL013_1", "UTbV_9CT_S1" },  { "UCTEL013_1", "UTbV_9CT_S2" },
      { "UCTEL013_1", "UTbV_9CT_S3" },  { "UCTEL073_0", "UTbX_1CB_M1E" }, { "UCTEL073_0", "UTbX_1CB_M1W" },
      { "UCTEL051_0", "UTbX_1CB_M2" },  { "UCTEL051_0", "UTbX_1CB_M3" },  { "UCTEL051_0", "UTbX_1CB_M4" },
      { "UCTEL071_0", "UTbX_1CB_S1E" }, { "UCTEL071_0", "UTbX_1CB_S1W" }, { "UCTEL073_0", "UTbX_1CB_S2E" },
      { "UCTEL073_0", "UTbX_1CB_S2W" }, { "UCTEL051_0", "UTbX_1CB_S3" },  { "UCTEL051_0", "UTbX_1CB_S4" },
      { "UCTEL052_0", "UTbX_1CT_M1E" }, { "UCTEL052_0", "UTbX_1CT_M1W" }, { "UCTEL043_0", "UTbX_1CT_M2" },
      { "UCTEL043_0", "UTbX_1CT_M3" },  { "UCTEL043_0", "UTbX_1CT_M4" },  { "UCTEL062_0", "UTbX_1CT_S1E" },
      { "UCTEL062_0", "UTbX_1CT_S1W" }, { "UCTEL052_0", "UTbX_1CT_S2E" }, { "UCTEL052_0", "UTbX_1CT_S2W" },
      { "UCTEL043_0", "UTbX_1CT_S3" },  { "UCTEL043_0", "UTbX_1CT_S4" },  { "UCTEL071_0", "UTbX_2CB_M1E" },
      { "UCTEL071_0", "UTbX_2CB_M1W" }, { "UCTEL041_0", "UTbX_2CB_M2" },  { "UCTEL041_0", "UTbX_2CB_M3" },
      { "UCTEL051_0", "UTbX_2CB_M4" },  { "UCTEL041_0", "UTbX_2CB_S1E" }, { "UCTEL041_0", "UTbX_2CB_S1W" },
      { "UCTEL041_0", "UTbX_2CB_S2" },  { "UCTEL041_0", "UTbX_2CB_S3" },  { "UCTEL062_0", "UTbX_2CT_M1E" },
      { "UCTEL062_0", "UTbX_2CT_M1W" }, { "UCTEL031_0", "UTbX_2CT_M2" },  { "UCTEL031_0", "UTbX_2CT_M3" },
      { "UCTEL043_0", "UTbX_2CT_M4" },  { "UCTEL031_0", "UTbX_2CT_S1E" }, { "UCTEL031_0", "UTbX_2CT_S1W" },
      { "UCTEL031_0", "UTbX_2CT_S2" },  { "UCTEL031_0", "UTbX_2CT_S3" },  { "UCTEL073_0", "UTbX_3CB_M1" },
      { "UCTEL042_0", "UTbX_3CB_M2" },  { "UCTEL042_0", "UTbX_3CB_M3" },  { "UCTEL042_0", "UTbX_3CB_M4" },
      { "UCTEL042_0", "UTbX_3CB_S1" },  { "UCTEL042_0", "UTbX_3CB_S2" },  { "UCTEL042_0", "UTbX_3CB_S3" },
      { "UCTEL052_0", "UTbX_3CT_M1" },  { "UCTEL032_0", "UTbX_3CT_M2" },  { "UCTEL032_0", "UTbX_3CT_M3" },
      { "UCTEL032_0", "UTbX_3CT_M4" },  { "UCTEL032_0", "UTbX_3CT_S1" },  { "UCTEL032_0", "UTbX_3CT_S2" },
      { "UCTEL032_0", "UTbX_3CT_S3" },  { "UCTEL103_0", "UTbX_4CB_M1" },  { "UCTEL091_0", "UTbX_4CB_M2" },
      { "UCTEL091_0", "UTbX_4CB_M3" },  { "UCTEL091_0", "UTbX_4CB_M4" },  { "UCTEL091_0", "UTbX_4CB_S1" },
      { "UCTEL091_0", "UTbX_4CB_S2" },  { "UCTEL091_0", "UTbX_4CB_S3" },  { "UCTEL033_0", "UTbX_4CT_M1" },
      { "UCTEL021_0", "UTbX_4CT_M2" },  { "UCTEL021_0", "UTbX_4CT_M3" },  { "UCTEL021_0", "UTbX_4CT_M4" },
      { "UCTEL021_0", "UTbX_4CT_S1" },  { "UCTEL021_0", "UTbX_4CT_S2" },  { "UCTEL021_0", "UTbX_4CT_S3" },
      { "UCTEL103_0", "UTbX_5CB_M1" },  { "UCTEL092_0", "UTbX_5CB_M2" },  { "UCTEL092_0", "UTbX_5CB_M3" },
      { "UCTEL092_0", "UTbX_5CB_M4" },  { "UCTEL092_0", "UTbX_5CB_S1" },  { "UCTEL092_0", "UTbX_5CB_S2" },
      { "UCTEL092_0", "UTbX_5CB_S3" },  { "UCTEL033_0", "UTbX_5CT_M1" },  { "UCTEL022_0", "UTbX_5CT_M2" },
      { "UCTEL022_0", "UTbX_5CT_M3" },  { "UCTEL022_0", "UTbX_5CT_M4" },  { "UCTEL022_0", "UTbX_5CT_S1" },
      { "UCTEL022_0", "UTbX_5CT_S2" },  { "UCTEL022_0", "UTbX_5CT_S3" },  { "UCTEL103_0", "UTbX_6CB_M1" },
      { "UCTEL093_0", "UTbX_6CB_M2" },  { "UCTEL093_0", "UTbX_6CB_M3" },  { "UCTEL093_0", "UTbX_6CB_M4" },
      { "UCTEL093_0", "UTbX_6CB_S1" },  { "UCTEL093_0", "UTbX_6CB_S2" },  { "UCTEL093_0", "UTbX_6CB_S3" },
      { "UCTEL033_0", "UTbX_6CT_M1" },  { "UCTEL023_0", "UTbX_6CT_M2" },  { "UCTEL023_0", "UTbX_6CT_M3" },
      { "UCTEL023_0", "UTbX_6CT_M4" },  { "UCTEL023_0", "UTbX_6CT_S1" },  { "UCTEL023_0", "UTbX_6CT_S2" },
      { "UCTEL023_0", "UTbX_6CT_S3" },  { "UCTEL103_0", "UTbX_7CB_M1" },  { "UCTEL081_0", "UTbX_7CB_M2" },
      { "UCTEL081_0", "UTbX_7CB_M3" },  { "UCTEL081_0", "UTbX_7CB_M4" },  { "UCTEL081_0", "UTbX_7CB_S1" },
      { "UCTEL081_0", "UTbX_7CB_S2" },  { "UCTEL081_0", "UTbX_7CB_S3" },  { "UCTEL033_0", "UTbX_7CT_M1" },
      { "UCTEL011_0", "UTbX_7CT_M2" },  { "UCTEL011_0", "UTbX_7CT_M3" },  { "UCTEL011_0", "UTbX_7CT_M4" },
      { "UCTEL011_0", "UTbX_7CT_S1" },  { "UCTEL011_0", "UTbX_7CT_S2" },  { "UCTEL011_0", "UTbX_7CT_S3" },
      { "UCTEL103_0", "UTbX_8CB_M1" },  { "UCTEL082_0", "UTbX_8CB_M2" },  { "UCTEL082_0", "UTbX_8CB_M3" },
      { "UCTEL082_0", "UTbX_8CB_M4" },  { "UCTEL082_0", "UTbX_8CB_S1" },  { "UCTEL082_0", "UTbX_8CB_S2" },
      { "UCTEL082_0", "UTbX_8CB_S3" },  { "UCTEL033_0", "UTbX_8CT_M1" },  { "UCTEL012_0", "UTbX_8CT_M2" },
      { "UCTEL012_0", "UTbX_8CT_M3" },  { "UCTEL012_0", "UTbX_8CT_M4" },  { "UCTEL012_0", "UTbX_8CT_S1" },
      { "UCTEL012_0", "UTbX_8CT_S2" },  { "UCTEL012_0", "UTbX_8CT_S3" },  { "UCTEL103_0", "UTbX_9CB_M1" },
      { "UCTEL083_0", "UTbX_9CB_M2" },  { "UCTEL083_0", "UTbX_9CB_M3" },  { "UCTEL083_0", "UTbX_9CB_M4" },
      { "UCTEL083_0", "UTbX_9CB_S1" },  { "UCTEL083_0", "UTbX_9CB_S2" },  { "UCTEL083_0", "UTbX_9CB_S3" },
      { "UCTEL033_0", "UTbX_9CT_M1" },  { "UCTEL013_0", "UTbX_9CT_M2" },  { "UCTEL013_0", "UTbX_9CT_M3" },
      { "UCTEL013_0", "UTbX_9CT_M4" },  { "UCTEL013_0", "UTbX_9CT_S1" },  { "UCTEL013_0", "UTbX_9CT_S2" } };

  UT_TELL40Map = { { 10346, "UATEL011_0" }, { 11370, "UATEL011_1" }, { 10347, "UATEL012_0" }, { 11371, "UATEL012_1" },
                   { 10348, "UATEL013_0" }, { 11372, "UATEL013_1" }, { 10343, "UATEL021_0" }, { 11367, "UATEL021_1" },
                   { 10344, "UATEL022_0" }, { 11368, "UATEL022_1" }, { 10345, "UATEL023_0" }, { 11369, "UATEL023_1" },
                   { 10333, "UATEL031_0" }, { 11357, "UATEL031_1" }, { 10334, "UATEL032_0" }, { 11358, "UATEL032_1" },
                   { 10342, "UATEL033_0" }, { 11366, "UATEL033_1" }, { 10330, "UATEL041_0" }, { 11354, "UATEL041_1" },
                   { 10331, "UATEL042_0" }, { 11355, "UATEL042_1" }, { 10332, "UATEL043_0" }, { 11356, "UATEL043_1" },
                   { 10341, "UATEL051_0" }, { 11365, "UATEL051_1" }, { 10328, "UATEL052_0" }, { 11352, "UATEL052_1" },
                   { 10329, "UATEL053_0" }, { 11353, "UATEL053_1" }, { 10327, "UATEL061_0" }, { 11351, "UATEL061_1" },
                   { 10339, "UATEL062_0" }, { 11363, "UATEL062_1" }, { 10340, "UATEL063_0" }, { 11364, "UATEL063_1" },
                   { 10325, "UATEL071_0" }, { 11349, "UATEL071_1" }, { 10326, "UATEL072_0" }, { 11350, "UATEL072_1" },
                   { 10338, "UATEL073_0" }, { 11362, "UATEL073_1" }, { 10335, "UATEL081_0" }, { 11359, "UATEL081_1" },
                   { 10336, "UATEL082_0" }, { 11360, "UATEL082_1" }, { 10337, "UATEL083_0" }, { 11361, "UATEL083_1" },
                   { 10322, "UATEL091_0" }, { 11346, "UATEL091_1" }, { 10323, "UATEL092_0" }, { 11347, "UATEL092_1" },
                   { 10324, "UATEL093_0" }, { 11348, "UATEL093_1" }, { 10291, "UATEL101_0" }, { 11315, "UATEL101_1" },
                   { 10292, "UATEL102_0" }, { 11316, "UATEL102_1" }, { 10321, "UATEL103_0" }, { 11345, "UATEL103_1" },
                   { 10288, "UATEL111_0" }, { 11312, "UATEL111_1" }, { 10289, "UATEL112_0" }, { 11313, "UATEL112_1" },
                   { 10290, "UATEL113_0" }, { 11314, "UATEL113_1" }, { 10278, "UATEL121_0" }, { 11302, "UATEL121_1" },
                   { 10279, "UATEL122_0" }, { 11303, "UATEL122_1" }, { 10287, "UATEL123_0" }, { 11311, "UATEL123_1" },
                   { 10275, "UATEL131_0" }, { 11299, "UATEL131_1" }, { 10276, "UATEL132_0" }, { 11300, "UATEL132_1" },
                   { 10277, "UATEL133_0" }, { 11301, "UATEL133_1" }, { 10285, "UATEL141_0" }, { 11309, "UATEL141_1" },
                   { 10286, "UATEL142_0" }, { 11310, "UATEL142_1" }, { 10274, "UATEL143_0" }, { 11298, "UATEL143_1" },
                   { 10272, "UATEL151_0" }, { 11296, "UATEL151_1" }, { 10273, "UATEL152_0" }, { 11297, "UATEL152_1" },
                   { 10284, "UATEL153_0" }, { 11308, "UATEL153_1" }, { 10271, "UATEL161_0" }, { 11295, "UATEL161_1" },
                   { 10282, "UATEL162_0" }, { 11306, "UATEL162_1" }, { 10283, "UATEL163_0" }, { 11307, "UATEL163_1" },
                   { 10270, "UATEL171_0" }, { 11294, "UATEL171_1" }, { 10280, "UATEL172_0" }, { 11304, "UATEL172_1" },
                   { 10281, "UATEL173_0" }, { 11305, "UATEL173_1" }, { 10267, "UATEL181_0" }, { 11291, "UATEL181_1" },
                   { 10268, "UATEL182_0" }, { 11292, "UATEL182_1" }, { 10269, "UATEL183_0" }, { 11293, "UATEL183_1" },
                   { 12366, "UCTEL011_0" }, { 13390, "UCTEL011_1" }, { 12367, "UCTEL012_0" }, { 13391, "UCTEL012_1" },
                   { 12368, "UCTEL013_0" }, { 13392, "UCTEL013_1" }, { 12363, "UCTEL021_0" }, { 13387, "UCTEL021_1" },
                   { 12364, "UCTEL022_0" }, { 13388, "UCTEL022_1" }, { 12365, "UCTEL023_0" }, { 13389, "UCTEL023_1" },
                   { 12360, "UCTEL031_0" }, { 13384, "UCTEL031_1" }, { 12361, "UCTEL032_0" }, { 13385, "UCTEL032_1" },
                   { 12362, "UCTEL033_0" }, { 13386, "UCTEL033_1" }, { 12346, "UCTEL041_0" }, { 13370, "UCTEL041_1" },
                   { 12347, "UCTEL042_0" }, { 13371, "UCTEL042_1" }, { 12359, "UCTEL043_0" }, { 13383, "UCTEL043_1" },
                   { 12345, "UCTEL051_0" }, { 13369, "UCTEL051_1" }, { 12357, "UCTEL052_0" }, { 13381, "UCTEL052_1" },
                   { 12358, "UCTEL053_0" }, { 13382, "UCTEL053_1" }, { 12344, "UCTEL061_0" }, { 13368, "UCTEL061_1" },
                   { 12355, "UCTEL062_0" }, { 13379, "UCTEL062_1" }, { 12356, "UCTEL063_0" }, { 13380, "UCTEL063_1" },
                   { 12341, "UCTEL071_0" }, { 13365, "UCTEL071_1" }, { 12342, "UCTEL072_0" }, { 13366, "UCTEL072_1" },
                   { 12343, "UCTEL073_0" }, { 13367, "UCTEL073_1" }, { 12352, "UCTEL081_0" }, { 13376, "UCTEL081_1" },
                   { 12353, "UCTEL082_0" }, { 13377, "UCTEL082_1" }, { 12354, "UCTEL083_0" }, { 13378, "UCTEL083_1" },
                   { 12349, "UCTEL091_0" }, { 13373, "UCTEL091_1" }, { 12350, "UCTEL092_0" }, { 13374, "UCTEL092_1" },
                   { 12351, "UCTEL093_0" }, { 13375, "UCTEL093_1" }, { 12313, "UCTEL101_0" }, { 13337, "UCTEL101_1" },
                   { 12314, "UCTEL102_0" }, { 13338, "UCTEL102_1" }, { 12348, "UCTEL103_0" }, { 13372, "UCTEL103_1" },
                   { 12310, "UCTEL111_0" }, { 13334, "UCTEL111_1" }, { 12311, "UCTEL112_0" }, { 13335, "UCTEL112_1" },
                   { 12312, "UCTEL113_0" }, { 13336, "UCTEL113_1" }, { 12300, "UCTEL121_0" }, { 13324, "UCTEL121_1" },
                   { 12301, "UCTEL122_0" }, { 13325, "UCTEL122_1" }, { 12309, "UCTEL123_0" }, { 13333, "UCTEL123_1" },
                   { 12297, "UCTEL131_0" }, { 13321, "UCTEL131_1" }, { 12298, "UCTEL132_0" }, { 13322, "UCTEL132_1" },
                   { 12299, "UCTEL133_0" }, { 13323, "UCTEL133_1" }, { 12307, "UCTEL141_0" }, { 13331, "UCTEL141_1" },
                   { 12308, "UCTEL142_0" }, { 13332, "UCTEL142_1" }, { 12296, "UCTEL143_0" }, { 13320, "UCTEL143_1" },
                   { 12294, "UCTEL151_0" }, { 13318, "UCTEL151_1" }, { 12295, "UCTEL152_0" }, { 13319, "UCTEL152_1" },
                   { 12306, "UCTEL153_0" }, { 13330, "UCTEL153_1" }, { 12293, "UCTEL161_0" }, { 13317, "UCTEL161_1" },
                   { 12304, "UCTEL162_0" }, { 13328, "UCTEL162_1" }, { 12305, "UCTEL163_0" }, { 13329, "UCTEL163_1" },
                   { 12292, "UCTEL171_0" }, { 13316, "UCTEL171_1" }, { 12302, "UCTEL172_0" }, { 13326, "UCTEL172_1" },
                   { 12303, "UCTEL173_0" }, { 13327, "UCTEL173_1" }, { 12289, "UCTEL181_0" }, { 13313, "UCTEL181_1" },
                   { 12290, "UCTEL182_0" }, { 13314, "UCTEL182_1" }, { 12291, "UCTEL183_0" }, { 13315, "UCTEL183_1" } };

  UT_BoardIDtoSourceID = {
      { 210, 10346 }, { 211, 11370 }, { 212, 10347 }, { 213, 11371 }, { 214, 10348 }, { 215, 11372 }, { 204, 10343 },
      { 205, 11367 }, { 206, 10344 }, { 207, 11368 }, { 208, 10345 }, { 209, 11369 }, { 184, 10333 }, { 185, 11357 },
      { 186, 10334 }, { 187, 11358 }, { 202, 10342 }, { 203, 11366 }, { 178, 10330 }, { 179, 11354 }, { 180, 10331 },
      { 181, 11355 }, { 182, 10332 }, { 183, 11356 }, { 200, 10341 }, { 201, 11365 }, { 174, 10328 }, { 175, 11352 },
      { 176, 10329 }, { 177, 11353 }, { 172, 10327 }, { 173, 11351 }, { 196, 10339 }, { 197, 11363 }, { 198, 10340 },
      { 199, 11364 }, { 168, 10325 }, { 169, 11349 }, { 170, 10326 }, { 171, 11350 }, { 194, 10338 }, { 195, 11362 },
      { 188, 10335 }, { 189, 11359 }, { 190, 10336 }, { 191, 11360 }, { 192, 10337 }, { 193, 11361 }, { 162, 10322 },
      { 163, 11346 }, { 164, 10323 }, { 165, 11347 }, { 166, 10324 }, { 167, 11348 }, { 100, 10291 }, { 101, 11315 },
      { 102, 10292 }, { 103, 11316 }, { 160, 10321 }, { 161, 11345 }, { 94, 10288 },  { 95, 11312 },  { 96, 10289 },
      { 97, 11313 },  { 98, 10290 },  { 99, 11314 },  { 74, 10278 },  { 75, 11302 },  { 76, 10279 },  { 77, 11303 },
      { 92, 10287 },  { 93, 11311 },  { 68, 10275 },  { 69, 11299 },  { 70, 10276 },  { 71, 11300 },  { 72, 10277 },
      { 73, 11301 },  { 88, 10285 },  { 89, 11309 },  { 90, 10286 },  { 91, 11310 },  { 66, 10274 },  { 67, 11298 },
      { 62, 10272 },  { 63, 11296 },  { 64, 10273 },  { 65, 11297 },  { 86, 10284 },  { 87, 11308 },  { 60, 10271 },
      { 61, 11295 },  { 82, 10282 },  { 83, 11306 },  { 84, 10283 },  { 85, 11307 },  { 58, 10270 },  { 59, 11294 },
      { 78, 10280 },  { 79, 11304 },  { 80, 10281 },  { 81, 11305 },  { 52, 10267 },  { 53, 11291 },  { 54, 10268 },
      { 55, 11292 },  { 56, 10269 },  { 57, 11293 },  { 154, 12366 }, { 155, 13390 }, { 156, 12367 }, { 157, 13391 },
      { 158, 12368 }, { 159, 13392 }, { 148, 12363 }, { 149, 13387 }, { 150, 12364 }, { 151, 13388 }, { 152, 12365 },
      { 153, 13389 }, { 142, 12360 }, { 143, 13384 }, { 144, 12361 }, { 145, 13385 }, { 146, 12362 }, { 147, 13386 },
      { 114, 12346 }, { 115, 13370 }, { 116, 12347 }, { 117, 13371 }, { 140, 12359 }, { 141, 13383 }, { 112, 12345 },
      { 113, 13369 }, { 136, 12357 }, { 137, 13381 }, { 138, 12358 }, { 139, 13382 }, { 110, 12344 }, { 111, 13368 },
      { 132, 12355 }, { 133, 13379 }, { 134, 12356 }, { 135, 13380 }, { 104, 12341 }, { 105, 13365 }, { 106, 12342 },
      { 107, 13366 }, { 108, 12343 }, { 109, 13367 }, { 126, 12352 }, { 127, 13376 }, { 128, 12353 }, { 129, 13377 },
      { 130, 12354 }, { 131, 13378 }, { 120, 12349 }, { 121, 13373 }, { 122, 12350 }, { 123, 13374 }, { 124, 12351 },
      { 125, 13375 }, { 48, 12313 },  { 49, 13337 },  { 50, 12314 },  { 51, 13338 },  { 118, 12348 }, { 119, 13372 },
      { 42, 12310 },  { 43, 13334 },  { 44, 12311 },  { 45, 13335 },  { 46, 12312 },  { 47, 13336 },  { 22, 12300 },
      { 23, 13324 },  { 24, 12301 },  { 25, 13325 },  { 40, 12309 },  { 41, 13333 },  { 16, 12297 },  { 17, 13321 },
      { 18, 12298 },  { 19, 13322 },  { 20, 12299 },  { 21, 13323 },  { 36, 12307 },  { 37, 13331 },  { 38, 12308 },
      { 39, 13332 },  { 14, 12296 },  { 15, 13320 },  { 10, 12294 },  { 11, 13318 },  { 12, 12295 },  { 13, 13319 },
      { 34, 12306 },  { 35, 13330 },  { 8, 12293 },   { 9, 13317 },   { 30, 12304 },  { 31, 13328 },  { 32, 12305 },
      { 33, 13329 },  { 6, 12292 },   { 7, 13316 },   { 26, 12302 },  { 27, 13326 },  { 28, 12303 },  { 29, 13327 },
      { 0, 12289 },   { 1, 13313 },   { 2, 12290 },   { 3, 13314 },   { 4, 12291 },   { 5, 13315 } };

  UT_TELL40BinsASide_Flow0 = {
      { 10346, 0 },  { 10347, 1 },  { 10348, 2 },  { 10343, 3 },  { 10344, 4 },  { 10345, 5 },  { 10333, 6 },
      { 10334, 7 },  { 10342, 8 },  { 10330, 9 },  { 10331, 10 }, { 10332, 11 }, { 10341, 12 }, { 10328, 13 },
      { 10329, 14 }, { 10327, 15 }, { 10339, 16 }, { 10340, 17 }, { 10325, 18 }, { 10326, 19 }, { 10338, 20 },
      { 10335, 21 }, { 10336, 22 }, { 10337, 23 }, { 10322, 24 }, { 10323, 25 }, { 10324, 26 }, { 10291, 27 },
      { 10292, 28 }, { 10321, 29 }, { 10288, 30 }, { 10289, 31 }, { 10290, 32 }, { 10278, 33 }, { 10279, 34 },
      { 10287, 35 }, { 10275, 36 }, { 10276, 37 }, { 10277, 38 }, { 10285, 39 }, { 10286, 40 }, { 10274, 41 },
      { 10272, 42 }, { 10273, 43 }, { 10284, 44 }, { 10271, 45 }, { 10282, 46 }, { 10283, 47 }, { 10270, 48 },
      { 10280, 49 }, { 10281, 50 }, { 10267, 51 }, { 10268, 52 }, { 10269, 53 } };

  UT_TELL40BinsASide_Flow1 = {
      { 11370, 0 },  { 11371, 1 },  { 11372, 2 },  { 11367, 3 },  { 11368, 4 },  { 11369, 5 },  { 11357, 6 },
      { 11358, 7 },  { 11366, 8 },  { 11354, 9 },  { 11355, 10 }, { 11356, 11 }, { 11365, 12 }, { 11352, 13 },
      { 11353, 14 }, { 11351, 15 }, { 11363, 16 }, { 11364, 17 }, { 11349, 18 }, { 11350, 19 }, { 11362, 20 },
      { 11359, 21 }, { 11360, 22 }, { 11361, 23 }, { 11346, 24 }, { 11347, 25 }, { 11348, 26 }, { 11315, 27 },
      { 11316, 28 }, { 11345, 29 }, { 11312, 30 }, { 11313, 31 }, { 11314, 32 }, { 11302, 33 }, { 11303, 34 },
      { 11311, 35 }, { 11299, 36 }, { 11300, 37 }, { 11301, 38 }, { 11309, 39 }, { 11310, 40 }, { 11298, 41 },
      { 11296, 42 }, { 11297, 43 }, { 11308, 44 }, { 11295, 45 }, { 11306, 46 }, { 11307, 47 }, { 11294, 48 },
      { 11304, 49 }, { 11305, 50 }, { 11291, 51 }, { 11292, 52 }, { 11293, 53 } };

  UT_TELL40BinsCSide_Flow0 = {
      { 12366, 0 },  { 12367, 1 },  { 12368, 2 },  { 12363, 3 },  { 12364, 4 },  { 12365, 5 },  { 12360, 6 },
      { 12361, 7 },  { 12362, 8 },  { 12346, 9 },  { 12347, 10 }, { 12359, 11 }, { 12345, 12 }, { 12357, 13 },
      { 12358, 14 }, { 12344, 15 }, { 12355, 16 }, { 12356, 17 }, { 12341, 18 }, { 12342, 19 }, { 12343, 20 },
      { 12352, 21 }, { 12353, 22 }, { 12354, 23 }, { 12349, 24 }, { 12350, 25 }, { 12351, 26 }, { 12313, 27 },
      { 12314, 28 }, { 12348, 29 }, { 12310, 30 }, { 12311, 31 }, { 12312, 32 }, { 12300, 33 }, { 12301, 34 },
      { 12309, 35 }, { 12297, 36 }, { 12298, 37 }, { 12299, 38 }, { 12307, 39 }, { 12308, 40 }, { 12296, 41 },
      { 12294, 42 }, { 12295, 43 }, { 12306, 44 }, { 12293, 45 }, { 12304, 46 }, { 12305, 47 }, { 12292, 48 },
      { 12302, 49 }, { 12303, 50 }, { 12289, 51 }, { 12290, 52 }, { 12291, 53 } };

  UT_TELL40BinsCSide_Flow1 = {
      { 13390, 0 },  { 13391, 1 },  { 13392, 2 },  { 13387, 3 },  { 13388, 4 },  { 13389, 5 },  { 13384, 6 },
      { 13385, 7 },  { 13386, 8 },  { 13370, 9 },  { 13371, 10 }, { 13383, 11 }, { 13369, 12 }, { 13381, 13 },
      { 13382, 14 }, { 13368, 15 }, { 13379, 16 }, { 13380, 17 }, { 13365, 18 }, { 13366, 19 }, { 13367, 20 },
      { 13376, 21 }, { 13377, 22 }, { 13378, 23 }, { 13373, 24 }, { 13374, 25 }, { 13375, 26 }, { 13337, 27 },
      { 13338, 28 }, { 13372, 29 }, { 13334, 30 }, { 13335, 31 }, { 13336, 32 }, { 13324, 33 }, { 13325, 34 },
      { 13333, 35 }, { 13321, 36 }, { 13322, 37 }, { 13323, 38 }, { 13331, 39 }, { 13332, 40 }, { 13320, 41 },
      { 13318, 42 }, { 13319, 43 }, { 13330, 44 }, { 13317, 45 }, { 13328, 46 }, { 13329, 47 }, { 13316, 48 },
      { 13326, 49 }, { 13327, 50 }, { 13313, 51 }, { 13314, 52 }, { 13315, 53 } };

  UT_ModulesMap = { { "UTaU_1AB_M1E", 5516800 }, { "UTaU_1AB_M1W", 5516288 }, { "UTaU_1AB_M2", 5515264 },
                    { "UTaU_1AB_M3", 5514240 },  { "UTaU_1AB_M4", 5513216 },  { "UTaU_1AB_S1E", 5508608 },
                    { "UTaU_1AB_S1W", 5508096 }, { "UTaU_1AB_S2E", 5507584 }, { "UTaU_1AB_S2W", 5507072 },
                    { "UTaU_1AB_S3", 5506048 },  { "UTaU_1AB_S4", 5505024 },  { "UTaU_1AT_M1E", 5509632 },
                    { "UTaU_1AT_M1W", 5509120 }, { "UTaU_1AT_M2", 5510144 },  { "UTaU_1AT_M3", 5511168 },
                    { "UTaU_1AT_M4", 5512192 },  { "UTaU_1AT_S1E", 5517824 }, { "UTaU_1AT_S1W", 5517312 },
                    { "UTaU_1AT_S2E", 5518848 }, { "UTaU_1AT_S2W", 5518336 }, { "UTaU_1AT_S3", 5519360 },
                    { "UTaU_1AT_S4", 5520384 },  { "UTaU_1CB_M1E", 4468224 }, { "UTaU_1CB_M1W", 4467712 },
                    { "UTaU_1CB_M2", 4466688 },  { "UTaU_1CB_M3", 4465664 },  { "UTaU_1CB_M4", 4464640 },
                    { "UTaU_1CB_S1E", 4460032 }, { "UTaU_1CB_S1W", 4459520 }, { "UTaU_1CB_S2E", 4459008 },
                    { "UTaU_1CB_S2W", 4458496 }, { "UTaU_1CB_S3", 4457472 },  { "UTaU_1CB_S4", 4456448 },
                    { "UTaU_1CT_M1E", 4461056 }, { "UTaU_1CT_M1W", 4460544 }, { "UTaU_1CT_M2", 4461568 },
                    { "UTaU_1CT_M3", 4462592 },  { "UTaU_1CT_M4", 4463616 },  { "UTaU_1CT_S1E", 4469248 },
                    { "UTaU_1CT_S1W", 4468736 }, { "UTaU_1CT_S2E", 4470272 }, { "UTaU_1CT_S2W", 4469760 },
                    { "UTaU_1CT_S3", 4470784 },  { "UTaU_1CT_S4", 4471808 },  { "UTaU_2AB_M1E", 5533184 },
                    { "UTaU_2AB_M1W", 5532672 }, { "UTaU_2AB_M2", 5531648 },  { "UTaU_2AB_M3", 5530624 },
                    { "UTaU_2AB_M4", 5529600 },  { "UTaU_2AB_S1E", 5523968 }, { "UTaU_2AB_S1W", 5523456 },
                    { "UTaU_2AB_S2", 5522432 },  { "UTaU_2AB_S3", 5521408 },  { "UTaU_2AT_M1E", 5526016 },
                    { "UTaU_2AT_M1W", 5525504 }, { "UTaU_2AT_M2", 5526528 },  { "UTaU_2AT_M3", 5527552 },
                    { "UTaU_2AT_M4", 5528576 },  { "UTaU_2AT_S1E", 5535232 }, { "UTaU_2AT_S1W", 5534720 },
                    { "UTaU_2AT_S2", 5535744 },  { "UTaU_2AT_S3", 5536768 },  { "UTaU_2CB_M1E", 4484608 },
                    { "UTaU_2CB_M1W", 4484096 }, { "UTaU_2CB_M2", 4483072 },  { "UTaU_2CB_M3", 4482048 },
                    { "UTaU_2CB_M4", 4481024 },  { "UTaU_2CB_S1E", 4475392 }, { "UTaU_2CB_S1W", 4474880 },
                    { "UTaU_2CB_S2", 4473856 },  { "UTaU_2CB_S3", 4472832 },  { "UTaU_2CT_M1E", 4477440 },
                    { "UTaU_2CT_M1W", 4476928 }, { "UTaU_2CT_M2", 4477952 },  { "UTaU_2CT_M3", 4478976 },
                    { "UTaU_2CT_M4", 4480000 },  { "UTaU_2CT_S1E", 4486656 }, { "UTaU_2CT_S1W", 4486144 },
                    { "UTaU_2CT_S2", 4487168 },  { "UTaU_2CT_S3", 4488192 },  { "UTaU_3AB_M1", 5549056 },
                    { "UTaU_3AB_M2", 5548032 },  { "UTaU_3AB_M3", 5547008 },  { "UTaU_3AB_M4", 5545984 },
                    { "UTaU_3AB_S1", 5539840 },  { "UTaU_3AB_S2", 5538816 },  { "UTaU_3AB_S3", 5537792 },
                    { "UTaU_3AT_M1", 5541888 },  { "UTaU_3AT_M2", 5542912 },  { "UTaU_3AT_M3", 5543936 },
                    { "UTaU_3AT_M4", 5544960 },  { "UTaU_3AT_S1", 5551104 },  { "UTaU_3AT_S2", 5552128 },
                    { "UTaU_3AT_S3", 5553152 },  { "UTaU_3CB_M1", 4500480 },  { "UTaU_3CB_M2", 4499456 },
                    { "UTaU_3CB_M3", 4498432 },  { "UTaU_3CB_M4", 4497408 },  { "UTaU_3CB_S1", 4491264 },
                    { "UTaU_3CB_S2", 4490240 },  { "UTaU_3CB_S3", 4489216 },  { "UTaU_3CT_M1", 4493312 },
                    { "UTaU_3CT_M2", 4494336 },  { "UTaU_3CT_M3", 4495360 },  { "UTaU_3CT_M4", 4496384 },
                    { "UTaU_3CT_S1", 4502528 },  { "UTaU_3CT_S2", 4503552 },  { "UTaU_3CT_S3", 4504576 },
                    { "UTaU_4AB_M1", 5565440 },  { "UTaU_4AB_M2", 5564416 },  { "UTaU_4AB_M3", 5563392 },
                    { "UTaU_4AB_M4", 5562368 },  { "UTaU_4AB_S1", 5556224 },  { "UTaU_4AB_S2", 5555200 },
                    { "UTaU_4AB_S3", 5554176 },  { "UTaU_4AT_M1", 5558272 },  { "UTaU_4AT_M2", 5559296 },
                    { "UTaU_4AT_M3", 5560320 },  { "UTaU_4AT_M4", 5561344 },  { "UTaU_4AT_S1", 5567488 },
                    { "UTaU_4AT_S2", 5568512 },  { "UTaU_4AT_S3", 5569536 },  { "UTaU_4CB_M1", 4516864 },
                    { "UTaU_4CB_M2", 4515840 },  { "UTaU_4CB_M3", 4514816 },  { "UTaU_4CB_M4", 4513792 },
                    { "UTaU_4CB_S1", 4507648 },  { "UTaU_4CB_S2", 4506624 },  { "UTaU_4CB_S3", 4505600 },
                    { "UTaU_4CT_M1", 4509696 },  { "UTaU_4CT_M2", 4510720 },  { "UTaU_4CT_M3", 4511744 },
                    { "UTaU_4CT_M4", 4512768 },  { "UTaU_4CT_S1", 4518912 },  { "UTaU_4CT_S2", 4519936 },
                    { "UTaU_4CT_S3", 4520960 },  { "UTaU_5AB_M1", 5581824 },  { "UTaU_5AB_M2", 5580800 },
                    { "UTaU_5AB_M3", 5579776 },  { "UTaU_5AB_M4", 5578752 },  { "UTaU_5AB_S1", 5572608 },
                    { "UTaU_5AB_S2", 5571584 },  { "UTaU_5AB_S3", 5570560 },  { "UTaU_5AT_M1", 5574656 },
                    { "UTaU_5AT_M2", 5575680 },  { "UTaU_5AT_M3", 5576704 },  { "UTaU_5AT_M4", 5577728 },
                    { "UTaU_5AT_S1", 5583872 },  { "UTaU_5AT_S2", 5584896 },  { "UTaU_5AT_S3", 5585920 },
                    { "UTaU_5CB_M1", 4533248 },  { "UTaU_5CB_M2", 4532224 },  { "UTaU_5CB_M3", 4531200 },
                    { "UTaU_5CB_M4", 4530176 },  { "UTaU_5CB_S1", 4524032 },  { "UTaU_5CB_S2", 4523008 },
                    { "UTaU_5CB_S3", 4521984 },  { "UTaU_5CT_M1", 4526080 },  { "UTaU_5CT_M2", 4527104 },
                    { "UTaU_5CT_M3", 4528128 },  { "UTaU_5CT_M4", 4529152 },  { "UTaU_5CT_S1", 4535296 },
                    { "UTaU_5CT_S2", 4536320 },  { "UTaU_5CT_S3", 4537344 },  { "UTaU_6AB_M1", 5598208 },
                    { "UTaU_6AB_M2", 5597184 },  { "UTaU_6AB_M3", 5596160 },  { "UTaU_6AB_M4", 5595136 },
                    { "UTaU_6AB_S1", 5588992 },  { "UTaU_6AB_S2", 5587968 },  { "UTaU_6AB_S3", 5586944 },
                    { "UTaU_6AT_M1", 5591040 },  { "UTaU_6AT_M2", 5592064 },  { "UTaU_6AT_M3", 5593088 },
                    { "UTaU_6AT_M4", 5594112 },  { "UTaU_6AT_S1", 5600256 },  { "UTaU_6AT_S2", 5601280 },
                    { "UTaU_6AT_S3", 5602304 },  { "UTaU_6CB_M1", 4549632 },  { "UTaU_6CB_M2", 4548608 },
                    { "UTaU_6CB_M3", 4547584 },  { "UTaU_6CB_M4", 4546560 },  { "UTaU_6CB_S1", 4540416 },
                    { "UTaU_6CB_S2", 4539392 },  { "UTaU_6CB_S3", 4538368 },  { "UTaU_6CT_M1", 4542464 },
                    { "UTaU_6CT_M2", 4543488 },  { "UTaU_6CT_M3", 4544512 },  { "UTaU_6CT_M4", 4545536 },
                    { "UTaU_6CT_S1", 4551680 },  { "UTaU_6CT_S2", 4552704 },  { "UTaU_6CT_S3", 4553728 },
                    { "UTaU_7AB_M1", 5614592 },  { "UTaU_7AB_M2", 5613568 },  { "UTaU_7AB_M3", 5612544 },
                    { "UTaU_7AB_M4", 5611520 },  { "UTaU_7AB_S1", 5605376 },  { "UTaU_7AB_S2", 5604352 },
                    { "UTaU_7AB_S3", 5603328 },  { "UTaU_7AT_M1", 5607424 },  { "UTaU_7AT_M2", 5608448 },
                    { "UTaU_7AT_M3", 5609472 },  { "UTaU_7AT_M4", 5610496 },  { "UTaU_7AT_S1", 5616640 },
                    { "UTaU_7AT_S2", 5617664 },  { "UTaU_7AT_S3", 5618688 },  { "UTaU_7CB_M1", 4566016 },
                    { "UTaU_7CB_M2", 4564992 },  { "UTaU_7CB_M3", 4563968 },  { "UTaU_7CB_M4", 4562944 },
                    { "UTaU_7CB_S1", 4556800 },  { "UTaU_7CB_S2", 4555776 },  { "UTaU_7CB_S3", 4554752 },
                    { "UTaU_7CT_M1", 4558848 },  { "UTaU_7CT_M2", 4559872 },  { "UTaU_7CT_M3", 4560896 },
                    { "UTaU_7CT_M4", 4561920 },  { "UTaU_7CT_S1", 4568064 },  { "UTaU_7CT_S2", 4569088 },
                    { "UTaU_7CT_S3", 4570112 },  { "UTaU_8AB_M1", 5630976 },  { "UTaU_8AB_M2", 5629952 },
                    { "UTaU_8AB_M3", 5628928 },  { "UTaU_8AB_M4", 5627904 },  { "UTaU_8AB_S1", 5621760 },
                    { "UTaU_8AB_S2", 5620736 },  { "UTaU_8AB_S3", 5619712 },  { "UTaU_8AT_M1", 5623808 },
                    { "UTaU_8AT_M2", 5624832 },  { "UTaU_8AT_M3", 5625856 },  { "UTaU_8AT_M4", 5626880 },
                    { "UTaU_8AT_S1", 5633024 },  { "UTaU_8AT_S2", 5634048 },  { "UTaU_8AT_S3", 5635072 },
                    { "UTaU_8CB_M1", 4582400 },  { "UTaU_8CB_M2", 4581376 },  { "UTaU_8CB_M3", 4580352 },
                    { "UTaU_8CB_M4", 4579328 },  { "UTaU_8CB_S1", 4573184 },  { "UTaU_8CB_S2", 4572160 },
                    { "UTaU_8CB_S3", 4571136 },  { "UTaU_8CT_M1", 4575232 },  { "UTaU_8CT_M2", 4576256 },
                    { "UTaU_8CT_M3", 4577280 },  { "UTaU_8CT_M4", 4578304 },  { "UTaU_8CT_S1", 4584448 },
                    { "UTaU_8CT_S2", 4585472 },  { "UTaU_8CT_S3", 4586496 },  { "UTaX_1AB_M1E", 5254656 },
                    { "UTaX_1AB_M1W", 5254144 }, { "UTaX_1AB_M2", 5253120 },  { "UTaX_1AB_M3", 5252096 },
                    { "UTaX_1AB_M4", 5251072 },  { "UTaX_1AB_S1E", 5246464 }, { "UTaX_1AB_S1W", 5245952 },
                    { "UTaX_1AB_S2E", 5245440 }, { "UTaX_1AB_S2W", 5244928 }, { "UTaX_1AB_S3", 5243904 },
                    { "UTaX_1AB_S4", 5242880 },  { "UTaX_1AT_M1E", 5247488 }, { "UTaX_1AT_M1W", 5246976 },
                    { "UTaX_1AT_M2", 5248000 },  { "UTaX_1AT_M3", 5249024 },  { "UTaX_1AT_M4", 5250048 },
                    { "UTaX_1AT_S1E", 5255680 }, { "UTaX_1AT_S1W", 5255168 }, { "UTaX_1AT_S2E", 5256704 },
                    { "UTaX_1AT_S2W", 5256192 }, { "UTaX_1AT_S3", 5257216 },  { "UTaX_1AT_S4", 5258240 },
                    { "UTaX_1CB_M1E", 4206080 }, { "UTaX_1CB_M1W", 4205568 }, { "UTaX_1CB_M2", 4204544 },
                    { "UTaX_1CB_M3", 4203520 },  { "UTaX_1CB_M4", 4202496 },  { "UTaX_1CB_S1E", 4197888 },
                    { "UTaX_1CB_S1W", 4197376 }, { "UTaX_1CB_S2E", 4196864 }, { "UTaX_1CB_S2W", 4196352 },
                    { "UTaX_1CB_S3", 4195328 },  { "UTaX_1CB_S4", 4194304 },  { "UTaX_1CT_M1E", 4198912 },
                    { "UTaX_1CT_M1W", 4198400 }, { "UTaX_1CT_M2", 4199424 },  { "UTaX_1CT_M3", 4200448 },
                    { "UTaX_1CT_M4", 4201472 },  { "UTaX_1CT_S1E", 4207104 }, { "UTaX_1CT_S1W", 4206592 },
                    { "UTaX_1CT_S2E", 4208128 }, { "UTaX_1CT_S2W", 4207616 }, { "UTaX_1CT_S3", 4208640 },
                    { "UTaX_1CT_S4", 4209664 },  { "UTaX_2AB_M1E", 5271040 }, { "UTaX_2AB_M1W", 5270528 },
                    { "UTaX_2AB_M2", 5269504 },  { "UTaX_2AB_M3", 5268480 },  { "UTaX_2AB_M4", 5267456 },
                    { "UTaX_2AB_S1E", 5261824 }, { "UTaX_2AB_S1W", 5261312 }, { "UTaX_2AB_S2", 5260288 },
                    { "UTaX_2AB_S3", 5259264 },  { "UTaX_2AT_M1E", 5263872 }, { "UTaX_2AT_M1W", 5263360 },
                    { "UTaX_2AT_M2", 5264384 },  { "UTaX_2AT_M3", 5265408 },  { "UTaX_2AT_M4", 5266432 },
                    { "UTaX_2AT_S1E", 5273088 }, { "UTaX_2AT_S1W", 5272576 }, { "UTaX_2AT_S2", 5273600 },
                    { "UTaX_2AT_S3", 5274624 },  { "UTaX_2CB_M1E", 4222464 }, { "UTaX_2CB_M1W", 4221952 },
                    { "UTaX_2CB_M2", 4220928 },  { "UTaX_2CB_M3", 4219904 },  { "UTaX_2CB_M4", 4218880 },
                    { "UTaX_2CB_S1E", 4213248 }, { "UTaX_2CB_S1W", 4212736 }, { "UTaX_2CB_S2", 4211712 },
                    { "UTaX_2CB_S3", 4210688 },  { "UTaX_2CT_M1E", 4215296 }, { "UTaX_2CT_M1W", 4214784 },
                    { "UTaX_2CT_M2", 4215808 },  { "UTaX_2CT_M3", 4216832 },  { "UTaX_2CT_M4", 4217856 },
                    { "UTaX_2CT_S1E", 4224512 }, { "UTaX_2CT_S1W", 4224000 }, { "UTaX_2CT_S2", 4225024 },
                    { "UTaX_2CT_S3", 4226048 },  { "UTaX_3AB_M1", 5286912 },  { "UTaX_3AB_M2", 5285888 },
                    { "UTaX_3AB_M3", 5284864 },  { "UTaX_3AB_M4", 5283840 },  { "UTaX_3AB_S1", 5277696 },
                    { "UTaX_3AB_S2", 5276672 },  { "UTaX_3AB_S3", 5275648 },  { "UTaX_3AT_M1", 5279744 },
                    { "UTaX_3AT_M2", 5280768 },  { "UTaX_3AT_M3", 5281792 },  { "UTaX_3AT_M4", 5282816 },
                    { "UTaX_3AT_S1", 5288960 },  { "UTaX_3AT_S2", 5289984 },  { "UTaX_3AT_S3", 5291008 },
                    { "UTaX_3CB_M1", 4238336 },  { "UTaX_3CB_M2", 4237312 },  { "UTaX_3CB_M3", 4236288 },
                    { "UTaX_3CB_M4", 4235264 },  { "UTaX_3CB_S1", 4229120 },  { "UTaX_3CB_S2", 4228096 },
                    { "UTaX_3CB_S3", 4227072 },  { "UTaX_3CT_M1", 4231168 },  { "UTaX_3CT_M2", 4232192 },
                    { "UTaX_3CT_M3", 4233216 },  { "UTaX_3CT_M4", 4234240 },  { "UTaX_3CT_S1", 4240384 },
                    { "UTaX_3CT_S2", 4241408 },  { "UTaX_3CT_S3", 4242432 },  { "UTaX_4AB_M1", 5303296 },
                    { "UTaX_4AB_M2", 5302272 },  { "UTaX_4AB_M3", 5301248 },  { "UTaX_4AB_M4", 5300224 },
                    { "UTaX_4AB_S1", 5294080 },  { "UTaX_4AB_S2", 5293056 },  { "UTaX_4AB_S3", 5292032 },
                    { "UTaX_4AT_M1", 5296128 },  { "UTaX_4AT_M2", 5297152 },  { "UTaX_4AT_M3", 5298176 },
                    { "UTaX_4AT_M4", 5299200 },  { "UTaX_4AT_S1", 5305344 },  { "UTaX_4AT_S2", 5306368 },
                    { "UTaX_4AT_S3", 5307392 },  { "UTaX_4CB_M1", 4254720 },  { "UTaX_4CB_M2", 4253696 },
                    { "UTaX_4CB_M3", 4252672 },  { "UTaX_4CB_M4", 4251648 },  { "UTaX_4CB_S1", 4245504 },
                    { "UTaX_4CB_S2", 4244480 },  { "UTaX_4CB_S3", 4243456 },  { "UTaX_4CT_M1", 4247552 },
                    { "UTaX_4CT_M2", 4248576 },  { "UTaX_4CT_M3", 4249600 },  { "UTaX_4CT_M4", 4250624 },
                    { "UTaX_4CT_S1", 4256768 },  { "UTaX_4CT_S2", 4257792 },  { "UTaX_4CT_S3", 4258816 },
                    { "UTaX_5AB_M1", 5319680 },  { "UTaX_5AB_M2", 5318656 },  { "UTaX_5AB_M3", 5317632 },
                    { "UTaX_5AB_M4", 5316608 },  { "UTaX_5AB_S1", 5310464 },  { "UTaX_5AB_S2", 5309440 },
                    { "UTaX_5AB_S3", 5308416 },  { "UTaX_5AT_M1", 5312512 },  { "UTaX_5AT_M2", 5313536 },
                    { "UTaX_5AT_M3", 5314560 },  { "UTaX_5AT_M4", 5315584 },  { "UTaX_5AT_S1", 5321728 },
                    { "UTaX_5AT_S2", 5322752 },  { "UTaX_5AT_S3", 5323776 },  { "UTaX_5CB_M1", 4271104 },
                    { "UTaX_5CB_M2", 4270080 },  { "UTaX_5CB_M3", 4269056 },  { "UTaX_5CB_M4", 4268032 },
                    { "UTaX_5CB_S1", 4261888 },  { "UTaX_5CB_S2", 4260864 },  { "UTaX_5CB_S3", 4259840 },
                    { "UTaX_5CT_M1", 4263936 },  { "UTaX_5CT_M2", 4264960 },  { "UTaX_5CT_M3", 4265984 },
                    { "UTaX_5CT_M4", 4267008 },  { "UTaX_5CT_S1", 4273152 },  { "UTaX_5CT_S2", 4274176 },
                    { "UTaX_5CT_S3", 4275200 },  { "UTaX_6AB_M1", 5336064 },  { "UTaX_6AB_M2", 5335040 },
                    { "UTaX_6AB_M3", 5334016 },  { "UTaX_6AB_M4", 5332992 },  { "UTaX_6AB_S1", 5326848 },
                    { "UTaX_6AB_S2", 5325824 },  { "UTaX_6AB_S3", 5324800 },  { "UTaX_6AT_M1", 5328896 },
                    { "UTaX_6AT_M2", 5329920 },  { "UTaX_6AT_M3", 5330944 },  { "UTaX_6AT_M4", 5331968 },
                    { "UTaX_6AT_S1", 5338112 },  { "UTaX_6AT_S2", 5339136 },  { "UTaX_6AT_S3", 5340160 },
                    { "UTaX_6CB_M1", 4287488 },  { "UTaX_6CB_M2", 4286464 },  { "UTaX_6CB_M3", 4285440 },
                    { "UTaX_6CB_M4", 4284416 },  { "UTaX_6CB_S1", 4278272 },  { "UTaX_6CB_S2", 4277248 },
                    { "UTaX_6CB_S3", 4276224 },  { "UTaX_6CT_M1", 4280320 },  { "UTaX_6CT_M2", 4281344 },
                    { "UTaX_6CT_M3", 4282368 },  { "UTaX_6CT_M4", 4283392 },  { "UTaX_6CT_S1", 4289536 },
                    { "UTaX_6CT_S2", 4290560 },  { "UTaX_6CT_S3", 4291584 },  { "UTaX_7AB_M1", 5352448 },
                    { "UTaX_7AB_M2", 5351424 },  { "UTaX_7AB_M3", 5350400 },  { "UTaX_7AB_M4", 5349376 },
                    { "UTaX_7AB_S1", 5343232 },  { "UTaX_7AB_S2", 5342208 },  { "UTaX_7AB_S3", 5341184 },
                    { "UTaX_7AT_M1", 5345280 },  { "UTaX_7AT_M2", 5346304 },  { "UTaX_7AT_M3", 5347328 },
                    { "UTaX_7AT_M4", 5348352 },  { "UTaX_7AT_S1", 5354496 },  { "UTaX_7AT_S2", 5355520 },
                    { "UTaX_7AT_S3", 5356544 },  { "UTaX_7CB_M1", 4303872 },  { "UTaX_7CB_M2", 4302848 },
                    { "UTaX_7CB_M3", 4301824 },  { "UTaX_7CB_M4", 4300800 },  { "UTaX_7CB_S1", 4294656 },
                    { "UTaX_7CB_S2", 4293632 },  { "UTaX_7CB_S3", 4292608 },  { "UTaX_7CT_M1", 4296704 },
                    { "UTaX_7CT_M2", 4297728 },  { "UTaX_7CT_M3", 4298752 },  { "UTaX_7CT_M4", 4299776 },
                    { "UTaX_7CT_S1", 4305920 },  { "UTaX_7CT_S2", 4306944 },  { "UTaX_7CT_S3", 4307968 },
                    { "UTaX_8AB_M1", 5368832 },  { "UTaX_8AB_M2", 5367808 },  { "UTaX_8AB_M3", 5366784 },
                    { "UTaX_8AB_M4", 5365760 },  { "UTaX_8AB_S1", 5359616 },  { "UTaX_8AB_S2", 5358592 },
                    { "UTaX_8AB_S3", 5357568 },  { "UTaX_8AT_M1", 5361664 },  { "UTaX_8AT_M2", 5362688 },
                    { "UTaX_8AT_M3", 5363712 },  { "UTaX_8AT_M4", 5364736 },  { "UTaX_8AT_S1", 5370880 },
                    { "UTaX_8AT_S2", 5371904 },  { "UTaX_8AT_S3", 5372928 },  { "UTaX_8CB_M1", 4320256 },
                    { "UTaX_8CB_M2", 4319232 },  { "UTaX_8CB_M3", 4318208 },  { "UTaX_8CB_M4", 4317184 },
                    { "UTaX_8CB_S1", 4311040 },  { "UTaX_8CB_S2", 4310016 },  { "UTaX_8CB_S3", 4308992 },
                    { "UTaX_8CT_M1", 4313088 },  { "UTaX_8CT_M2", 4314112 },  { "UTaX_8CT_M3", 4315136 },
                    { "UTaX_8CT_M4", 4316160 },  { "UTaX_8CT_S1", 4322304 },  { "UTaX_8CT_S2", 4323328 },
                    { "UTaX_8CT_S3", 4324352 },  { "UTbV_1AB_M1E", 5778944 }, { "UTbV_1AB_M1W", 5778432 },
                    { "UTbV_1AB_M2", 5777408 },  { "UTbV_1AB_M3", 5776384 },  { "UTbV_1AB_M4", 5775360 },
                    { "UTbV_1AB_S1E", 5770752 }, { "UTbV_1AB_S1W", 5770240 }, { "UTbV_1AB_S2E", 5769728 },
                    { "UTbV_1AB_S2W", 5769216 }, { "UTbV_1AB_S3", 5768192 },  { "UTbV_1AB_S4", 5767168 },
                    { "UTbV_1AT_M1E", 5771776 }, { "UTbV_1AT_M1W", 5771264 }, { "UTbV_1AT_M2", 5772288 },
                    { "UTbV_1AT_M3", 5773312 },  { "UTbV_1AT_M4", 5774336 },  { "UTbV_1AT_S1E", 5779968 },
                    { "UTbV_1AT_S1W", 5779456 }, { "UTbV_1AT_S2E", 5780992 }, { "UTbV_1AT_S2W", 5780480 },
                    { "UTbV_1AT_S3", 5781504 },  { "UTbV_1AT_S4", 5782528 },  { "UTbV_1CB_M1E", 4730368 },
                    { "UTbV_1CB_M1W", 4729856 }, { "UTbV_1CB_M2", 4728832 },  { "UTbV_1CB_M3", 4727808 },
                    { "UTbV_1CB_M4", 4726784 },  { "UTbV_1CB_S1E", 4722176 }, { "UTbV_1CB_S1W", 4721664 },
                    { "UTbV_1CB_S2E", 4721152 }, { "UTbV_1CB_S2W", 4720640 }, { "UTbV_1CB_S3", 4719616 },
                    { "UTbV_1CB_S4", 4718592 },  { "UTbV_1CT_M1E", 4723200 }, { "UTbV_1CT_M1W", 4722688 },
                    { "UTbV_1CT_M2", 4723712 },  { "UTbV_1CT_M3", 4724736 },  { "UTbV_1CT_M4", 4725760 },
                    { "UTbV_1CT_S1E", 4731392 }, { "UTbV_1CT_S1W", 4730880 }, { "UTbV_1CT_S2E", 4732416 },
                    { "UTbV_1CT_S2W", 4731904 }, { "UTbV_1CT_S3", 4732928 },  { "UTbV_1CT_S4", 4733952 },
                    { "UTbV_2AB_M1E", 5795328 }, { "UTbV_2AB_M1W", 5794816 }, { "UTbV_2AB_M2", 5793792 },
                    { "UTbV_2AB_M3", 5792768 },  { "UTbV_2AB_M4", 5791744 },  { "UTbV_2AB_S1E", 5786112 },
                    { "UTbV_2AB_S1W", 5785600 }, { "UTbV_2AB_S2", 5784576 },  { "UTbV_2AB_S3", 5783552 },
                    { "UTbV_2AT_M1E", 5788160 }, { "UTbV_2AT_M1W", 5787648 }, { "UTbV_2AT_M2", 5788672 },
                    { "UTbV_2AT_M3", 5789696 },  { "UTbV_2AT_M4", 5790720 },  { "UTbV_2AT_S1E", 5797376 },
                    { "UTbV_2AT_S1W", 5796864 }, { "UTbV_2AT_S2", 5797888 },  { "UTbV_2AT_S3", 5798912 },
                    { "UTbV_2CB_M1E", 4746752 }, { "UTbV_2CB_M1W", 4746240 }, { "UTbV_2CB_M2", 4745216 },
                    { "UTbV_2CB_M3", 4744192 },  { "UTbV_2CB_M4", 4743168 },  { "UTbV_2CB_S1E", 4737536 },
                    { "UTbV_2CB_S1W", 4737024 }, { "UTbV_2CB_S2", 4736000 },  { "UTbV_2CB_S3", 4734976 },
                    { "UTbV_2CT_M1E", 4739584 }, { "UTbV_2CT_M1W", 4739072 }, { "UTbV_2CT_M2", 4740096 },
                    { "UTbV_2CT_M3", 4741120 },  { "UTbV_2CT_M4", 4742144 },  { "UTbV_2CT_S1E", 4748800 },
                    { "UTbV_2CT_S1W", 4748288 }, { "UTbV_2CT_S2", 4749312 },  { "UTbV_2CT_S3", 4750336 },
                    { "UTbV_3AB_M1", 5811200 },  { "UTbV_3AB_M2", 5810176 },  { "UTbV_3AB_M3", 5809152 },
                    { "UTbV_3AB_M4", 5808128 },  { "UTbV_3AB_S1", 5801984 },  { "UTbV_3AB_S2", 5800960 },
                    { "UTbV_3AB_S3", 5799936 },  { "UTbV_3AT_M1", 5804032 },  { "UTbV_3AT_M2", 5805056 },
                    { "UTbV_3AT_M3", 5806080 },  { "UTbV_3AT_M4", 5807104 },  { "UTbV_3AT_S1", 5813248 },
                    { "UTbV_3AT_S2", 5814272 },  { "UTbV_3AT_S3", 5815296 },  { "UTbV_3CB_M1", 4762624 },
                    { "UTbV_3CB_M2", 4761600 },  { "UTbV_3CB_M3", 4760576 },  { "UTbV_3CB_M4", 4759552 },
                    { "UTbV_3CB_S1", 4753408 },  { "UTbV_3CB_S2", 4752384 },  { "UTbV_3CB_S3", 4751360 },
                    { "UTbV_3CT_M1", 4755456 },  { "UTbV_3CT_M2", 4756480 },  { "UTbV_3CT_M3", 4757504 },
                    { "UTbV_3CT_M4", 4758528 },  { "UTbV_3CT_S1", 4764672 },  { "UTbV_3CT_S2", 4765696 },
                    { "UTbV_3CT_S3", 4766720 },  { "UTbV_4AB_M1", 5827584 },  { "UTbV_4AB_M2", 5826560 },
                    { "UTbV_4AB_M3", 5825536 },  { "UTbV_4AB_M4", 5824512 },  { "UTbV_4AB_S1", 5818368 },
                    { "UTbV_4AB_S2", 5817344 },  { "UTbV_4AB_S3", 5816320 },  { "UTbV_4AT_M1", 5820416 },
                    { "UTbV_4AT_M2", 5821440 },  { "UTbV_4AT_M3", 5822464 },  { "UTbV_4AT_M4", 5823488 },
                    { "UTbV_4AT_S1", 5829632 },  { "UTbV_4AT_S2", 5830656 },  { "UTbV_4AT_S3", 5831680 },
                    { "UTbV_4CB_M1", 4779008 },  { "UTbV_4CB_M2", 4777984 },  { "UTbV_4CB_M3", 4776960 },
                    { "UTbV_4CB_M4", 4775936 },  { "UTbV_4CB_S1", 4769792 },  { "UTbV_4CB_S2", 4768768 },
                    { "UTbV_4CB_S3", 4767744 },  { "UTbV_4CT_M1", 4771840 },  { "UTbV_4CT_M2", 4772864 },
                    { "UTbV_4CT_M3", 4773888 },  { "UTbV_4CT_M4", 4774912 },  { "UTbV_4CT_S1", 4781056 },
                    { "UTbV_4CT_S2", 4782080 },  { "UTbV_4CT_S3", 4783104 },  { "UTbV_5AB_M1", 5843968 },
                    { "UTbV_5AB_M2", 5842944 },  { "UTbV_5AB_M3", 5841920 },  { "UTbV_5AB_M4", 5840896 },
                    { "UTbV_5AB_S1", 5834752 },  { "UTbV_5AB_S2", 5833728 },  { "UTbV_5AB_S3", 5832704 },
                    { "UTbV_5AT_M1", 5836800 },  { "UTbV_5AT_M2", 5837824 },  { "UTbV_5AT_M3", 5838848 },
                    { "UTbV_5AT_M4", 5839872 },  { "UTbV_5AT_S1", 5846016 },  { "UTbV_5AT_S2", 5847040 },
                    { "UTbV_5AT_S3", 5848064 },  { "UTbV_5CB_M1", 4795392 },  { "UTbV_5CB_M2", 4794368 },
                    { "UTbV_5CB_M3", 4793344 },  { "UTbV_5CB_M4", 4792320 },  { "UTbV_5CB_S1", 4786176 },
                    { "UTbV_5CB_S2", 4785152 },  { "UTbV_5CB_S3", 4784128 },  { "UTbV_5CT_M1", 4788224 },
                    { "UTbV_5CT_M2", 4789248 },  { "UTbV_5CT_M3", 4790272 },  { "UTbV_5CT_M4", 4791296 },
                    { "UTbV_5CT_S1", 4797440 },  { "UTbV_5CT_S2", 4798464 },  { "UTbV_5CT_S3", 4799488 },
                    { "UTbV_6AB_M1", 5860352 },  { "UTbV_6AB_M2", 5859328 },  { "UTbV_6AB_M3", 5858304 },
                    { "UTbV_6AB_M4", 5857280 },  { "UTbV_6AB_S1", 5851136 },  { "UTbV_6AB_S2", 5850112 },
                    { "UTbV_6AB_S3", 5849088 },  { "UTbV_6AT_M1", 5853184 },  { "UTbV_6AT_M2", 5854208 },
                    { "UTbV_6AT_M3", 5855232 },  { "UTbV_6AT_M4", 5856256 },  { "UTbV_6AT_S1", 5862400 },
                    { "UTbV_6AT_S2", 5863424 },  { "UTbV_6AT_S3", 5864448 },  { "UTbV_6CB_M1", 4811776 },
                    { "UTbV_6CB_M2", 4810752 },  { "UTbV_6CB_M3", 4809728 },  { "UTbV_6CB_M4", 4808704 },
                    { "UTbV_6CB_S1", 4802560 },  { "UTbV_6CB_S2", 4801536 },  { "UTbV_6CB_S3", 4800512 },
                    { "UTbV_6CT_M1", 4804608 },  { "UTbV_6CT_M2", 4805632 },  { "UTbV_6CT_M3", 4806656 },
                    { "UTbV_6CT_M4", 4807680 },  { "UTbV_6CT_S1", 4813824 },  { "UTbV_6CT_S2", 4814848 },
                    { "UTbV_6CT_S3", 4815872 },  { "UTbV_7AB_M1", 5876736 },  { "UTbV_7AB_M2", 5875712 },
                    { "UTbV_7AB_M3", 5874688 },  { "UTbV_7AB_M4", 5873664 },  { "UTbV_7AB_S1", 5867520 },
                    { "UTbV_7AB_S2", 5866496 },  { "UTbV_7AB_S3", 5865472 },  { "UTbV_7AT_M1", 5869568 },
                    { "UTbV_7AT_M2", 5870592 },  { "UTbV_7AT_M3", 5871616 },  { "UTbV_7AT_M4", 5872640 },
                    { "UTbV_7AT_S1", 5878784 },  { "UTbV_7AT_S2", 5879808 },  { "UTbV_7AT_S3", 5880832 },
                    { "UTbV_7CB_M1", 4828160 },  { "UTbV_7CB_M2", 4827136 },  { "UTbV_7CB_M3", 4826112 },
                    { "UTbV_7CB_M4", 4825088 },  { "UTbV_7CB_S1", 4818944 },  { "UTbV_7CB_S2", 4817920 },
                    { "UTbV_7CB_S3", 4816896 },  { "UTbV_7CT_M1", 4820992 },  { "UTbV_7CT_M2", 4822016 },
                    { "UTbV_7CT_M3", 4823040 },  { "UTbV_7CT_M4", 4824064 },  { "UTbV_7CT_S1", 4830208 },
                    { "UTbV_7CT_S2", 4831232 },  { "UTbV_7CT_S3", 4832256 },  { "UTbV_8AB_M1", 5893120 },
                    { "UTbV_8AB_M2", 5892096 },  { "UTbV_8AB_M3", 5891072 },  { "UTbV_8AB_M4", 5890048 },
                    { "UTbV_8AB_S1", 5883904 },  { "UTbV_8AB_S2", 5882880 },  { "UTbV_8AB_S3", 5881856 },
                    { "UTbV_8AT_M1", 5885952 },  { "UTbV_8AT_M2", 5886976 },  { "UTbV_8AT_M3", 5888000 },
                    { "UTbV_8AT_M4", 5889024 },  { "UTbV_8AT_S1", 5895168 },  { "UTbV_8AT_S2", 5896192 },
                    { "UTbV_8AT_S3", 5897216 },  { "UTbV_8CB_M1", 4844544 },  { "UTbV_8CB_M2", 4843520 },
                    { "UTbV_8CB_M3", 4842496 },  { "UTbV_8CB_M4", 4841472 },  { "UTbV_8CB_S1", 4835328 },
                    { "UTbV_8CB_S2", 4834304 },  { "UTbV_8CB_S3", 4833280 },  { "UTbV_8CT_M1", 4837376 },
                    { "UTbV_8CT_M2", 4838400 },  { "UTbV_8CT_M3", 4839424 },  { "UTbV_8CT_M4", 4840448 },
                    { "UTbV_8CT_S1", 4846592 },  { "UTbV_8CT_S2", 4847616 },  { "UTbV_8CT_S3", 4848640 },
                    { "UTbV_9AB_M1", 5909504 },  { "UTbV_9AB_M2", 5908480 },  { "UTbV_9AB_M3", 5907456 },
                    { "UTbV_9AB_M4", 5906432 },  { "UTbV_9AB_S1", 5900288 },  { "UTbV_9AB_S2", 5899264 },
                    { "UTbV_9AB_S3", 5898240 },  { "UTbV_9AT_M1", 5902336 },  { "UTbV_9AT_M2", 5903360 },
                    { "UTbV_9AT_M3", 5904384 },  { "UTbV_9AT_M4", 5905408 },  { "UTbV_9AT_S1", 5911552 },
                    { "UTbV_9AT_S2", 5912576 },  { "UTbV_9AT_S3", 5913600 },  { "UTbV_9CB_M1", 4860928 },
                    { "UTbV_9CB_M2", 4859904 },  { "UTbV_9CB_M3", 4858880 },  { "UTbV_9CB_M4", 4857856 },
                    { "UTbV_9CB_S1", 4851712 },  { "UTbV_9CB_S2", 4850688 },  { "UTbV_9CB_S3", 4849664 },
                    { "UTbV_9CT_M1", 4853760 },  { "UTbV_9CT_M2", 4854784 },  { "UTbV_9CT_M3", 4855808 },
                    { "UTbV_9CT_M4", 4856832 },  { "UTbV_9CT_S1", 4862976 },  { "UTbV_9CT_S2", 4864000 },
                    { "UTbV_9CT_S3", 4865024 },  { "UTbX_1AB_M1E", 6041088 }, { "UTbX_1AB_M1W", 6040576 },
                    { "UTbX_1AB_M2", 6039552 },  { "UTbX_1AB_M3", 6038528 },  { "UTbX_1AB_M4", 6037504 },
                    { "UTbX_1AB_S1E", 6032896 }, { "UTbX_1AB_S1W", 6032384 }, { "UTbX_1AB_S2E", 6031872 },
                    { "UTbX_1AB_S2W", 6031360 }, { "UTbX_1AB_S3", 6030336 },  { "UTbX_1AB_S4", 6029312 },
                    { "UTbX_1AT_M1E", 6033920 }, { "UTbX_1AT_M1W", 6033408 }, { "UTbX_1AT_M2", 6034432 },
                    { "UTbX_1AT_M3", 6035456 },  { "UTbX_1AT_M4", 6036480 },  { "UTbX_1AT_S1E", 6042112 },
                    { "UTbX_1AT_S1W", 6041600 }, { "UTbX_1AT_S2E", 6043136 }, { "UTbX_1AT_S2W", 6042624 },
                    { "UTbX_1AT_S3", 6043648 },  { "UTbX_1AT_S4", 6044672 },  { "UTbX_1CB_M1E", 4992512 },
                    { "UTbX_1CB_M1W", 4992000 }, { "UTbX_1CB_M2", 4990976 },  { "UTbX_1CB_M3", 4989952 },
                    { "UTbX_1CB_M4", 4988928 },  { "UTbX_1CB_S1E", 4984320 }, { "UTbX_1CB_S1W", 4983808 },
                    { "UTbX_1CB_S2E", 4983296 }, { "UTbX_1CB_S2W", 4982784 }, { "UTbX_1CB_S3", 4981760 },
                    { "UTbX_1CB_S4", 4980736 },  { "UTbX_1CT_M1E", 4985344 }, { "UTbX_1CT_M1W", 4984832 },
                    { "UTbX_1CT_M2", 4985856 },  { "UTbX_1CT_M3", 4986880 },  { "UTbX_1CT_M4", 4987904 },
                    { "UTbX_1CT_S1E", 4993536 }, { "UTbX_1CT_S1W", 4993024 }, { "UTbX_1CT_S2E", 4994560 },
                    { "UTbX_1CT_S2W", 4994048 }, { "UTbX_1CT_S3", 4995072 },  { "UTbX_1CT_S4", 4996096 },
                    { "UTbX_2AB_M1E", 6057472 }, { "UTbX_2AB_M1W", 6056960 }, { "UTbX_2AB_M2", 6055936 },
                    { "UTbX_2AB_M3", 6054912 },  { "UTbX_2AB_M4", 6053888 },  { "UTbX_2AB_S1E", 6048256 },
                    { "UTbX_2AB_S1W", 6047744 }, { "UTbX_2AB_S2", 6046720 },  { "UTbX_2AB_S3", 6045696 },
                    { "UTbX_2AT_M1E", 6050304 }, { "UTbX_2AT_M1W", 6049792 }, { "UTbX_2AT_M2", 6050816 },
                    { "UTbX_2AT_M3", 6051840 },  { "UTbX_2AT_M4", 6052864 },  { "UTbX_2AT_S1E", 6059520 },
                    { "UTbX_2AT_S1W", 6059008 }, { "UTbX_2AT_S2", 6060032 },  { "UTbX_2AT_S3", 6061056 },
                    { "UTbX_2CB_M1E", 5008896 }, { "UTbX_2CB_M1W", 5008384 }, { "UTbX_2CB_M2", 5007360 },
                    { "UTbX_2CB_M3", 5006336 },  { "UTbX_2CB_M4", 5005312 },  { "UTbX_2CB_S1E", 4999680 },
                    { "UTbX_2CB_S1W", 4999168 }, { "UTbX_2CB_S2", 4998144 },  { "UTbX_2CB_S3", 4997120 },
                    { "UTbX_2CT_M1E", 5001728 }, { "UTbX_2CT_M1W", 5001216 }, { "UTbX_2CT_M2", 5002240 },
                    { "UTbX_2CT_M3", 5003264 },  { "UTbX_2CT_M4", 5004288 },  { "UTbX_2CT_S1E", 5010944 },
                    { "UTbX_2CT_S1W", 5010432 }, { "UTbX_2CT_S2", 5011456 },  { "UTbX_2CT_S3", 5012480 },
                    { "UTbX_3AB_M1", 6073344 },  { "UTbX_3AB_M2", 6072320 },  { "UTbX_3AB_M3", 6071296 },
                    { "UTbX_3AB_M4", 6070272 },  { "UTbX_3AB_S1", 6064128 },  { "UTbX_3AB_S2", 6063104 },
                    { "UTbX_3AB_S3", 6062080 },  { "UTbX_3AT_M1", 6066176 },  { "UTbX_3AT_M2", 6067200 },
                    { "UTbX_3AT_M3", 6068224 },  { "UTbX_3AT_M4", 6069248 },  { "UTbX_3AT_S1", 6075392 },
                    { "UTbX_3AT_S2", 6076416 },  { "UTbX_3AT_S3", 6077440 },  { "UTbX_3CB_M1", 5024768 },
                    { "UTbX_3CB_M2", 5023744 },  { "UTbX_3CB_M3", 5022720 },  { "UTbX_3CB_M4", 5021696 },
                    { "UTbX_3CB_S1", 5015552 },  { "UTbX_3CB_S2", 5014528 },  { "UTbX_3CB_S3", 5013504 },
                    { "UTbX_3CT_M1", 5017600 },  { "UTbX_3CT_M2", 5018624 },  { "UTbX_3CT_M3", 5019648 },
                    { "UTbX_3CT_M4", 5020672 },  { "UTbX_3CT_S1", 5026816 },  { "UTbX_3CT_S2", 5027840 },
                    { "UTbX_3CT_S3", 5028864 },  { "UTbX_4AB_M1", 6089728 },  { "UTbX_4AB_M2", 6088704 },
                    { "UTbX_4AB_M3", 6087680 },  { "UTbX_4AB_M4", 6086656 },  { "UTbX_4AB_S1", 6080512 },
                    { "UTbX_4AB_S2", 6079488 },  { "UTbX_4AB_S3", 6078464 },  { "UTbX_4AT_M1", 6082560 },
                    { "UTbX_4AT_M2", 6083584 },  { "UTbX_4AT_M3", 6084608 },  { "UTbX_4AT_M4", 6085632 },
                    { "UTbX_4AT_S1", 6091776 },  { "UTbX_4AT_S2", 6092800 },  { "UTbX_4AT_S3", 6093824 },
                    { "UTbX_4CB_M1", 5041152 },  { "UTbX_4CB_M2", 5040128 },  { "UTbX_4CB_M3", 5039104 },
                    { "UTbX_4CB_M4", 5038080 },  { "UTbX_4CB_S1", 5031936 },  { "UTbX_4CB_S2", 5030912 },
                    { "UTbX_4CB_S3", 5029888 },  { "UTbX_4CT_M1", 5033984 },  { "UTbX_4CT_M2", 5035008 },
                    { "UTbX_4CT_M3", 5036032 },  { "UTbX_4CT_M4", 5037056 },  { "UTbX_4CT_S1", 5043200 },
                    { "UTbX_4CT_S2", 5044224 },  { "UTbX_4CT_S3", 5045248 },  { "UTbX_5AB_M1", 6106112 },
                    { "UTbX_5AB_M2", 6105088 },  { "UTbX_5AB_M3", 6104064 },  { "UTbX_5AB_M4", 6103040 },
                    { "UTbX_5AB_S1", 6096896 },  { "UTbX_5AB_S2", 6095872 },  { "UTbX_5AB_S3", 6094848 },
                    { "UTbX_5AT_M1", 6098944 },  { "UTbX_5AT_M2", 6099968 },  { "UTbX_5AT_M3", 6100992 },
                    { "UTbX_5AT_M4", 6102016 },  { "UTbX_5AT_S1", 6108160 },  { "UTbX_5AT_S2", 6109184 },
                    { "UTbX_5AT_S3", 6110208 },  { "UTbX_5CB_M1", 5057536 },  { "UTbX_5CB_M2", 5056512 },
                    { "UTbX_5CB_M3", 5055488 },  { "UTbX_5CB_M4", 5054464 },  { "UTbX_5CB_S1", 5048320 },
                    { "UTbX_5CB_S2", 5047296 },  { "UTbX_5CB_S3", 5046272 },  { "UTbX_5CT_M1", 5050368 },
                    { "UTbX_5CT_M2", 5051392 },  { "UTbX_5CT_M3", 5052416 },  { "UTbX_5CT_M4", 5053440 },
                    { "UTbX_5CT_S1", 5059584 },  { "UTbX_5CT_S2", 5060608 },  { "UTbX_5CT_S3", 5061632 },
                    { "UTbX_6AB_M1", 6122496 },  { "UTbX_6AB_M2", 6121472 },  { "UTbX_6AB_M3", 6120448 },
                    { "UTbX_6AB_M4", 6119424 },  { "UTbX_6AB_S1", 6113280 },  { "UTbX_6AB_S2", 6112256 },
                    { "UTbX_6AB_S3", 6111232 },  { "UTbX_6AT_M1", 6115328 },  { "UTbX_6AT_M2", 6116352 },
                    { "UTbX_6AT_M3", 6117376 },  { "UTbX_6AT_M4", 6118400 },  { "UTbX_6AT_S1", 6124544 },
                    { "UTbX_6AT_S2", 6125568 },  { "UTbX_6AT_S3", 6126592 },  { "UTbX_6CB_M1", 5073920 },
                    { "UTbX_6CB_M2", 5072896 },  { "UTbX_6CB_M3", 5071872 },  { "UTbX_6CB_M4", 5070848 },
                    { "UTbX_6CB_S1", 5064704 },  { "UTbX_6CB_S2", 5063680 },  { "UTbX_6CB_S3", 5062656 },
                    { "UTbX_6CT_M1", 5066752 },  { "UTbX_6CT_M2", 5067776 },  { "UTbX_6CT_M3", 5068800 },
                    { "UTbX_6CT_M4", 5069824 },  { "UTbX_6CT_S1", 5075968 },  { "UTbX_6CT_S2", 5076992 },
                    { "UTbX_6CT_S3", 5078016 },  { "UTbX_7AB_M1", 6138880 },  { "UTbX_7AB_M2", 6137856 },
                    { "UTbX_7AB_M3", 6136832 },  { "UTbX_7AB_M4", 6135808 },  { "UTbX_7AB_S1", 6129664 },
                    { "UTbX_7AB_S2", 6128640 },  { "UTbX_7AB_S3", 6127616 },  { "UTbX_7AT_M1", 6131712 },
                    { "UTbX_7AT_M2", 6132736 },  { "UTbX_7AT_M3", 6133760 },  { "UTbX_7AT_M4", 6134784 },
                    { "UTbX_7AT_S1", 6140928 },  { "UTbX_7AT_S2", 6141952 },  { "UTbX_7AT_S3", 6142976 },
                    { "UTbX_7CB_M1", 5090304 },  { "UTbX_7CB_M2", 5089280 },  { "UTbX_7CB_M3", 5088256 },
                    { "UTbX_7CB_M4", 5087232 },  { "UTbX_7CB_S1", 5081088 },  { "UTbX_7CB_S2", 5080064 },
                    { "UTbX_7CB_S3", 5079040 },  { "UTbX_7CT_M1", 5083136 },  { "UTbX_7CT_M2", 5084160 },
                    { "UTbX_7CT_M3", 5085184 },  { "UTbX_7CT_M4", 5086208 },  { "UTbX_7CT_S1", 5092352 },
                    { "UTbX_7CT_S2", 5093376 },  { "UTbX_7CT_S3", 5094400 },  { "UTbX_8AB_M1", 6155264 },
                    { "UTbX_8AB_M2", 6154240 },  { "UTbX_8AB_M3", 6153216 },  { "UTbX_8AB_M4", 6152192 },
                    { "UTbX_8AB_S1", 6146048 },  { "UTbX_8AB_S2", 6145024 },  { "UTbX_8AB_S3", 6144000 },
                    { "UTbX_8AT_M1", 6148096 },  { "UTbX_8AT_M2", 6149120 },  { "UTbX_8AT_M3", 6150144 },
                    { "UTbX_8AT_M4", 6151168 },  { "UTbX_8AT_S1", 6157312 },  { "UTbX_8AT_S2", 6158336 },
                    { "UTbX_8AT_S3", 6159360 },  { "UTbX_8CB_M1", 5106688 },  { "UTbX_8CB_M2", 5105664 },
                    { "UTbX_8CB_M3", 5104640 },  { "UTbX_8CB_M4", 5103616 },  { "UTbX_8CB_S1", 5097472 },
                    { "UTbX_8CB_S2", 5096448 },  { "UTbX_8CB_S3", 5095424 },  { "UTbX_8CT_M1", 5099520 },
                    { "UTbX_8CT_M2", 5100544 },  { "UTbX_8CT_M3", 5101568 },  { "UTbX_8CT_M4", 5102592 },
                    { "UTbX_8CT_S1", 5108736 },  { "UTbX_8CT_S2", 5109760 },  { "UTbX_8CT_S3", 5110784 },
                    { "UTbX_9AB_M1", 6171648 },  { "UTbX_9AB_M2", 6170624 },  { "UTbX_9AB_M3", 6169600 },
                    { "UTbX_9AB_M4", 6168576 },  { "UTbX_9AB_S1", 6162432 },  { "UTbX_9AB_S2", 6161408 },
                    { "UTbX_9AB_S3", 6160384 },  { "UTbX_9AT_M1", 6164480 },  { "UTbX_9AT_M2", 6165504 },
                    { "UTbX_9AT_M3", 6166528 },  { "UTbX_9AT_M4", 6167552 },  { "UTbX_9AT_S1", 6173696 },
                    { "UTbX_9AT_S2", 6174720 },  { "UTbX_9AT_S3", 6175744 },  { "UTbX_9CB_M1", 5123072 },
                    { "UTbX_9CB_M2", 5122048 },  { "UTbX_9CB_M3", 5121024 },  { "UTbX_9CB_M4", 5120000 },
                    { "UTbX_9CB_S1", 5113856 },  { "UTbX_9CB_S2", 5112832 },  { "UTbX_9CB_S3", 5111808 },
                    { "UTbX_9CT_M1", 5115904 },  { "UTbX_9CT_M2", 5116928 },  { "UTbX_9CT_M3", 5117952 },
                    { "UTbX_9CT_M4", 5118976 },  { "UTbX_9CT_S1", 5125120 },  { "UTbX_9CT_S2", 5126144 },
                    { "UTbX_9CT_S3", 5127168 } };
}
// Function to get the tuple at specified dimensions
std::tuple<float, float, std::string, std::string> UTCoordinatesMap::getTuple( int module, int face, int stave,
                                                                               int side, int sector ) const {
  return UT_CoordinatesMap[module][face][stave][side][sector];
}

unsigned int             UTCoordinatesMap::getChannel( std::string module ) { return UT_ModulesMap[module]; }
std::vector<std::string> UTCoordinatesMap::getNamesfromTELL40( std::string module ) {
  auto                     range = UT_TELL40toModuleName.equal_range( module );
  std::vector<std::string> values;
  for ( auto i = range.first; i != range.second; ++i ) { values.push_back( i->second ); }
  if ( values.empty() ) { std::cout << "Module " << module << " not found in UT_TELL40toModuleName." << std::endl; }
  return values;
}
std::string UTCoordinatesMap::getTELL40( unsigned int sourceID ) { return UT_TELL40Map[sourceID]; }

unsigned int UTCoordinatesMap::getTELL40Bin( unsigned int sourceID ) {
  if ( sourceID / 1000 == 10 ) // 10346-10269
    return UT_TELL40BinsASide_Flow0[sourceID];
  else if ( sourceID / 1000 == 11 ) // 11370- 11293
    return UT_TELL40BinsASide_Flow1[sourceID];
  else if ( sourceID / 1000 == 12 ) // 12366-12291
    return UT_TELL40BinsCSide_Flow0[sourceID];
  else if ( sourceID / 1000 == 13 ) // 13371-13315
    return UT_TELL40BinsCSide_Flow1[sourceID];
  else
    return 1;
}

std::vector<std::string> UTCoordinatesMap::getTELL40NamesASide() {
  std::vector<std::string> keys;
  for ( const auto& pair : UT_TELL40Map ) {
    if ( pair.second.find( "UA" ) != std::string::npos ) keys.push_back( pair.second );
  }
  return keys;
}

std::vector<std::string> UTCoordinatesMap::getTELL40NamesCSide() {
  std::vector<std::string> keys;
  for ( const auto& pair : UT_TELL40Map ) {
    if ( pair.second.find( "UC" ) != std::string::npos ) keys.push_back( pair.second );
  }
  return keys;
}

std::vector<std::string> UTCoordinatesMap::getModulesNames() {
  std::vector<std::string> keys;
  for ( const auto& pair : UT_ModulesMap ) { keys.push_back( pair.first ); }
  return keys;
}

unsigned int UTCoordinatesMap::getSourceIDfromBoardNumber( unsigned int boardID ) {
  return UT_BoardIDtoSourceID[boardID];
}
