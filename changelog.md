### Version: 1.1
 Date: 21.11.2023 \
 Author: W.Krupa


* Reimplementation of counter
* Implementation of the lanes counters
* Fixes in bank types decoding
* Fixes is debug massages
