###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organizatiodn  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import sys
import os
import glob
from Gaudi.Configuration import VERBOSE, DEBUG, INFO, ERROR
from Gaudi.Configuration import ApplicationMgr
from Configurables import LHCbApp
from Configurables import GaudiSequencer
from Configurables import LoKiSvc
from Configurables import UTDigitMonitor, UTRawBankToUTDigitsAlg, UTRawBankToUTNZSDigitsAlg
from GaudiConf import IOHelper
from Gaudi.Configuration import MessageSvc, INFO, DEBUG
from Configurables import Gaudi__Histograming__Sink__Root as RootHistoSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
from Configurables import createODIN
from Configurables import LHCb__UnpackRawEvent
from Configurables import EventSelector
from DDDB.CheckDD4Hep import UseDD4Hep

app = LHCbApp()
app.DataType = "Upgrade"
app.Simulation = False
app.EvtMax = 1

if UseDD4Hep:
    # Prepare detector description
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    dd4hepsvc = DD4hepSvc()
    dd4hepsvc.VerboseLevel = 1
    dd4hepsvc.GeometryLocation = "${DETECTOR_PROJECT_ROOT}/compact"
    dd4hepsvc.GeometryVersion = "run3/trunk"
    dd4hepsvc.GeometryMain = "LHCb.xml"
    dd4hepsvc.DetectorList = ["/world", "UT"]
    #dd4hepsvc.ConditionsVersion = "add-UT-ReadoutMap-Ion2023" # <- please add this line
    #dd4hepsvc.RunNumber=276525
    iovProd = IOVProducer("ReserveIOVDD4hep", ODIN='DAQ/ODIN')

else:
    # DetDesc case
    LHCbApp().DataType = "Upgrade"
    LHCbApp().DDDBtag = 'upgrade/UTv4r2-newUTID'
    LHCbApp().CondDBtag = "master"
    LHCbApp().Simulation = False
    iovProd = IOVProducer()
LoKiSvc().Welcome = False
MessageSvc().OutputLevel = INFO
EventSelector().PrintFreq = 1

input_path = '/data4/krupaw/UT_Data/0000275911/'

# Setup input
data = []
if os.path.exists(input_path):
    data = glob.glob(input_path + '*.mdf')
else:
    print("Input directory doesn't exist!")
    sys.exit()
if data == []:
    print("Input data doesn't exist!")
    sys.exit()

IOHelper("MDF").inputFiles(data)

monSeq = GaudiSequencer("UTSequence")
monSeq.IgnoreFilterPassed = True

unpacker = LHCb__UnpackRawEvent(
    'UnpackRawEvent',
    BankTypes=['ODIN'],
    RawBankLocations=['/Event/DAQ/RawBanks/ODIN'])
odin = createODIN(RawBanks="DAQ/RawBanks/ODIN")
decoder = UTRawBankToUTNZSDigitsAlg("UTRawToDigits", OutputLevel=INFO, Type="", RawEventLocation='/Event/DAQ/RawEvent')
monitor = UTDigitMonitor("UTDigitMonitor", OutputLevel=INFO, InputData='Raw/UT/Digits')
monSeq.Members = [unpacker, odin, iovProd, decoder, monitor]

appMgr = ApplicationMgr(
    EvtMax=-1,
    TopAlg=[monSeq],
    HistogramPersistency="ROOT",
    ExtSvc=[
        MessageSvcSink(),
        RootHistoSink(FileName="./ut_nzs.root"),
    ],
)
