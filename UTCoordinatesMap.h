/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3),copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence,CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/*
 * UTEmuCoordinatesMap.h
 *
 *  Created on: August,2023
 *      Author: Wojciech Krupa (wokrupa@cern.ch)
 */

#pragma once
#pragma GCC diagnostic ignored "-Wunused-variable"

#include <iostream>
#include <map>
#include <string>
#include <tuple>
#include <vector>

class UTCoordinatesMap {
public:
  // Define a tuple containing two floats and two strings
  typedef std::tuple<float, float, std::string, std::string> CoordinatesTuple;

  // Constructor initializes the 5D vector
  UTCoordinatesMap();

  // Function to access and modify elements in the 5D vector
  CoordinatesTuple         getTuple( int, int, int, int, int ) const;
  unsigned int             getChannel( std::string );
  std::string              getTELL40( unsigned int );
  unsigned int             getTELL40Bin( unsigned int );
  std::vector<std::string> getTELL40NamesASide();
  std::vector<std::string> getTELL40NamesCSide();
  std::vector<std::string> getModulesNames();
  unsigned int             getSourceIDfromBoardNumber( unsigned int );
  std::vector<std::string> getNamesfromTELL40( std::string );

private:
  std::vector<std::vector<std::vector<std::vector<std::vector<CoordinatesTuple>>>>> UT_CoordinatesMap;
  std::map<std::string, unsigned int>                                               UT_ModulesMap;
  std::map<unsigned int, std::string>                                               UT_TELL40Map;
  std::map<unsigned int, unsigned int>                                              UT_BoardIDtoSourceID;
  std::multimap<std::string, std::string>                                           UT_TELL40toModuleName;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsASide_Flow0;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsCSide_Flow0;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsASide_Flow1;
  std::map<unsigned int, unsigned int>                                              UT_TELL40BinsCSide_Flow1;
};
