/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class UTRawBankToUTNZSDigitsAlg
 *
 *  Algorithm to decode UTError Bank to ASIC Headers (TELL40 decoder)
 *  Implementation file for class : UTRawBankToUTNZSDigitsAlg
 *
 *  - \b RawEventLocation: Location of UTError Bank
 *  - \b OutputDigitData:  Location of output UTDigit
 *
 *  @author Christos Hadjivasiliou (christos.hadjivasiliou@cern.ch)
 *  @date   2023-08-10
 */

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTNZSDecoder.h"
#include "Kernel/UTRawBankMap.h"
#include "LHCbAlgs/Transformer.h"
#include "UTDAQ/UTCoordinatesMap.h"
#include "UTDAQ/UTDAQHelper.h"
#include <algorithm>
#include <bitset>
#include <cstdint>
#include <iomanip>
#include <string>
#include <vector>

class UTRawBankToUTNZSDigitsAlg
    : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::UTDigits, std::vector<std::pair<unsigned int, int>>>(
          const LHCb::RawEvent& )> {
public:
  /// Standard constructor
  UTRawBankToUTNZSDigitsAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : MultiTransformer{
            name,
            pSvcLocator,
            { { "RawEventLocation", LHCb::RawEventLocation::Default } },
            { { "OutputDigitData", LHCb::UTDigitLocation::UTDigits }, { "OutputBankData", "Raw/UT/Banks" } } } {}

  std::tuple<LHCb::UTDigits, std::vector<std::pair<unsigned int, int>>>
                                   operator()( const LHCb::RawEvent& ) const override; ///< Algorithm execution
  StatusCode                       initialize() override;
  std::vector<LHCb::RawBank::View> UnpackBank( const LHCb::RawEvent& ) const;
  // Properties
  Gaudi::Property<std::string> m_type{ this, "Type", "NZS" };

  // Counters for decoded banks
  mutable Gaudi::Accumulators::Counter<> m_validHeaders{ this, "#Nb Valid UT banks" };
  mutable Gaudi::Accumulators::Counter<> m_validDigits{ this, "#Nb Valid UT digits" };
  mutable Gaudi::Accumulators::Counter<> m_invalidBanks{ this, "#Nb Invalid banks" };
  mutable Gaudi::Accumulators::Counter<> m_errors_insert{ this, "#Error - failure during inserting hit in TES" };

  mutable std::map<std::string, std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_tell40_counter;
  mutable std::map<std::string, std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_tell40_counter_lane;
  mutable std::map<std::string, std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_tell40_error_counter;
  mutable std::map<std::string, std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_tell40_digits_counter;
  mutable std::map<std::string, std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_tell40_missingChannels_counter;

  mutable UTCoordinatesMap UTMap;

private:
  // The readout tool for handling DAQID and ChannelID
  ToolHandle<IUTReadoutTool> readoutTool{ this, "ReadoutTool", "UTReadoutTool" };
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UTRawBankToUTNZSDigitsAlg )

using namespace LHCb;

StatusCode UTRawBankToUTNZSDigitsAlg::initialize() {

  auto TELL40NamesASide = UTMap.getTELL40NamesASide();
  auto TELL40NamesCSide = UTMap.getTELL40NamesCSide();

  TELL40NamesASide.insert( TELL40NamesASide.end(), TELL40NamesCSide.begin(), TELL40NamesCSide.end() );

  for ( const auto& tell_name : TELL40NamesASide ) {

    const std::string counter_name{ tell_name };
    m_tell40_counter[tell_name] = std::make_unique<Gaudi::Accumulators::Counter<>>( this, counter_name );

    const std::string counter_error_name{ "Error: " + tell_name };
    m_tell40_error_counter[tell_name] = std::make_unique<Gaudi::Accumulators::Counter<>>( this, counter_error_name );

    const std::string counter_digits_name{ "Data: " + tell_name };
    m_tell40_digits_counter[tell_name] = std::make_unique<Gaudi::Accumulators::Counter<>>( this, counter_digits_name );

    const std::string missingChannels_counter_name_{ "ASIC_ID not set: " + tell_name };
    m_tell40_missingChannels_counter[tell_name] =
        std::make_unique<Gaudi::Accumulators::Counter<>>( this, missingChannels_counter_name_ );

    for ( unsigned int lane = 0; lane < 6; lane++ ) {
      const std::string counter_name_lane{ tell_name + "_Lane" + std::to_string( lane ) };
      m_tell40_counter_lane[tell_name + "_Lane" + std::to_string( lane )] =
          std::make_unique<Gaudi::Accumulators::Counter<>>( this, counter_name_lane );
    }
  }

  return StatusCode::SUCCESS;
}

std::vector<LHCb::RawBank::View> UTRawBankToUTNZSDigitsAlg::UnpackBank( const LHCb::RawEvent& rawevt ) const {
  std::vector<LHCb::RawBank::View> tBanks;
  if ( m_type == "UTError" ) {
    tBanks.push_back( rawevt.banks( RawBank::UTError ) );
  } else if ( m_type == "UTSpecial" ) {
    tBanks.push_back( rawevt.banks( RawBank::UTSpecial ) );
  } else {
    tBanks.push_back( rawevt.banks( RawBank::UTError ) );
    tBanks.push_back( rawevt.banks( RawBank::UTNZS ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorFragmentThrottled ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorBXIDCorrupted ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorSyncBXIDCorrupted ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorFragmentMissing ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorFragmentTruncated ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorIdleBXIDCorrupted ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorFragmentMalformed ) );
    tBanks.push_back( rawevt.banks( RawBank::DaqErrorEVIDJumped ) );
  }
  return tBanks;
}

//=============================================================================
// Main execution
//=============================================================================

std::tuple<LHCb::UTDigits, std::vector<std::pair<unsigned int, int>>>
UTRawBankToUTNZSDigitsAlg::operator()( const LHCb::RawEvent& rawevt ) const {

  LHCb::UTDigits                            digits;
  std::vector<std::pair<unsigned int, int>> banks_type;

  UTDAQID daqid;

  for ( const auto& banks : UnpackBank( rawevt ) ) {
    for ( const auto& bank : banks ) {

      unsigned int board_id  = LHCb::UTDAQ::boardIDfromSourceID( bank->sourceID() );
      unsigned int source_id = (unsigned int)( bank->sourceID() );
      auto         buf       = m_tell40_counter[UTMap.getTELL40( source_id )]->buffer();
      ++buf;

      banks_type.push_back( std::pair<unsigned, int>( source_id, bank->type() ) );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "------------------------- ------- UT RAW BANK  ------------------------------- -----------"
                << endmsg;
        debug() << "Bank: 0x" << std::uppercase << std::hex << bank->sourceID() << endmsg;
        debug() << "   Size: " << std::dec << bank->size() << "B" << endmsg;
        debug() << "   Type: " << bank->type() << endmsg;
        debug() << "   Version: " << bank->version() << endmsg;
        debug() << "|     |      HEADER                 | LANE5        | LANE4        | LANE3        | LANE2        "
                   "| LANE1        | LANE0       |"
                << endmsg;
        // Dump raw UT bank
        u_int8_t     iw2     = 0;
        unsigned int counter = 0;
        for ( const u_int8_t* w = bank->begin<u_int8_t>(); w != bank->end<u_int8_t>(); ++w ) {
          ++iw2;
          if ( ( iw2 - 1 ) % 32 == 0 ) {
            counter++;
            if ( counter % 34 == 0 ) {
              debug() << endmsg;
              debug() << "---------------------------------------------------------------------------------------------"
                         "-------------------------------"
                      << endmsg;
            }
            debug() << endmsg;
            debug() << "0x" << std::setfill( '0' ) << std::setw( 2 ) << std::hex << unsigned( iw2 ) << " ";
          }
          if ( ( iw2 - 1 ) % 4 == 0 ) debug() << " | ";

          debug() << std::uppercase << std::hex << std::setw( 2 ) << unsigned( *w ) << " ";
        }
        debug() << endmsg;
      }

      if ( bank->type() == 98 || bank->type() == 68 ) {
        auto decode = [&]( auto decoder ) {
          auto buf = m_tell40_digits_counter[UTMap.getTELL40( source_id )]->buffer();
          ++buf;

          if ( msgLevel( MSG::DEBUG ) ) {
            std::string bxid = std::bitset<12>( decoder.getBXID() ).to_string();
            std::string flag = std::bitset<4>( decoder.getflags() ).to_string();
            debug() << "Bxid: " << bxid << " " << std::dec << decoder.getBXID() << " Flag bits: " << flag << endmsg;
            debug() << "Clusters in lanes from 0 to 5: ";
            for ( unsigned int lane = 0; lane < 6; lane++ ) { debug() << decoder.nClusters( lane ) << " "; }
            debug() << endmsg;
          }

          if ( decoder.nClusters() != 0 ) ++m_validDigits;

          // Decode NZS event
          for ( const auto& aWord : decoder.posRange() ) {
            try {
              std::string binary = std::bitset<16>( aWord.value() ).to_string();
              if ( msgLevel( MSG::DEBUG ) ) {
                debug() << "----------- DECODED DIGIT --------------------" << endmsg;
                debug() << "Binary: " << binary << " Hex: " << std::hex << std::setfill( '0' ) << std::uppercase
                        << std::setw( 4 ) << aWord.value() << endmsg;
                debug() << "Position in the bank: " << std::dec << aWord.pos() << endmsg;
                debug() << "Lane: " << std::dec << aWord.lane() << "  ASIC: " << aWord.asic_num() << endmsg;
              }

              auto buf = m_tell40_counter_lane[UTMap.getTELL40( source_id ) + "_Lane" + std::to_string( aWord.lane() )]
                             ->buffer();
              ++buf;

              if ( aWord.is_not_data_header() ) {
                UTDAQID daqidL =
                    UTDAQID( ( UTDAQID::BoardID )( ( ( bank->sourceID() & 0xff ) - 1 ) * 2 +
                                                   ( ( bank->sourceID() & 0x400 ) >> 10 ) ),
                             ( UTDAQID::LaneID )( aWord.lane() ), ( UTDAQID::ChannelID )( aWord.channelIDL() ) );

                Detector::UT::ChannelID channelIDL = readoutTool->daqIDToChannelID( daqidL );

                if ( msgLevel( MSG::DEBUG ) ) {
                  debug() << "Channel from data bank: " << std::dec << aWord.channelIDL()
                          << " ADC from data bank: " << static_cast<int16_t>( (int8_t)aWord.adcL() ) << endmsg;
                  debug() << daqidL << endmsg;
                  debug() << channelIDL << endmsg;
                  debug() << "1st Channel: " << aWord.channelIDL()
                          << " ADC: " << static_cast<int16_t>( (int8_t)aWord.adcL() ) << endmsg;
                }
                try {
                  digits.insert(
                      new UTDigit( channelIDL, (int8_t)aWord.adcL(), daqidL.id(), aWord.asic_num(), decoder.getBXID() ),
                      channelIDL );
                } catch ( ... ) {
                  if ( msgLevel( MSG::DEBUG ) ) debug() << "Problem with inserting" << endmsg;
                  ++m_errors_insert;
                  continue;
                }
              } else {
                if ( msgLevel( MSG::DEBUG ) ) debug() << "Skipping this digit" << endmsg;
              }

              if ( aWord.is_not_data_header() ) {
                UTDAQID daqidR =
                    UTDAQID( ( UTDAQID::BoardID )( ( ( bank->sourceID() & 0xff ) - 1 ) * 2 +
                                                   ( ( bank->sourceID() & 0x400 ) >> 10 ) ),
                             ( UTDAQID::LaneID )( aWord.lane() ), ( UTDAQID::ChannelID )( aWord.channelIDR() ) );

                Detector::UT::ChannelID channelIDR = readoutTool->daqIDToChannelID( daqidR );

                if ( msgLevel( MSG::DEBUG ) ) {
                  std::string binary = std::bitset<16>( aWord.value() ).to_string();
                  debug() << "Channel from data bank: " << std::dec << aWord.channelIDR()
                          << " ADC from data bank: " << static_cast<int16_t>( (int8_t)aWord.adcR() ) << endmsg;
                  debug() << daqidR << endmsg;
                  debug() << channelIDR << endmsg;
                  debug() << "2nd Channel: " << aWord.channelIDR()
                          << " ADC: " << static_cast<int16_t>( (int8_t)aWord.adcR() ) << endmsg;
                }
                try {
                  digits.insert(
                      new UTDigit( channelIDR, (int8_t)aWord.adcR(), daqidR.id(), aWord.asic_num(), decoder.getBXID() ),
                      channelIDR );
                } catch ( ... ) {
                  if ( msgLevel( MSG::DEBUG ) ) debug() << "Problem with inserting" << endmsg;
                  ++m_errors_insert;
                  continue;
                }
              } else {
                if ( msgLevel( MSG::DEBUG ) ) debug() << "Skipping this digit" << endmsg;
              }
            } catch ( const std::exception& ex ) {
              if ( msgLevel( MSG::DEBUG ) ) debug() << ex.what() << endmsg;
              auto buf = m_tell40_missingChannels_counter[UTMap.getTELL40( source_id )]->buffer();
              ++buf;
            } catch ( ... ) {
              if ( msgLevel( MSG::DEBUG ) ) debug() << "I found issue for: " << bank->sourceID() << endmsg;
              auto buf = m_tell40_error_counter[UTMap.getTELL40( source_id )]->buffer();
              ++buf;
            }
          }
        };

        if ( ::UTDAQ::version{ bank->version() } == ::UTDAQ::version::v5 ) {
          decode( UTNZSDecoder<::UTDAQ::version::v5>{ *bank } );
          ++m_validHeaders;
        } else {
          debug() << "Wrong version of the RawBank" << endmsg;
          ++m_invalidBanks;
        }
      }
    }
  }
  return { std::move( digits ), std::move( banks_type ) };
}
//=============================================================================
