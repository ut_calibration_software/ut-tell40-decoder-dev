#W.Krupa
#Script to update stack a little bit faster

#!/bin/bash

# Input directory containing Git repositories
input_dir="/data4/krupaw/stack3/"

# Function to perform a `git pull` in a directory and its subdirectories
git_pull_recursive() {
  local current_dir="$1"
  cd "$current_dir"

  if [ -d .git ]; then
    echo "Pulling updates in $current_dir"
    git pull
  fi

  for dir in *; do
    if [ -d "$dir" ]; then
      git_pull_recursive "$current_dir/$dir"
    fi
  done
  cd ..
}

# Check if the input directory exists
if [ ! -d "$input_dir" ]; then
  echo "Input directory does not exist: $input_dir"
  exit 1
fi

# Call the recursive function to perform `git pull` for all repositories
git_pull_recursive "$input_dir"

echo "Git pull completed for all repositories in $input_dir and its subdirectories"
