/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

/** @class UTRawBankToUTDigitsAlg
 *
 *  Algorithm to decode UTRawBank to UTDigits (TELL40 decoder)
 *  Implementation file for class : UTRawBankToUTDigitsAlg
 *
 *  - \b RawEventLocation: Location of UTRawBank
 *  - \b OutputDigitData:  Location of output UTDigit
 *
 *  @author Xuhao Yuan (based on code by A Beiter and M Needham)
 *  @author Wojciech Krupa (based on code by Xuhao Yuan, wokrupa@cern.ch)
 *  @date   2021-04-19  / 2023-05-25
 */

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTDecoder.h"
#include "Kernel/UTRawBankMap.h"
#include "LHCbAlgs/Transformer.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTDAQHelper.h"
#include "UTDAQ/UTHeaderWord.h"
#include <algorithm>
#include <bitset>
#include <iomanip>
#include <string>
#include <vector>

class UTRawBankToUTDigitsAlg : public LHCb::Algorithm::Transformer<LHCb::UTDigits(const LHCb::RawEvent &)>
{
public:
  /// Standard constructor
  UTRawBankToUTDigitsAlg(const std::string &name, ISvcLocator *pSvcLocator)
      : Transformer{name,
                    pSvcLocator,
                    {{"RawEventLocation", LHCb::RawEventLocation::Default}},
                    {"OutputDigitData", LHCb::UTDigitLocation::UTDigits}} {}

  LHCb::UTDigits operator()(const LHCb::RawEvent &) const override; ///< Algorithm execution
  StatusCode initialize() override;
  std::vector<LHCb::RawBank::View> UnpackBank(const LHCb::RawEvent &) const;

  // properties
  Gaudi::Property<std::string> m_type{this, "Type", "ZS"};

  // Counters for decoded banks
  mutable Gaudi::Accumulators::Counter<> m_validHeaders{this, "#Nb Valid UT banks"};

  mutable Gaudi::Accumulators::Counter<> m_validHitsBanks{this, "#Nb Valid UT banks with hits"};

  mutable Gaudi::Accumulators::Counter<> m_validDigits{this, "#Nb Valid UT digits"};
  mutable Gaudi::Accumulators::Counter<> m_invalidBanks{this, "#Nb Invalid banks"};
  mutable Gaudi::Accumulators::Counter<> m_errors_channel{this, "#Error - invalid channel number"};
  mutable Gaudi::Accumulators::Counter<> m_errors_insert{this, "#Error - failure during inserting hit in TES"};
  mutable Gaudi::Accumulators::Counter<> m_errors_duplicat{this, "#Errors - duplicate channel"};

  mutable std::vector<std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_tell40_counter_flow0;
  mutable std::vector<std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_tell40_counter_flow1;
  mutable std::vector<std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_data_tell40_counter_flow0;
  mutable std::vector<std::unique_ptr<Gaudi::Accumulators::Counter<>>> m_data_tell40_counter_flow1;

private:
  // The readout tool for handling DAQID and ChannelID
  ToolHandle<IUTReadoutTool> readoutTool{this, "ReadoutTool", "UTReadoutTool"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT(UTRawBankToUTDigitsAlg)

using namespace LHCb;

StatusCode UTRawBankToUTDigitsAlg::initialize()
{

  for (unsigned int tell40 = 0; tell40 < 130; tell40++)
  {
    const std::string counter_name{"Tell_40_" + std::to_string(tell40) + "_0"};
    m_tell40_counter_flow0.push_back(std::make_unique<Gaudi::Accumulators::Counter<>>(this, counter_name));
    const std::string counter_name_{"Tell_40_" + std::to_string(tell40) + "_1"};
    m_tell40_counter_flow1.push_back(std::make_unique<Gaudi::Accumulators::Counter<>>(this, counter_name_));
    const std::string hits_counter_name{"Hits_Tell_40_" + std::to_string(tell40) + "_0"};
    m_data_tell40_counter_flow0.push_back(
        std::make_unique<Gaudi::Accumulators::Counter<>>(this, hits_counter_name));
    const std::string hits_counter_name_{"Hits_Tell_40_" + std::to_string(tell40) + "_1"};
    m_data_tell40_counter_flow1.push_back(
        std::make_unique<Gaudi::Accumulators::Counter<>>(this, hits_counter_name_));
  }

  return StatusCode::SUCCESS;
}

std::vector<LHCb::RawBank::View> UTRawBankToUTDigitsAlg::UnpackBank(const LHCb::RawEvent &rawevt) const
{

  std::vector<LHCb::RawBank::View> tBanks;
  if (m_type == "UT")
  {
    tBanks.push_back(rawevt.banks(RawBank::UT));
    tBanks.push_back(rawevt.banks(RawBank::UTError));
  }
  else if (m_type == "UT_Clustering")
  {
    tBanks.push_back(rawevt.banks(RawBank::UT));
    tBanks.push_back(rawevt.banks(RawBank::UTError));
  }
  else if (m_type == "Error")
    tBanks.push_back(rawevt.banks(RawBank::UTError));
  else if (m_type == "Special")
    tBanks.push_back(rawevt.banks(RawBank::UTSpecial));
  else
  {
    tBanks.push_back(rawevt.banks(RawBank::UT));
    tBanks.push_back(rawevt.banks(RawBank::UTError));
  }
  return tBanks;
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::UTDigits UTRawBankToUTDigitsAlg::operator()(const LHCb::RawEvent &rawevt) const
{
  // Retrieve  the decoded Digits
  LHCb::UTDigits digits;
  for (const auto &banks : UnpackBank(rawevt))
  {
    for (const auto &bank : banks)
    {
      unsigned int source_id = LHCb::UTDAQ::boardIDfromSourceID(bank->sourceID());

      if (bank->size() == 0)
        continue;
      // if ( 0 ) {
      if (msgLevel(MSG::DEBUG))
      {
        debug() << "------------------------- ------- UT RAW BANK  ------------------------------- -----------"
                << endmsg;
        debug() << "Bank: 0x" << std::uppercase << std::hex << bank->sourceID() << "   Size: " << std::dec
                << bank->size() << "B" << endmsg;
        debug() << "   Type: " << bank->type() << "   Version: " << bank->version() << endmsg;
        debug() << "|     |      HEADER                 | LANE5        | LANE4        | LANE3        | LANE2        "
                   "| LANE1        | LANE0       |"
                << endmsg;
        // Dump raw UT bank
        u_int8_t iw2 = 0;
        unsigned int counter = 0;
        for (const u_int8_t *w = bank->begin<u_int8_t>(); w != bank->end<u_int8_t>(); ++w)
        {
          ++iw2;
          if ((iw2 - 1) % 32 == 0)
          {
            counter++;
            if (counter % 34 == 0)
            {
              debug() << endmsg;
              debug() << "---------------------------------------------------------------------------------------------"
                         "-------------------------------"
                      << endmsg;
            }
            debug() << endmsg;
            debug() << "0x" << std::setfill('0') << std::setw(2) << std::hex << unsigned(iw2) << " ";
          }
          if ((iw2 - 1) % 4 == 0)
            debug() << " | ";

          debug() << std::uppercase << std::hex << std::setw(2) << unsigned(*w) << " ";
        }
        debug() << endmsg;
      }
      // Accumulator for indicating which TELL40s were active
      if (((unsigned int)(bank->sourceID() >> 10 & 0x1)) == 0)
      {
        auto buf = m_tell40_counter_flow0[(unsigned int)(bank->sourceID() & 0x3FF)]->buffer();
        ++buf;
      }
      else if (((unsigned int)(bank->sourceID() >> 10 & 0x1)) == 1)
      {
        auto buf = m_tell40_counter_flow1[(unsigned int)(bank->sourceID() & 0x3FF)]->buffer();
        ++buf;
      }

      // Run decoder over UT Bank
      auto decode = [&](auto decoder)
      {
        // if ( 0 ) {
        if (msgLevel(MSG::DEBUG))
        {
          std::string bxid = std::bitset<12>(decoder.getBXID()).to_string();
          std::string flag = std::bitset<4>(decoder.getflags()).to_string();
          debug() << "Bxid: " << bxid << " " << std::dec << decoder.getBXID() << " Flag bits: " << flag << endmsg;
          debug() << "Clusters in lanes from 0 to 5: ";
          for (unsigned int lane = 0; lane < 6; lane++)
          {
            debug() << decoder.nClusters(lane) << " ";
          }
          debug() << endmsg;
        }

        if (decoder.nClusters() != 0)
          ++m_validHitsBanks;

        // If there is data bank, decode event
        // Decode ZS event with clustering
        if (m_type == "UT_Clustering")
        {
          // 1 means weightening,
          for (const auto &aWord : decoder.posAdcRange(1, 4))
          {
            std::vector<Detector::UT::ChannelID> duplicates;
            UTDAQID daqid;

            daqid = UTDAQID((UTDAQID::BoardID)(((bank->sourceID() & 0xff) - 1) * 2 +
                                               ((bank->sourceID() & 0x400) >> 10)),
                            (UTDAQID::LaneID)(aWord.channelID() / 512),
                            (UTDAQID::ChannelID)(aWord.channelID() & 0x1ff));

            if (((unsigned int)(bank->sourceID() >> 10 & 0x1)) == 0)
            {
              auto buf = m_data_tell40_counter_flow0[(unsigned int)(bank->sourceID() & 0x3FF)]->buffer();
              ++buf;
            }
            else if (((unsigned int)(bank->sourceID() >> 10 & 0x1)) == 1)
            {
              auto buf = m_data_tell40_counter_flow1[(unsigned int)(bank->sourceID() & 0x3FF)]->buffer();
              ++buf;
            }

            try
            {
              Detector::UT::ChannelID channelID = readoutTool->daqIDToChannelID(daqid);
              if (msgLevel(MSG::DEBUG))
              {
                std::string binary = std::bitset<16>(aWord.value()).to_string();
                debug() << "----------- DECODED DIGIT --------------------" << endmsg;
                debug() << "Binary: " << binary << " Hex: " << std::hex << std::setfill('0') << std::uppercase
                        << std::setw(4) << aWord.value() << endmsg;
                debug() << "Channel from data bank: " << std::dec << aWord.channelID()
                        << " ADC from data bank: " << aWord.adc() << endmsg;
                debug() << "ClusterSize: " << aWord.clusterSize() << " ClusterCharge: " << aWord.clusterCharge()
                        << " FracStripBits: " << aWord.fractionalStripPosition() << endmsg;
                debug() << daqid << endmsg;
                debug() << channelID << endmsg;
              }
              if (std::find(duplicates.begin(), duplicates.end(), channelID) != duplicates.end())
              {
                ++m_errors_duplicat;
              }
              else
              {
                ++m_validDigits;
                try
                {
                  digits.insert(new UTDigit(channelID, aWord.adc(), daqid.id()), channelID);
                }
                catch (...)
                {
                  debug() << "Problem with inserting hits" << endmsg;
                  ++m_errors_insert;
                  continue;
                }
              }
              duplicates.push_back(channelID);
            }
            catch (...)
            {
              debug() << "Problem with channel ID: " << aWord.channelID() << endmsg;
              debug() << daqid << endmsg;
              ++m_errors_channel;
              continue;
            }
          }
        }
        // Otherwise decode as ZS
        else
        {
          for (const auto &aWord : decoder.posRange())
          {
            std::vector<Detector::UT::ChannelID> duplicates;
            UTDAQID daqid;

            daqid = UTDAQID((UTDAQID::BoardID)(((bank->sourceID() & 0xff) - 1) * 2 +
                                               ((bank->sourceID() & 0x400) >> 10)),
                            (UTDAQID::LaneID)(aWord.channelID() / 512),
                            (UTDAQID::ChannelID)(aWord.channelID() & 0x1ff));

            if (((unsigned int)(bank->sourceID() >> 10 & 0x1)) == 0)
            {
              auto buf = m_data_tell40_counter_flow0[(unsigned int)(bank->sourceID() & 0x3FF)]->buffer();
              ++buf;
            }
            else if (((unsigned int)(bank->sourceID() >> 10 & 0x1)) == 1)
            {
              auto buf = m_data_tell40_counter_flow1[(unsigned int)(bank->sourceID() & 0x3FF)]->buffer();
              ++buf;
            }

            try
            {
              Detector::UT::ChannelID channelID = readoutTool->daqIDToChannelID(daqid);
              if (msgLevel(MSG::DEBUG))
              {
                debug() << "----------- DECODED DIGIT --------------------" << endmsg;

                std::string binary = std::bitset<16>(aWord.value()).to_string();
                debug() << "Binary: " << binary << " Hex: " << std::hex << std::setfill('0') << std::uppercase
                        << std::setw(4) << aWord.value() << endmsg;
                debug() << "Channel from data bank: " << std::dec << aWord.channelID()
                        << " ADC from data bank: " << aWord.adc() << endmsg;
                debug() << daqid << endmsg;
                debug() << channelID << endmsg;
              }
              if (std::find(duplicates.begin(), duplicates.end(), channelID) != duplicates.end())
              {
                ++m_errors_duplicat;
              }
              else
              {
                ++m_validDigits;
                try
                {
                  digits.insert(new UTDigit(channelID, aWord.adc(), daqid.id()), channelID);
                }
                catch (...)
                {
                  debug() << "Problem with inserting" << endmsg;
                  ++m_errors_insert;
                  continue;
                }
              }
              duplicates.push_back(channelID);
            }
            catch (...)
            {
              debug() << "Problem with channel ID: " << aWord.channelID() << endmsg;
              std::string binary = std::bitset<16>(aWord.value()).to_string();
              debug() << "Binary: " << binary << " Hex: " << std::hex << std::setfill('0') << std::uppercase
                      << std::setw(4) << aWord.value() << endmsg;
              debug() << std::dec << daqid << endmsg;
              ++m_errors_channel;
              continue;
            }
          }
        }
      };
      if (::UTDAQ::version{bank->version()} == ::UTDAQ::version::v2)
      {
        decode(UTDecoder<::UTDAQ::version::v2>{*bank});
        ++m_validHeaders;
      }
      else if (::UTDAQ::version{bank->version()} == ::UTDAQ::version::v5)
      {
        decode(UTDecoder<::UTDAQ::version::v5>{*bank});
      }
      else
      {
        debug() << "Wrong version of the RawBank" << endmsg;
        ++m_invalidBanks;
      }
    }
  }
  return digits;
}

//=============================================================================
