###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import logging
from Gaudi.Configuration import VERBOSE, DEBUG, INFO
from PRConfig.TestFileDB import test_file_db
from PyConf.application import (
    configure_input,
    configure,
    CompositeNode,
    default_raw_event,
    make_odin
)
from PyConf.Algorithms import UTDigitMonitor, UTRawBankToUTDigitsAlg

# I/O configuration
options = test_file_db["upgrade-baseline-ut-new-geo-digi"].make_lbexec_options(
    simulation=True,
    python_logging_level=logging.INFO,
    evt_max=2000,
    histo_file='ut_histo_mc.root',
    ntuple_file='ut_mc.root',
    data_type='Upgrade',
    geometry_version="run3/trunk",
    conditions_version='master',
    input_type="ROOT"
)

# Setting up algorithms pipeline
configure_input(options)
odin = make_odin()
decoder = UTRawBankToUTDigitsAlg(
    OutputLevel=DEBUG, name='UTRawToDigits', Type = "UT", RawEventLocation=default_raw_event(["UT"]))
digits = decoder.OutputDigitData
ut_digits_monitor = UTDigitMonitor(
    OutputLevel=DEBUG, name="UTDigitMonitor", InputData=digits)
top_node = CompositeNode("UT_Decoding", [decoder, ut_digits_monitor])
configure(options, top_node)