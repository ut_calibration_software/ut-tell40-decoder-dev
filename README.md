# UT TELL 40 RawBank decoder (local repository)

This repository contains current local version of ZS and NZS decoder for UT TELL40 output. 

In this README.md you can find instructions on how to set up the whole environment
## Setting up

### 1. Build stack

**First, you need to build the lhcb-software stack.**  

Official documentation: https://gitlab.cern.ch/rmatev/lb-stack-setup

**IMPORTANT:** You need at least 10 GiB of free space to compile the stack. Please consider using work directory on lxplus or your local CVM (local machine with cern virtual machine environment).
Go to your stack destination and do:
```sh
curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - stack
```
Usually local cvm machines are much faster than network file systems such as AFS.

Go to ```stack``` directory and compile Moore or LHCb:
```sh
make Moore
```

It will took ~40 minuts up to 1h to compile it for the first time.

### 2. Set up decoder

**Add the decoder to software stack:**

* Add `UTRawBankToUTDigitsAlg.cpp` and  `UTRawBankToUTDigitsNZSAlg.cpp` - `$PATH/stack/LHCb/UT/UTDAQ/src/component/` 

* Update `UTDAQDefinitions.h` - `$PATH/stack/LHCb/UT/UTKernel/include/Kernel/UTDAQDefinitions.h`

* Add the line:  `src/component/UTRawBankToUTDigitsAlg.cpp`, `src/Lib/UTCoordinatesMap.cpp`  and `src/component/UTRawBankToUTDigitsNZSAlg.cpp` to: `$PATH/stack/LHCb/UT/UTDAQ/CMakeLists.txt` to make the decoder visible for LHCb projects or copy this file.

* Then, repalce:
`UTDAQ` directory with the content: - `$PATH/stack/LHCb/UT/UTKernel/include/UTDAQ`

* `UTDecoder.h` - `$PATH/stack/LHCb/UT/UTKernel/include/Kernel/UTDecoder.h`

* `UTNZSDecoder.h` - `$PATH/stack/LHCb/UT/UTKernel/include/Kernel/UTNZSDecoder.h`

* `UTDigit.h` - `$PATH/stack/LHCb/Event/DigiEvent/include/Event/UTDigit.h`

* `UTCoordinatesMap.cpp` - `$PATH/stack/LHCb/UT/UTDAQ/src/Lib/UTCoordinatesMap.cpp`

* `UTCoordinatesMap.h` - `$PATH/stack/LHCb/UT/UTDAQ/include/UTDAQ/UTCoordinatesMap.h`

* Finally, recompile (`make Moore`).

The basic monitoring algorithm is ready to use in `Lbcom`: `$PATH/stack/Lbcom/UT/UTCheckers/src/UTDigitMonitor.cpp`

Also to make your life easier check `update_stack.sh` and `copy_all.py` 

### 3. Get the data

I've shared some data file ready to use in:
`\eos\user\w\wokrupa\UT_DATA\Runs`

All member of `lhcb-upgrade-ut group` should have access to this location. In case of any troubles please contact me (wokrupa@agh.edu.pl).

* `Runs/0000275911`: NZS run 4x3 - most of tell40s active; 33 events 
* `Runs/0000280628`: NZS run all flavours 4000 events; asic id not set...
* `Runs/0000280977`: part of ZS global run (10GB) 

* `ManyHits`: Data contains many hits.
* `SO_Normal`: 4x3 normal data with latest updates (for example SO bug is fixed). Threshold=31 for all the chips. HV was off, nominal temperature, and there was no beam, and pedestals were loaded. We chose a random trigger of 1 MHz.
* `SO_NZS`: 4x3 NZS data with latest updates (for example SO bug is fixed). Threshold=31 for all the chips. HV was off, nominal temperature, and there was no beam, and pedestals were loaded. We chose a random trigger of 1 MHz.
* `SO_Hits`: **(I recommend to use this data sample for the first time)** 4x3 normal data with latest updates (for example SO bug is fixed) and with a threshold (threshold=6 in UTaX_8CB_M3_chip 0) which is not 31 in one of the chips. HV was off, nominal temperature, and there was no beam, and pedestals were loaded. We chose a random trigger of 1 MHz.

The simulated data are already in eos and you don't need to set the path. 

To get the data  from there, login to `lxplus` then `lbgw` (online account required) and finally to `plus` (online account required). 
 
**IMPORTANT:**
Change path to data location in `ut_test_data.py` and other...!

### 4. Run the decoder

To run:

In case of real data, indicate input directory `ut_test_data.py`

```sh
utils/run-env Moore gaudirun.py $PATH/ut_test_sim.py 
utils/run-env Moore gaudirun.py $PATH/ut_test_data.py 
....
```
