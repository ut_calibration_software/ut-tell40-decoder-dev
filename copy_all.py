# run: python3 copy_all.py
# Simple script to copy all files 
import shutil
import sys, os

input_path = "/data4/krupaw/ut-tell40-decoder-dev/"
output_path = "/data4/krupaw/stack2/"

# Dictionary of source file locations and their corresponding destination locations
file_copy_dict = {
    input_path + "UTRawBankToUTDigitsAlg.cpp": output_path + "/LHCb/UT/UTDAQ/src/component/UTRawBankToUTDigitsAlg.cpp",
    input_path + "UTRawBankToUTNZSDigitsAlg.cpp": output_path + "/LHCb/UT/UTDAQ/src/component/UTRawBankToUTNZSDigitsAlg.cpp",
    input_path + "CMakeLists.txt": output_path + "/LHCb/UT/UTDAQ/CMakeLists.txt",
    input_path + "UTDAQDefinitions.h": output_path + "/LHCb/UT/UTKernel/include/Kernel/UTDAQDefinitions.h",
    input_path + "UTDAQ/UTNZSWord.h": output_path + "/LHCb/UT/UTKernel/include/UTDAQ/UTNZSWord.h",
    input_path + "UTDAQ/UTHeaderWord.h": output_path + "/LHCb/UT/UTKernel/include/UTDAQ/UTHeaderWord.h",
    input_path + "UTDAQ/UTADCWord.h": output_path + "/LHCb/UT/UTKernel/include/UTDAQ/UTADCWord.h",
    input_path + "UTDecoder.h": output_path + "/LHCb/UT/UTKernel/include/Kernel/UTDecoder.h",
    input_path + "UTNZSDecoder.h": output_path + "/LHCb/UT/UTKernel/include/Kernel/UTNZSDecoder.h",
    input_path + "UTDigit.h": output_path + "/LHCb/Event/DigiEvent/include/Event/UTDigit.h",
    input_path + "UTCoordinatesMap.h": output_path + "/LHCb/UT/UTDAQ/include/UTDAQ/UTCoordinatesMap.h",
    input_path + "UTCoordinatesMap.cpp": output_path + "/LHCb/UT/UTDAQ/src/Lib/UTCoordinatesMap.cpp",
}

# Iterate through the dictionary and copy files from source to destination
for source_file, destination_file in file_copy_dict.items():
    # Check if the source file exists
    if not os.path.exists(source_file):
        print(f"Source file does not exist: {source_file}")
        continue

    # Get the directory path for the destination file and create it if it doesn't exist
    destination_dir = os.path.dirname(destination_file)
    if not os.path.exists(destination_dir):
        print(f"Destination directory does not exist. Skipping...")
        continue

    # Copy the source file to the destination file
    shutil.copy(source_file, destination_file)
    print(f"File {source_file} copied to {destination_file}")

print("Files copied successfully.")
